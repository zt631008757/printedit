package com.app.android.minjieprint.responce;

import java.util.List;

public class GetFileResponce extends BaseResponce {

    public List<DocInfo> data;
    public String msg;
    public String code;

    public class DocInfo {
        public String updatedon;
        public String docname;
        public String content;
    }
}
