package com.app.android.minjieprint.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.responce.GetLogListResponce;

import java.util.List;

/**
 * Created by Administrator on 2018/8/22.
 */

public class LogoList_NameAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    List<GetLogListResponce.LogoGroup> list = null;
    CommCallBack callBack;
    GetLogListResponce.LogoGroup currentGroupInfo;

    public LogoList_NameAdapter(Context mContext, CommCallBack callBack) {
        this.mContext = mContext;
        this.callBack = callBack;
    }

    public void setData(List<GetLogListResponce.LogoGroup> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void setSelectGroup(GetLogListResponce.LogoGroup currentGroupInfo) {
        this.currentGroupInfo = currentGroupInfo;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        ViewGroup viewGroup = (ViewGroup) mInflater.inflate(R.layout.item_logolist_name, null, false);
        ContentViewHolder holder = new ContentViewHolder(viewGroup);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ContentViewHolder viewHolder = (ContentViewHolder) holder;
        final GetLogListResponce.LogoGroup info = list.get(position);
        viewHolder.tv_name.setText(info.groupName);
        viewHolder.adapter.setData(info.childList,currentGroupInfo);

        if (info.isSelect) {
            viewHolder.recyclerview_name_child.setVisibility(View.VISIBLE);
            viewHolder.iv_img.setImageResource(R.drawable.ico_logolist_move);
        } else {
            viewHolder.recyclerview_name_child.setVisibility(View.GONE);
            viewHolder.iv_img.setImageResource(R.drawable.ico_logolist_add);
        }

        viewHolder.ll_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                info.isSelect = !info.isSelect;
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }


    class ContentViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name;
        ImageView iv_img;
        RecyclerView recyclerview_name_child;
        LogoList_NameChildAdapter adapter;
        LinearLayout ll_content;

        public ContentViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            ll_content = itemView.findViewById(R.id.ll_content);
            iv_img = itemView.findViewById(R.id.iv_img);

            recyclerview_name_child = itemView.findViewById(R.id.recyclerview_name_child);
            recyclerview_name_child.setLayoutManager(new LinearLayoutManager(mContext));
            adapter = new LogoList_NameChildAdapter(mContext, callBack);
            recyclerview_name_child.setAdapter(adapter);
        }
    }
}
