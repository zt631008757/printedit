package com.app.android.minjieprint.responce;

import com.app.android.minjieprint.bean.LogoInfo;

import java.util.List;

public class GetLogListResponce extends BaseResponce {

    public Data data;

    public class Data extends BaseResponce_Data {
        public List<LogoGroup> groupList;
    }

    public class LogoGroup {
        public String groupName;
        public List<LogoGroup> childList;
        public List<LogoInfo> logoList;

        public boolean isSelect = false;
    }

}
