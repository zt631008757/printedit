package com.app.android.minjieprint.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.event.Event_ChangeLanguge;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.manager.LangugeManager;
import com.app.android.minjieprint.tool.Log;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.Locale;

/**
 * Created by Administrator on 2018/8/22.
 */

public class LangugeListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    List<Locale> list = null;
    CommCallBack callBack;

    public LangugeListAdapter(Context mContext, CommCallBack callBack) {
        this.mContext = mContext;
        this.callBack = callBack;
    }

    public void setData(List<Locale> list) {
        this.list = list;
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        ViewGroup viewGroup = (ViewGroup) mInflater.inflate(R.layout.item_languge, null, false);
        ContentViewHolder holder = new ContentViewHolder(viewGroup);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ContentViewHolder viewHolder = (ContentViewHolder) holder;
        final Locale info = list.get(position);
        viewHolder.tv_text.setText(info.getDisplayLanguage(info));
        if (LangugeManager.getCurrentLanguge().getDisplayName().equals(info.getDisplayName())) {
            viewHolder.iv_img.setVisibility(View.VISIBLE);
        } else {
            viewHolder.iv_img.setVisibility(View.GONE);
        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("切换语言：" + info.getDisplayLanguage());

                LangugeManager.saveLanguge(info);
                EventBus.getDefault().post(new Event_ChangeLanguge());

                if(callBack!=null)
                {
                    callBack.onResult(null);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class ContentViewHolder extends RecyclerView.ViewHolder {
        TextView tv_text;
        ImageView iv_img;

        public ContentViewHolder(View itemView) {
            super(itemView);
            tv_text = itemView.findViewById(R.id.tv_text);
            iv_img = itemView.findViewById(R.id.iv_img);

        }
    }
}
