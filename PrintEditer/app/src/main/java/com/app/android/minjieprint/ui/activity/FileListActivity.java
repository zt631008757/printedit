package com.app.android.minjieprint.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.adapter.FragmentViewPagerAdapter;
import com.app.android.minjieprint.ui.fragment.FileListFragment;
import com.app.android.minjieprint.ui.view.MyTabIndicator;
import com.app.android.minjieprint.util.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/6/20.
 */

public class FileListActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filelist);
        initView();
        getData();
    }

    ViewPager viewpager;
    MyTabIndicator mytabindicator;

    FileListFragment localFileListFragment;  //本地
    FileListFragment serverFileListFragment;  //服务器

    //初始化控件
    private void initView() {
        setTitle(Util.getString(R.string.file_list_title));    //设置标题
        setLeftImgClickListener();  //设置左键返回

        mytabindicator = findViewById(R.id.mytabindicator);
        viewpager = findViewById(R.id.viewpager);

        //业务部分
        List<String> strs = new ArrayList<>();
        strs.add(Util.getString(R.string.file_list_local));
//        strs.add(Util.getString(R.string.file_list_sever));

        List<Fragment> fragmentList = new ArrayList<>();
        localFileListFragment = new FileListFragment();
        localFileListFragment.type = 0;
//        serverFileListFragment = new FileListFragment();
//        serverFileListFragment.type = 1;
        fragmentList.add(localFileListFragment);
//        fragmentList.add(serverFileListFragment);

        mytabindicator.setData(strs, true);

        FragmentViewPagerAdapter viewPagerAdapter = new FragmentViewPagerAdapter(getSupportFragmentManager(), fragmentList);
        viewpager.setAdapter(viewPagerAdapter);
        viewpager.setOffscreenPageLimit(4);
        viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                mytabindicator.setSelectedPosition(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                mytabindicator.setOnscroll(arg0, arg1);
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });
        mytabindicator.setOnTabChangedListener(new MyTabIndicator.OnTabChangedListener() {
            @Override
            public void onTabChanged(int index) {
                viewpager.setCurrentItem(index);
            }
        });
        mytabindicator.setSelectedPosition(0);
        mytabindicator.setOnscroll(0, 0);
        viewpager.setCurrentItem(0);
    }

    //网路请求 获取数据
    private void getData() {
    }

    //绑定数据
    private void bindUI() {

    }

    //点击事件
    @Override
    public void onClick(View view) {
        super.onClick(view);
        Intent intent;
        switch (view.getId()) {
//            case R.id.tv_tohaibao:
//                intent = new Intent(mContext, YaoQing_HaiBaoActivity.class);
//                startActivity(intent);
//                break;
        }
    }
}
