package com.app.android.minjieprint.adapter;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.tool.Log;
import com.feasycom.bean.BluetoothDeviceWrapper;

import java.util.ArrayList;

public class SearchDeviceListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    CommCallBack callBack;
    ArrayList<BluetoothDeviceWrapper> mDevices = new ArrayList<BluetoothDeviceWrapper>();

    public SearchDeviceListAdapter(Context context, CommCallBack callBack) {
        super();
        mContext = context;
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        // TODO Auto-generated method stub
        return mDevices.size();
    }


    public void addDevice(BluetoothDeviceWrapper deviceDetail) {
        if (null == deviceDetail) return;
        boolean has = false;
        for (int i = 0; i < mDevices.size(); i++) {
            if (deviceDetail.getAddress().equals(mDevices.get(i).getAddress())) {
                mDevices.get(i).setName(deviceDetail.getName());
                mDevices.get(i).setRssi(deviceDetail.getRssi());
                mDevices.get(i).setAdvData(deviceDetail.getAdvData());
                has = true;
                break;
            }
        }
        if (!has) {
            mDevices.add(deviceDetail);
        }
    }

    public void clearList() {
        mDevices.clear();
        notifyDataSetChanged();
    }

    public ArrayList<BluetoothDeviceWrapper> getmDevices() {
        return mDevices;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        ViewGroup viewGroup = (ViewGroup) mInflater.inflate(R.layout.search_device_info, null, false);
        ContentViewHolder holder = new ContentViewHolder(viewGroup);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        // TODO Auto-generated method stub
        ContentViewHolder viewHolder = (ContentViewHolder) holder;
        final BluetoothDeviceWrapper deviceDetail = mDevices.get(position);
        String deviceName = deviceDetail.getName();
        String deviceAdd = deviceDetail.getAddress();
        int deviceRssi = deviceDetail.getRssi().intValue();
        if (deviceName != null && deviceName.length() > 0) {
            //设备名长度限制，最大30
            if (deviceName.length() >= 30) {
                deviceName = deviceName.substring(0, 30);
            }
            viewHolder.tvName.setText(deviceName);
        } else {
            viewHolder.tvName.setText("unknow");
        }
        if (deviceAdd != null && deviceAdd.length() > 0) {
            viewHolder.tvAddr.setText(" (" + deviceAdd + ")");
        } else {
            viewHolder.tvAddr.setText(" (unknow)");
        }

        viewHolder.pbRssi.setProgress(100 + deviceRssi);
        viewHolder.tvRssi.setText("rssi(" + deviceRssi + ")");
        viewHolder.rl_root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callBack != null) {
                    Log.i("test", "点击");
                    callBack.onResult(deviceDetail);
                }
            }
        });
    }

    class ContentViewHolder extends RecyclerView.ViewHolder {
        TextView tvRssi;
        TextView tvName;
        TextView tvAddr;
        ProgressBar pbRssi;
        RelativeLayout rl_root;

        public ContentViewHolder(View itemView) {
            super(itemView);
            tvRssi = itemView.findViewById(R.id.tv_rssi);
            tvName = itemView.findViewById(R.id.tv_name);
            tvAddr = itemView.findViewById(R.id.tv_addr);
            pbRssi = itemView.findViewById(R.id.pb_rssi);
            rl_root = itemView.findViewById(R.id.rl_root);
        }
    }

}
