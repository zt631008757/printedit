package com.app.android.minjieprint.ui.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.View;
import android.widget.ImageView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.bean.ElementsInfo;
import com.app.android.minjieprint.tool.Log;
import com.app.android.minjieprint.util.BitmapUtil;
import com.app.android.minjieprint.util.ImageDispose;

public class ElementView_Img extends ElementView_Base {

    public ElementView_Img(Context context, ElementsInfo info) {
        super(context, info);
        init();
    }

    @Override
    public void refreshUI() {
        super.refreshUI();
        iv_img.setImageBitmap(bitmap);
        setRotation(elementsInfo.orientation);
    }

    ImageView iv_img;
    Bitmap bitmap;

    private void init() {
        View.inflate(mContext, R.layout.view_ele_img, this);
        iv_img = findViewById(R.id.iv_img);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    bitmap = BitmapUtil.getSmallBitmap(elementsInfo.imgLocalPath);
                    ((Activity) getContext()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            refreshUI();
                        }
                    });
                } catch (Exception E) {

                }
            }
        }).start();
    }
}
