package com.app.android.minjieprint.manager;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.text.TextUtils;

import com.app.android.minjieprint.dialog.Comm_Dialog;
import com.app.android.minjieprint.interface_.CommCallBack;
//import com.yanzhenjie.permission.AndPermission;

public class AndPermissionManager {

    public static void requestPermission(final Context mContext, OnRequestPermissionListener listener, String... requestPermssions) {
        if (requestPermssions == null || requestPermssions.length == 0) {
            return;
        }
//        AndPermission.with(mContext)
//                .runtime()
//                .permission(requestPermssions)
//                .rationale((context, permissions, executor) -> {
//                    // 此处可以选择显示提示弹窗
//                    executor.execute();
//                })
//                .onGranted(permissions -> {
//                    // Storage permission are allowed.
//                    if (listener != null) {
//                        listener.onGranted();
//                    }
//                })
//                .onDenied(permissions -> {
//                    // Storage permission are not allowed.
//                    // 判断用户是不是不再显示权限弹窗了，若不再显示的话进入权限设置页
//                    if (AndPermission.hasAlwaysDeniedPermission(mContext, permissions)) {
//                        String msg = "";
//                        if(permissions.contains(Manifest.permission.CAMERA)){
//                            msg = msg + "、相机";
//                        }
//                        if(permissions.contains(Manifest.permission.WRITE_EXTERNAL_STORAGE)){
//                            msg = msg + "、存储";
//                        }
//                        if(!TextUtils.isEmpty(msg) && msg.startsWith("、")){
//                            msg = msg.replaceFirst("、","");
//                        }
//                        Comm_Dialog.showCommDialog(mContext, "", "请在手机设置中，打开" + msg +  "权限", "去设置", "取消", new CommCallBack() {
//                            @Override
//                            public void onResult(Object obj) {
//                                // 打开权限设置页
//                                startAppSettings(mContext);
//                            }
//                        },null);
//                        return;
//                    }
//                    if (listener != null) {
//                        listener.onDenied();
//                    }
//                })
//                .start();


    }

    public interface OnRequestPermissionListener {
        void onGranted();

        void onDenied();

    }

    /**
     * 启动应用的设置
     */
    public static void startAppSettings(Context context) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.parse("package:" + context.getPackageName()));
//        intent.addCategory(Intent.CATEGORY_DEFAULT);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(intent);
    }

}
