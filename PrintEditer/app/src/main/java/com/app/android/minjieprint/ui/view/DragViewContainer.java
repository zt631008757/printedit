package com.app.android.minjieprint.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.tool.Log;
import com.app.android.minjieprint.util.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robert on 2017/6/21.
 */

public class DragViewContainer extends FrameLayout implements BaseElementView.DeleteMoveLayout {

    CommCallBack callBack_Edit;

    public void setCallBack_Edit(CommCallBack callBack_Edit) {
        this.callBack_Edit = callBack_Edit;
    }

    public BaseElementView currentView;

    public void clearCurrent() {
        currentView = null;
        refreshView();
    }

    //刷新当前选中view
    private void refreshView() {
        for (int i = 0; i < mMoveLayoutList.size(); i++) {
            if (mMoveLayoutList.get(i) == currentView) {
                mMoveLayoutList.get(i).setSelect(true);
            } else {
                mMoveLayoutList.get(i).setSelect(false);
            }
        }
        if (callBack_Edit != null) {
            callBack_Edit.onResult(null);
        }
    }

    //选中回调
    CommCallBack callBack = new CommCallBack() {
        @Override
        public void onResult(Object obj) {
            currentView = (BaseElementView) obj;
            refreshView();
        }
    };

    //删除当前view
    public void deleteCurrentView() {
        if (currentView != null) {
            removeView(currentView);
            mMoveLayoutList.remove(currentView);
            currentView = null;
            refreshView();
        } else {
            if (mMoveLayoutList.size() > 0) {
                View view = mMoveLayoutList.get(mMoveLayoutList.size() - 1);
                removeView(view);
                mMoveLayoutList.remove(view);
                refreshView();
            }
        }
    }

    //旋转
    public void rotate() {
        if (currentView != null) {
            currentView.elementView_base.elementsInfo.orientation += 90;
            if (currentView.elementView_base.elementsInfo.orientation == 360) {
                currentView.elementView_base.elementsInfo.orientation = 0;
            }

            int temp = currentView.elementView_base.elementsInfo.width;
            currentView.elementView_base.elementsInfo.width = currentView.elementView_base.elementsInfo.height;
            currentView.elementView_base.elementsInfo.height = temp;
            currentView.refresh();
        }
    }

    //缩小
    public void sacle_small() {
        if (currentView != null) {
            currentView.center(0, 0);
            currentView.bottom(-10);
            currentView.right(-10);
            currentView.move();
        }
    }

    //放大
    public void sacle_big() {
        if (currentView != null) {
            currentView.center(0, 0);
            currentView.bottom(10);
            currentView.right(10);
            currentView.move();
        }
    }


    //移动
    public void move(int moveAction, int scrollX) {
        if (currentView != null) {
            switch (moveAction) {
                case 1: //上
                    currentView.center(0, -5);
                    currentView.move();
                    break;
                case 2: //左
                    currentView.center(-5, 0);
                    currentView.move();
                    break;
                case 3: //右
                    currentView.center(5, 0);
                    currentView.move();
                    break;
                case 4: //下
                    currentView.center(0, 5);
                    currentView.move();
                    break;
                case 5:  //水平居中
                    int canvasWidth = getResources().getDisplayMetrics().widthPixels - Util.dip2px(mContext, 16);
                    int x = scrollX + (canvasWidth - currentView.getWidth()) / 2 + 15;
                    currentView.elementView_base.elementsInfo.xpos = x;
                    currentView.refresh();
                    break;
                case 6:         //垂直居中
                    int y = (getHeight() - currentView.getHeight()) / 2 + 15;
                    currentView.elementView_base.elementsInfo.ypos = y;
                    currentView.refresh();
                    break;
            }
        }
    }


    private int mSelfViewWidth = 0;
    private int mSelfViewHeight = 0;

    private Context mContext;

    /**
     * the identity of the moveLayout
     */
    private int mLocalIdentity = 0;

    public List<BaseElementView> mMoveLayoutList;

    /*
     * 拖拽框最小尺寸
     */
    private int mMinHeight = 90;
    private int mMinWidth = 90;

    public DragViewContainer(Context context) {
        super(context);
        init(context, this);
    }

    public DragViewContainer(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, this);
    }

    public DragViewContainer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, this);
    }

    private void init(Context c, DragViewContainer thisp) {
        mContext = c;
        mMoveLayoutList = new ArrayList<>();

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                clearCurrent();
            }
        });
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //  Log.e(TAG, "onDraw: height=" + getHeight());
        mSelfViewWidth = getWidth();
        mSelfViewHeight = getHeight();

        if (mMoveLayoutList != null) {
            int count = mMoveLayoutList.size();
            for (int a = 0; a < count; a++) {
                mMoveLayoutList.get(a).setViewWidthHeight(mSelfViewWidth, mSelfViewHeight);
            }
        }

    }

    /**
     * call set Min height before addDragView
     *
     * @param height
     */
    private void setMinHeight(int height) {
        mMinHeight = height;
    }

    /**
     * call set Min width before addDragView
     *
     * @param width
     */
    private void setMinWidth(int width) {
        mMinWidth = width;
    }

//    public void addDragView(ElementView_Base selfView, int left, int top, int right, int bottom, boolean isFixedSize, boolean whitebg) {
//        addDragView(selfView, left, top, right, bottom, isFixedSize, whitebg, mMinWidth, mMinHeight);
//    }
//
//    public void addDragView(ElementView_Base selfView) {
//        addDragView(selfView, selfView.elementsInfo.xpos, selfView.elementsInfo.ypos, selfView.elementsInfo.width, selfView.elementsInfo.height);
//    }

//    /**
//     * 每个moveLayout都可以拥有自己的最小尺寸
//     */
//    public void addDragView(int resId, int left, int top, int right, int bottom, boolean isFixedSize, boolean whitebg, int minwidth, int minheight) {
//        LayoutInflater inflater2 = LayoutInflater.from(mContext);
//        View selfView = inflater2.inflate(resId, null);
//        addDragView(selfView, left, top, right, bottom, isFixedSize, whitebg, minwidth, minheight);
//    }

    /**
     * 每个moveLayout都可以拥有自己的最小尺寸
     */
    public void addDragView(ElementView_Base selfView) {

        int left = (int) selfView.elementsInfo.xpos;
        int top = (int) selfView.elementsInfo.ypos;
        int width = (int) selfView.elementsInfo.width - Util.dip2px(getContext(), 10);
        int height = (int) selfView.elementsInfo.height - Util.dip2px(getContext(), 10);

        BaseElementView moveLayout = new BaseElementView(mContext);
        moveLayout.setCallBack(callBack);
        moveLayout.setClickable(true);
//        moveLayout.setMinHeight(mMinHeight);
//        moveLayout.setMinWidth(mMinWidth);
        if (width < mMinWidth) width = mMinWidth;
        if (height < mMinHeight) height = mMinHeight;

        //set postion
        LayoutParams lp = new LayoutParams(width, height);
        lp.setMargins(left, top, 0, 0);
        moveLayout.setLayoutParams(lp);

        //add sub view (has click indicator)
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View dragSubView = inflater.inflate(R.layout.drag_sub_view, null);
        RelativeLayout addYourViewHere = dragSubView.findViewById(R.id.add_your_view_here);
        RelativeLayout.LayoutParams lv = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        addYourViewHere.addView(selfView, lv);

        moveLayout.elementView_base = selfView;

        moveLayout.addView(dragSubView);

        moveLayout.setOnDeleteMoveLayout(this);
        moveLayout.setIdentity(mLocalIdentity++);

        addView(moveLayout);

        mMoveLayoutList.add(moveLayout);

        currentView = moveLayout;
        refreshView();
    }

//    public void addDragView(int resId, int left, int top, int right, int bottom, boolean isFixedSize, boolean whitebg) {
//        LayoutInflater inflater2 = LayoutInflater.from(mContext);
//        View selfView = inflater2.inflate(resId, null);
//        addDragView(selfView, left, top, right, bottom, isFixedSize, whitebg);
//    }

    @Override
    public void onDeleteMoveLayout(int identity) {
        int count = mMoveLayoutList.size();
        for (int a = 0; a < count; a++) {
            if (mMoveLayoutList.get(a).getIdentity() == identity) {
                //delete
//                removeView(mMoveLayoutList.get(a));
            }
        }
    }

//
//    /**
//     * delete interface
//     */
//    private DeleteDragView mListener = null;
//    public interface DeleteDragView {
//        void onDeleteDragView();
//    }
//    public void setOnDeleteDragView(DeleteDragView l) {
//        mListener = l;
//    }


}
