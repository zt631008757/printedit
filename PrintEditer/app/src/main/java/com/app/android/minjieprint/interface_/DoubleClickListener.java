package com.app.android.minjieprint.interface_;

import android.view.MotionEvent;
import android.view.View;

import com.app.android.minjieprint.tool.Log;

public  abstract class DoubleClickListener implements View.OnTouchListener {
    private static final long DOUBLE_TIME = 1000;
    private static long lastClickTime = 0;

    long currentTimeMillis = System.currentTimeMillis();
    /**相对于View的x、y坐标 **/
    private float xInView, yInView;
    /**在屏幕中的x、y坐标 **/
    private float xDown, yDown;
    @Override
    public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    xInView = event.getX();
                    yInView = event.getY();
                    xDown = event.getRawX();
                    yDown = event.getRawY() ;
                    break;

                case MotionEvent.ACTION_UP:
                    if (xDown == event.getRawX() && yDown == event.getRawY()) {
                        //点击事件
                        Log.i("点击");
                        if (currentTimeMillis - lastClickTime < DOUBLE_TIME) {
                            onDoubleClick(v);
                        }
                        lastClickTime = currentTimeMillis;
                    } else {

                    }
                    break;
            }
        return false;
    }

    public abstract void onDoubleClick(View v);
}
