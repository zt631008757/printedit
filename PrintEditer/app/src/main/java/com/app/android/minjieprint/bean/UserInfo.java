package com.app.android.minjieprint.bean;


import com.app.android.minjieprint.responce.BaseResponce;

import java.io.Serializable;

/**
 * 用户信息
 */

public class UserInfo extends BaseResponce implements Serializable {

    public String userCode;
    public String id;//": 29509,
    public String nickname;//": "",
    public String avatarUrl;//": "",
    public String phoneNumber;//": "18672782213",
    public String emailAddress;//": "",
    public String loginPassword;//": "$2a$10$43rDC1vOwCllK8jtpaAg0.HR/aya8yxujm.6.lUMPqq5ooBzPH7XK",
    public String payPassword;//": "",
    public String recommendCode;//": "LzTHEG",
    public String inviteCode;//": "cgghhhhh",
    public String minerLevelId;//": 0,
    public String availableStatus;//": 1,
    public int authenticationStatus;//": 0,    2  已认证
    public String frozenReason;//": "",
    public String registerTime;//": "2019-06-17T11:24:14.000+0000",
    public String updateTime;//": "2019-06-17T11:24:14.000+0000"
    public String minerLevelPic;    //等级图标
    public String businessFlag;    //是否是商户
    public String upgradeTip;   //升级tip
    public String cartoonLevel;//卡通等级

}
