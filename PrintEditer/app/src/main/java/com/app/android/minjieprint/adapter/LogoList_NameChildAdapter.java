package com.app.android.minjieprint.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.responce.GetLogListResponce;

import java.util.List;

/**
 * Created by Administrator on 2018/8/22.
 */

public class LogoList_NameChildAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    List<GetLogListResponce.LogoGroup> list = null;
    CommCallBack callBack;
    GetLogListResponce.LogoGroup currentGroupInfo;

    public LogoList_NameChildAdapter(Context mContext, CommCallBack callBack) {
        this.mContext = mContext;
        this.callBack = callBack;
    }

    public void setData(List<GetLogListResponce.LogoGroup> list, GetLogListResponce.LogoGroup currentGroupInfo) {
        this.list = list;
        this.currentGroupInfo = currentGroupInfo;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        ViewGroup viewGroup = (ViewGroup) mInflater.inflate(R.layout.item_logolist_name_child, null, false);
        ContentViewHolder holder = new ContentViewHolder(viewGroup);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ContentViewHolder viewHolder = (ContentViewHolder) holder;
        final GetLogListResponce.LogoGroup info = list.get(position);
        viewHolder.tv_text.setText(info.groupName + "（" + (info.logoList == null ? "0" : info.logoList.size()) + "）");

        if (position == list.size() - 1) {
            viewHolder.view_line.setVisibility(View.GONE);
        } else {
            viewHolder.view_line.setVisibility(View.VISIBLE);
        }

        if (currentGroupInfo != null && currentGroupInfo.groupName.equals(info.groupName)) {
            viewHolder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.mainColor_light));
            viewHolder.tv_text.setTextColor(Color.parseColor("#ffffff"));
        } else {
            viewHolder.itemView.setBackgroundColor(Color.parseColor("#ffffff"));
            viewHolder.tv_text.setTextColor(Color.parseColor("#666666"));
        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callBack != null) {
                    callBack.onResult(info);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class ContentViewHolder extends RecyclerView.ViewHolder {
        TextView tv_text;
        View view_line;

        public ContentViewHolder(View itemView) {
            super(itemView);
            tv_text = itemView.findViewById(R.id.tv_text);
            view_line = itemView.findViewById(R.id.view_line);
        }
    }
}
