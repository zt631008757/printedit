package com.app.android.minjieprint.manager;

import com.app.android.minjieprint.MyApplication;
import com.app.android.minjieprint.constant.SPConstants;
import com.app.android.minjieprint.tool.SPUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class LangugeManager {


    public static List<Locale> getAllLanguge() {
        List<Locale> list = new ArrayList<>();
//        list.add(Locale.getDefault());
        list.add(Locale.CHINESE);
        list.add(Locale.ENGLISH);
        return list;
    }

    //获取语言
    public static Locale getCurrentLanguge() {
        String languge = SPUtil.getStringValue(MyApplication.context, SPConstants.Languge, Locale.getDefault().getLanguage());
        Locale locale = new Locale(languge);
        return locale;
    }

    //保存当前使用的语言
    public static void saveLanguge(Locale locale) {
        SPUtil.putValue(MyApplication.context, SPConstants.Languge, locale.getLanguage());
    }

}
