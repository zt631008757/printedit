package com.app.android.minjieprint.tool;

import android.content.Context;

import com.app.android.minjieprint.dialog.LoadingDialog;


public class CommLoading {

    static LoadingDialog mDialog;

    public static void showLoading(Context mContext) {
        showLoading(mContext, null);
    }

    public static void showLoading(Context mContext, String text) {
        try {
            if (mDialog == null) {
                mDialog = new LoadingDialog(mContext);
                mDialog.setData(text);
            }
            mDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //隐藏弹窗
    public static void dismissLoading() {
        try {
            if (mDialog != null) {
                if (mDialog.isShowing()) {
                    mDialog.dismiss();
                    mDialog = null;
                } else {
                    mDialog = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //隐藏弹窗
    public static void dismissLoadingWithoutAnim() {
        try {

            if (mDialog != null) {
                if (mDialog.isShowing()) {
                    mDialog.dismiss();
                    mDialog = null;
                } else {
                    mDialog = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
