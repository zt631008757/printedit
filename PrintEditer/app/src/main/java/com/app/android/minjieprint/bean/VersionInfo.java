package com.app.android.minjieprint.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2019/1/24.
 */

public class VersionInfo implements Serializable {
    public String newestVersion;// ": "1.0.8",
    public String packageUrl;// ": "http://test-game-play.oss-cn-hangzhou.aliyuncs.com/temp/2019/1/24/4a21aa7a14104e72a81e87a71603dfba.apk",
    public String packageSize;// ": 12,
    public int isConstraint;// ": 1,
    public String updateNotes;// ": "更新日志",
    public long createTime;// ": "2019-01-24 14:32:28",

}
