package com.app.android.minjieprint.ui.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.bean.CommObjectInfo;
import com.app.android.minjieprint.bean.ElementsInfo;
import com.app.android.minjieprint.bean.TypeFaceInfo;
import com.app.android.minjieprint.dialog.CommSelectDialog;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.tool.Log;
import com.app.android.minjieprint.ui.activity.FontManagerActivity;
import com.app.android.minjieprint.util.AnimUtil;
import com.app.android.minjieprint.util.Util;
import com.google.zxing.BarcodeFormat;
import com.jrummyapps.android.colorpicker.ColorPickerDialog;
import com.jrummyapps.android.colorpicker.ColorPickerDialogListener;

import java.util.ArrayList;
import java.util.List;

/**
 * 属性控件
 */
public class View_PropertyView extends LinearLayout implements View.OnClickListener {
    public View_PropertyView(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public View_PropertyView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    public View_PropertyView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    Context mContext;
    CommCallBack callBack;
    BaseElementView baseElementView;
    ElementsInfo elementsInfo;


    ImageView iv_close;
    LinearLayout ll_root, ll_text, ll_location, ll_tiaoma, ll_erweima, ll_rect, ll_line, ll_datetime;

    Comm_PropItemView cpv_fontname, cpv_fontsize, cpv_font_space, cpv_gravity, cpv_italic, cpv_underscore, cpv_deleteline, cpv_rotate, cpv_posion_x, cpv_posion_y,
            cpv_posion_width, cpv_posion_height, cpv_color, cpv_hastext, cpv_correction, cpv_rectstyle, cpv_fill, cpv_borderwidth, cpv_radius, cpv_linestyle, cpv_linepadding, cpv_codetype,
            cpv_datetime_format_date, cpv_datetime_format_time, cpv_datetime_date_count;


    private void init() {
        View.inflate(mContext, R.layout.view_action_property, this);
        iv_close = findViewById(R.id.iv_close);
        ll_root = findViewById(R.id.ll_root);

        ll_text = findViewById(R.id.ll_text);
        ll_location = findViewById(R.id.ll_location);
        ll_tiaoma = findViewById(R.id.ll_tiaoma);
        ll_erweima = findViewById(R.id.ll_erweima);
        ll_rect = findViewById(R.id.ll_rect);
        ll_line = findViewById(R.id.ll_line);
        ll_datetime = findViewById(R.id.ll_datetime);

        //通用
        cpv_color = findViewById(R.id.cpv_color);

        //文本
        cpv_fontname = findViewById(R.id.cpv_fontname);
        cpv_fontsize = findViewById(R.id.cpv_fontsize);
        cpv_font_space = findViewById(R.id.cpv_font_space);
        cpv_gravity = findViewById(R.id.cpv_gravity);
        cpv_italic = findViewById(R.id.cpv_italic);
        cpv_underscore = findViewById(R.id.cpv_underscore);
        cpv_deleteline = findViewById(R.id.cpv_deleteline);

        //条码
        cpv_hastext = findViewById(R.id.cpv_hastext);
        cpv_codetype = findViewById(R.id.cpv_codetype);

        //二维码
        cpv_correction = findViewById(R.id.cpv_correction);

        //形状
        cpv_rectstyle = findViewById(R.id.cpv_rectstyle);
        cpv_fill = findViewById(R.id.cpv_fill);
        cpv_borderwidth = findViewById(R.id.cpv_borderwidth);
        cpv_radius = findViewById(R.id.cpv_radius);

        //线条
        cpv_linestyle = findViewById(R.id.cpv_linestyle);
        cpv_linepadding = findViewById(R.id.cpv_linepadding);

        //日期
        cpv_datetime_format_date = findViewById(R.id.cpv_datetime_format_date);
        cpv_datetime_format_time = findViewById(R.id.cpv_datetime_format_time);
        cpv_datetime_date_count = findViewById(R.id.cpv_datetime_date_count);


        //位置
        cpv_rotate = findViewById(R.id.cpv_rotate);
        cpv_posion_x = findViewById(R.id.cpv_posion_x);
        cpv_posion_y = findViewById(R.id.cpv_posion_y);
        cpv_posion_width = findViewById(R.id.cpv_posion_width);
        cpv_posion_height = findViewById(R.id.cpv_posion_height);


        //绑定事件
        cpv_fontname.setOnClickListener(this);
        cpv_codetype.setOnClickListener(this);
        cpv_color.setOnClickListener(this);
        iv_close.setOnClickListener(this);
        ll_root.setOnClickListener(this);
        cpv_datetime_format_date.setOnClickListener(this);
        cpv_datetime_format_time.setOnClickListener(this);


        //文本
        cpv_fontsize.setCallBack(new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                elementsInfo.fontsize = (int) obj;
                baseElementView.refresh();
            }
        });
        cpv_font_space.setCallBack(new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                elementsInfo.kerning = (int) obj;
                baseElementView.refresh();
            }
        });
        cpv_gravity.setCallBack(new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                elementsInfo.alignment = (int) obj;
                baseElementView.refresh();
            }
        });
        cpv_italic.setCallBack(new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                elementsInfo.italic = (boolean) obj;
                baseElementView.refresh();
            }
        });
        cpv_underscore.setCallBack(new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                elementsInfo.underscore = (boolean) obj;
                baseElementView.refresh();
            }
        });
        cpv_deleteline.setCallBack(new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                elementsInfo.deleteline = (boolean) obj;
                baseElementView.refresh();
            }
        });


        //通用
        cpv_posion_x.setCallBack(new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                elementsInfo.xpos = (int) obj;
                baseElementView.refresh();
            }
        });
        cpv_posion_y.setCallBack(new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                elementsInfo.ypos = (int) obj;
                baseElementView.refresh();
            }
        });
        cpv_posion_width.setCallBack(new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                elementsInfo.width = (int) obj;
                baseElementView.refresh();
            }
        });
        cpv_posion_height.setCallBack(new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                elementsInfo.height = (int) obj;
                baseElementView.refresh();
            }
        });
        cpv_rotate.setCallBack(new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                elementsInfo.orientation = (int) obj;
                baseElementView.refresh();
            }
        });
        //条码
        cpv_hastext.setCallBack(new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                elementsInfo.hastext = (boolean) obj;
                baseElementView.refresh();
            }
        });
        cpv_codetype.setCallBack(new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                elementsInfo.codetype = (String) obj;
                baseElementView.refresh();
            }
        });
        //二维码
        cpv_correction.setCallBack(new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                elementsInfo.correction = (int) obj;
                baseElementView.refresh();
            }
        });
        //矩形
        cpv_rectstyle.setCallBack(new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                elementsInfo.rectStyle = (int) obj;
                baseElementView.refresh();
            }
        });
        cpv_fill.setCallBack(new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                elementsInfo.fill = (boolean) obj;
                baseElementView.refresh();
            }
        });
        cpv_borderwidth.setCallBack(new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                elementsInfo.borderwidth = (int) obj;
                baseElementView.refresh();
            }
        });
        cpv_radius.setCallBack(new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                elementsInfo.radius = (int) obj;
                baseElementView.refresh();
            }
        });
        //线条
        cpv_linestyle.setCallBack(new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                elementsInfo.lineStyle = (int) obj;
                baseElementView.refresh();
            }
        });
        cpv_linepadding.setCallBack(new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                elementsInfo.padding = (int) obj;
                baseElementView.refresh();
            }
        });
        //日期
//        cpv_datetime_format_date.setCallBack(new CommCallBack() {
//            @Override
//            public void onResult(Object obj) {
//                elementsInfo.format_date = (String) obj;
//                baseElementView.refresh();
//            }
//        });
//        cpv_datetime_format_time.setCallBack(new CommCallBack() {
//            @Override
//            public void onResult(Object obj) {
//                elementsInfo.format_time = (String) obj;
//                baseElementView.refresh();
//            }
//        });
        cpv_datetime_date_count.setCallBack(new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                elementsInfo.date_migration = (int) obj;
                baseElementView.refresh();
            }
        });
    }

    public void setCallBack(CommCallBack callBack) {
        this.callBack = callBack;
    }

    public void setElementsInfo(BaseElementView baseElementView) {
        this.baseElementView = baseElementView;
        this.elementsInfo = baseElementView.elementView_base.elementsInfo;
        bindUI();
    }

    public void bindUI() {
        if (baseElementView == null) return;
        ll_text.setVisibility(GONE);
        ll_location.setVisibility(GONE);
        ll_tiaoma.setVisibility(GONE);
        ll_erweima.setVisibility(GONE);
        ll_rect.setVisibility(GONE);
        ll_line.setVisibility(GONE);
        ll_datetime.setVisibility(GONE);
        //1:文本  2：条码  3：二维码  4：图片  5：Logo   6：线条  7 形状   8:序列号   9：时间  10 扫码  11 语音识别  12 图像识别
        switch (elementsInfo.viewtype) {
            case "文本":
            case "时间":
            case "序列号":   //序列号

                ll_text.setVisibility(VISIBLE);
                cpv_fontname.setContext(elementsInfo.fontname);
                cpv_fontsize.setContext(elementsInfo.fontsize + "");
                cpv_font_space.setContext(elementsInfo.kerning + "");
                cpv_gravity.setSelectValue(elementsInfo.alignment);
                cpv_italic.setSwitchChecked(elementsInfo.italic);
                cpv_underscore.setSwitchChecked(elementsInfo.underscore);
                cpv_deleteline.setSwitchChecked(elementsInfo.deleteline);

                switch (elementsInfo.viewtype) {
                    case "时间":
                        ll_datetime.setVisibility(VISIBLE);
                        cpv_datetime_format_date.setContext(elementsInfo.format_date);
                        cpv_datetime_format_time.setContext(elementsInfo.format_time);
                        cpv_datetime_date_count.setContext(elementsInfo.date_migration + "");
                        break;
                }

                break;
            case "条码":
                ll_tiaoma.setVisibility(VISIBLE);
                cpv_hastext.setSwitchChecked(elementsInfo.hastext);
                cpv_codetype.setContext(elementsInfo.codetype);
                break;
            case "二维码":
                ll_erweima.setVisibility(VISIBLE);
                cpv_correction.setSelectValue(elementsInfo.correction);
                break;
            case "形状":
                ll_rect.setVisibility(VISIBLE);
                cpv_rectstyle.setSelectValue(elementsInfo.rectStyle);
                cpv_fill.setSwitchChecked(elementsInfo.fill);
                cpv_borderwidth.setContext(elementsInfo.borderwidth + "");
                cpv_radius.setContext(elementsInfo.radius + "");
                if (elementsInfo.rectStyle == 1)  //圆角矩形
                {
                    cpv_radius.setVisibility(VISIBLE);
                } else {
                    cpv_radius.setVisibility(GONE);
                }
                break;
            case "线条":
                ll_line.setVisibility(VISIBLE);
                cpv_linestyle.setSelectValue(elementsInfo.lineStyle);
                cpv_linepadding.setContext(elementsInfo.padding + "");
                if (elementsInfo.lineStyle == 1)  //虚线
                {
                    cpv_linepadding.setVisibility(VISIBLE);
                } else {
                    cpv_linepadding.setVisibility(GONE);
                }
                break;
        }

        //通用
        cpv_color.setContext(elementsInfo.color);
        try {
            cpv_color.tv_content.setTextColor(Color.parseColor(elementsInfo.color));
        } catch (Exception e) {
            cpv_color.tv_content.setTextColor(Color.parseColor("#333333"));
        }

        //位置
        ll_location.setVisibility(VISIBLE);

        cpv_rotate.setSelectValue(elementsInfo.orientation);
        cpv_posion_x.setContext(elementsInfo.xpos + "");
        cpv_posion_y.setContext(elementsInfo.ypos + "");
        cpv_posion_y.setNumber_minValue(4);
        cpv_posion_width.setContext(elementsInfo.width + "");
        cpv_posion_height.setContext(elementsInfo.height + "");

        //
    }


    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.iv_close:
                close();
                break;
            case R.id.ll_root:

                break;
            case R.id.cpv_color:
                openColorDialog();
                break;
            case R.id.cpv_fontname:  //选择字体
                intent = new Intent(mContext, FontManagerActivity.class);
                mContext.startActivity(intent);
                FontManagerActivity.callBack = new CommCallBack() {
                    @Override
                    public void onResult(Object obj) {
                        Log.i("onResult");
                        TypeFaceInfo typeFaceInfo = (TypeFaceInfo) obj;
                        elementsInfo.fontname = typeFaceInfo.name;
                        baseElementView.refresh();
                    }
                };
                break;
            case R.id.cpv_codetype:   //选择条码类型
                BarcodeFormat[] formatList = {BarcodeFormat.CODE_39, BarcodeFormat.CODE_93, BarcodeFormat.CODE_128, BarcodeFormat.EAN_8, BarcodeFormat.EAN_13, BarcodeFormat.UPC_A, BarcodeFormat.UPC_E};
                List<CommObjectInfo> list = new ArrayList<>();
                int select = 0;
                for (int i = 0; i < formatList.length; i++) {
                    BarcodeFormat format = formatList[i];
                    CommObjectInfo objectInfo = new CommObjectInfo(i + "", format.name());
                    list.add(objectInfo);
                    if (elementsInfo.codetype.equals(format.name())) {
                        select = i;
                    }
                    Log.i("format.name:" + format.name());
                }
                CommSelectDialog dialog = new CommSelectDialog(mContext);
                dialog.setData(list, select, new CommCallBack() {
                    @Override
                    public void onResult(Object obj) {
                        CommObjectInfo selectObj = (CommObjectInfo) obj;
                        elementsInfo.codetype = selectObj.name;
                        baseElementView.refresh();
                    }
                });
                dialog.show();
                break;
            case R.id.cpv_datetime_format_date:   //选择日期格式
                List<CommObjectInfo> list1 = new ArrayList<>();
                list1.add(new CommObjectInfo("", "yyyy-MM-dd"));
                list1.add(new CommObjectInfo("", "yyyy年MM月dd日"));
                list1.add(new CommObjectInfo("", "yyyy年MM月"));
                list1.add(new CommObjectInfo("", "MM月dd日"));
                list1.add(new CommObjectInfo("", "yyyy-MM"));
                list1.add(new CommObjectInfo("", "MM-dd"));
                list1.add(new CommObjectInfo("", "yyyy/MM/dd"));
                list1.add(new CommObjectInfo("", "yyyy/MM"));
                list1.add(new CommObjectInfo("", "MM/dd"));
                list1.add(new CommObjectInfo("", "MM-dd-yyyy"));
                list1.add(new CommObjectInfo("", "dd-MM-yyyy"));
                int select1 = 0;
                for (int i = 0; i < list1.size(); i++) {
                    if (elementsInfo.format_date.equals(list1.get(i).name)) {
                        select1 = i;
                    }
                }
                CommSelectDialog dialog1 = new CommSelectDialog(mContext);
                dialog1.setData(list1, select1, new CommCallBack() {
                    @Override
                    public void onResult(Object obj) {
                        CommObjectInfo selectObj = (CommObjectInfo) obj;
                        elementsInfo.format_date = selectObj.name;
                        baseElementView.refresh();
                    }
                });
                dialog1.show();
                break;
            case R.id.cpv_datetime_format_time:   //选择时间格式
                List<CommObjectInfo> list2 = new ArrayList<>();
                list2.add(new CommObjectInfo("", "HH:mm:ss"));
                list2.add(new CommObjectInfo("", "HH:mm"));
                list2.add(new CommObjectInfo("", "mm:ss"));
                int select2 = 0;
                for (int i = 0; i < list2.size(); i++) {
                    if (elementsInfo.format_time.equals(list2.get(i).name)) {
                        select1 = i;
                    }
                }
                CommSelectDialog dialog2 = new CommSelectDialog(mContext);
                dialog2.setData(list2, select2, new CommCallBack() {
                    @Override
                    public void onResult(Object obj) {
                        CommObjectInfo selectObj = (CommObjectInfo) obj;
                        elementsInfo.format_time = selectObj.name;
                        baseElementView.refresh();
                    }
                });
                dialog2.show();
                break;
        }
    }

    //显示
    public void show() {
        setVisibility(VISIBLE);
        AnimUtil.enterFromBottom(this);
    }

    //关闭
    public void close() {
        AnimUtil.outToBottom(this, new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                setVisibility(GONE);
            }
        });
    }

    //显示颜色选择器
    public void showSelectColor() {
        cpv_color.performClick();
    }

    public static final int DIALGE_ID = 0;

    private void openColorDialog() {
        int color = Color.parseColor(elementsInfo.color);
        int red = (color & 0xff0000) >> 16;
        int green = (color & 0x00ff00) >> 8;
        int blue = (color & 0x0000ff);
        //传入的默认color
        ColorPickerDialog colorPickerDialog = ColorPickerDialog.newBuilder().setColor(color)
                //设置dialog标题
                .setDialogType(ColorPickerDialog.TYPE_CUSTOM)
                //设置为自定义模式
                .setShowAlphaSlider(false)
                //设置有透明度模式，默认没有透明度
                .setDialogId(DIALGE_ID)
                //设置Id,回调时传回用于判断
                .setAllowPresets(false)
                .setShowColorShades(false)
                //不显示预知模式
                .create();
        //Buider创建
        colorPickerDialog.setColorPickerDialogListener(new ColorPickerDialogListener() {
            @Override
            public void onColorSelected(int dialogId, int color) {
                if (dialogId == DIALGE_ID) {
                    elementsInfo.color = Util.toHexEncoding(color);
                    Log.i("elementsInfo.color:" + elementsInfo.color);
                    baseElementView.refresh();
                }
            }

            @Override
            public void onDialogDismissed(int dialogId) {

            }
        });
        //设置回调，用于获取选择的颜色
        colorPickerDialog.show(((Activity) getContext()).getFragmentManager(), "color-picker-dialog");
    }

}
