package com.app.android.minjieprint.constant;

/**
 * Created by zt on 2018/8/13.
 * 所有接口地址放这里（相对路径）
 */

public class ApiConstants {
    //登录
    public static final String checkmobile = "/users/checkmobile/";   //发验证码
    public static final String smsregister = "/users/smsregister/";   //注册
    public static final String login = "/users/login/";   //登录
    public static final String reset = "/users/reset/";   //修改密码
    public static final String docs = "/docs/";             //获取文档

}
