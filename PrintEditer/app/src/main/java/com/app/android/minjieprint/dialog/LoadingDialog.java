package com.app.android.minjieprint.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.util.AnimUtil;
import com.app.android.minjieprint.util.StatusBarUtil_Dialog;

/**
 * Created by Administrator on 2018/8/29.
 */

public class LoadingDialog extends Dialog implements View.OnClickListener {
    public LoadingDialog(@NonNull Context context) {
        super(context, R.style.myDialog);
    }

    public LoadingDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
    }

    protected LoadingDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    LinearLayout ll_content;
    View view_bg;
    TextView tv_text;

    CommCallBack callBack;
    String text;

    public void setData(String text) {
        this.text = text;
    }

    //设置输入回调
    public void setIntputCallBack(CommCallBack callBack) {
        this.callBack = callBack;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_commloading);
        initView();

        StatusBarUtil_Dialog.setImmersiveStatusBar(this, true);
    }

    private void initView() {
        ll_content = (LinearLayout) findViewById(R.id.ll_content);
        view_bg = findViewById(R.id.view_bg);
        tv_text = findViewById(R.id.tv_text);
        tv_text.setText(text);

        view_bg.setOnClickListener(this);
        ll_content.setOnClickListener(this);

        AnimUtil.fadeIn(view_bg);
        AnimUtil.fadeIn(ll_content);

        if (!TextUtils.isEmpty(text)) {
            tv_text.setVisibility(View.VISIBLE);
            tv_text.setText(text);
        } else {
            tv_text.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.view_bg:
                dismissWithAnim();
                break;
            case R.id.ll_content:
                //不处理，只消耗点击事件
                break;
        }
    }

    public void dismissWithAnim() {
        if (view_bg.getAnimation() != null) return;
        AnimUtil.fadeOut(ll_content, null);
        AnimUtil.fadeOut(view_bg, new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        dismissWithAnim();
    }
}
