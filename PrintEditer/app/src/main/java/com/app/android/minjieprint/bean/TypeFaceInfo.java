package com.app.android.minjieprint.bean;

public class TypeFaceInfo {
    public String name;
    public String filePath;

    public TypeFaceInfo(String name, String filePath) {
        this.name = name;
        this.filePath = filePath;
    }
}
