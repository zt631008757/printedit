package com.app.android.minjieprint.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2018/9/20.
 */

public class CommObjectInfo implements Serializable {
    public String id;
    public String name;

    public boolean isSelect;

    public CommObjectInfo() {
    }

    public CommObjectInfo(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
