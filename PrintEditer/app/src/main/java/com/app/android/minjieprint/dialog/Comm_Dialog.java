package com.app.android.minjieprint.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.util.AnimUtil;
import com.app.android.minjieprint.util.StatusBarUtil_Dialog;

/**
 * Created by Administrator on 2018/8/29.
 */

public class Comm_Dialog extends Dialog implements View.OnClickListener {
    public Comm_Dialog(@NonNull Context context) {
        super(context, R.style.myDialog);
    }

    public Comm_Dialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
    }

    protected Comm_Dialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public static Dialog showCommDialog(Context context, String title, String msg, String btnOkStr, String btnCancelStr, CommCallBack okClickListener, CommCallBack cancelClickListener) {
        Comm_Dialog dialog = new Comm_Dialog(context);
        dialog.setData(title, msg, btnOkStr, btnCancelStr);
        dialog.setCallBack(okClickListener, cancelClickListener);
        dialog.show();
        return dialog;
    }

    LinearLayout ll_content;
    View view_bg;
    TextView tv_title, content_text, pop_ok, pop_cancel, pop_comit;

    CommCallBack okClickListener;
    CommCallBack cancelClickListener;
    String title, btnOkStr, btnCancelStr, msg,des;

    //设置回调
    private void setCallBack(CommCallBack okClickListener, CommCallBack cancelClickListener) {
        this.cancelClickListener = cancelClickListener;
        this.okClickListener = okClickListener;
    }

    private void setData(String title, String msg, String btnOkStr, String btnCancelStr) {
        this.title = title;
        this.btnOkStr = btnOkStr;
        this.btnCancelStr = btnCancelStr;
        this.msg = msg;
    }

    private void setData(String title, String msg,String des, String btnOkStr, String btnCancelStr) {
        this.title = title;
        this.btnOkStr = btnOkStr;
        this.btnCancelStr = btnCancelStr;
        this.msg = msg;
        this.des = des;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_comm);
        initView();
        StatusBarUtil_Dialog.setImmersiveStatusBar(this, false);
    }

    private void initView() {
        ll_content = findViewById(R.id.ll_content);
        view_bg = findViewById(R.id.view_bg);
        view_bg.setOnClickListener(this);
        ll_content.setOnClickListener(this);
        AnimUtil.fadeIn(view_bg);
        AnimUtil.fadeIn(ll_content);

        tv_title = findViewById(R.id.tv_title); //标题
        content_text = findViewById(R.id.content_text);  //提示文字
        pop_ok = findViewById(R.id.pop_ok);  //单个按钮文字
        pop_cancel = findViewById(R.id.pop_cancel);  //两个按钮 左边取消按钮
        pop_comit = findViewById(R.id.pop_comit);  //两个按钮 左边取消按钮

        content_text.setMovementMethod(ScrollingMovementMethod.getInstance());
        if (!TextUtils.isEmpty(title)) {
            tv_title.setVisibility(View.VISIBLE);
            tv_title.setText(title);
        } else {
            tv_title.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(msg)) {
//                if (msg.length() < 15) {
//                    content_text.setGravity(Gravity.CENTER);
//                } else {
//                    content_text.setGravity(Gravity.LEFT);
//                }
            content_text.setText(msg);
        }
        if (!TextUtils.isEmpty(btnOkStr) && TextUtils.isEmpty(btnCancelStr))    //只有一个按钮
        {
            pop_ok.setVisibility(View.VISIBLE);
            pop_cancel.setVisibility(View.GONE);
            pop_comit.setVisibility(View.GONE);
            pop_ok.setText(btnOkStr);
        } else if (!TextUtils.isEmpty(btnOkStr) && !TextUtils.isEmpty(btnCancelStr))   //有两个按钮
        {
            pop_ok.setVisibility(View.GONE);
            pop_cancel.setVisibility(View.VISIBLE);
            pop_comit.setVisibility(View.VISIBLE);
            pop_cancel.setText(btnCancelStr);
            pop_comit.setText(btnOkStr);
        }

        pop_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissWithAnim();
                if (okClickListener != null)
                    okClickListener.onResult(null);
            }
        });
        pop_comit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissWithAnim();
                if (okClickListener != null)
                    okClickListener.onResult(null);
            }
        });
        pop_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissWithAnim();
                if (cancelClickListener != null)
                    cancelClickListener.onResult(null);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.view_bg:
                dismissWithAnim();
                break;
            case R.id.ll_content:
                //不处理，只消耗点击事件
                break;
        }
    }

    public void dismissWithAnim() {
        if (view_bg.getAnimation() != null) return;
        AnimUtil.fadeOut(ll_content, null);
        AnimUtil.fadeOut(view_bg, new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        dismissWithAnim();
    }
}
