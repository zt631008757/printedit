package com.app.android.minjieprint.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.app.android.minjieprint.bean.ElementsInfo;
import com.app.android.minjieprint.tool.Log;

public abstract class ElementView_Base extends RelativeLayout {
    static final long DOUBLE_TIME = 300;

    public ElementView_Base(Context context, ElementsInfo elementsInfo) {
        super(context);
        this.elementsInfo = elementsInfo;
        this.mContext = context;
    }

    public ElementView_Base(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ElementView_Base(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ElementsInfo elementsInfo;
    public Context mContext;

    //刷新控件
    public void refreshUI() {
//        setBackgroundColor(Color.parseColor("#ff00ff"));
    }

    //    @Override
//    protected void dispatchDraw(Canvas canvas) {
//        super.dispatchDraw(canvas);
//        Log.i("dispatchDraw");
//    }
//
//    @Override
//    protected void onLayout(boolean changed, int l, int t, int r, int b) {
//        super.onLayout(changed, l, t, r, b);
//        Log.i("onLayout");
//    }
//
//
//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//        Log.i("onMeasure");
//    }

}
