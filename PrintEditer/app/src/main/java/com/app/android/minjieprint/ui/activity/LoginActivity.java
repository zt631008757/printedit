package com.app.android.minjieprint.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.constant.SPConstants;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.interface_.OkHttpCallBack;
import com.app.android.minjieprint.manager.API_LoginManager;
import com.app.android.minjieprint.manager.UserManager;
import com.app.android.minjieprint.responce.BaseResponce;
import com.app.android.minjieprint.responce.LoginResponce;
import com.app.android.minjieprint.tool.CommLoading;
import com.app.android.minjieprint.tool.CommToast;
import com.app.android.minjieprint.tool.SPUtil;
import com.app.android.minjieprint.ui.view.Comm_EditView;
import com.app.android.minjieprint.ui.view.Comm_SubmitBtnView;
import com.app.android.minjieprint.ui.view.MultiStateView;
import com.app.android.minjieprint.util.Util;

/**
 * Created by Administrator on 2018/6/20.
 */

public class LoginActivity extends BaseActivity {

    public static boolean isOpen = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isOpen = false;
    }

    MultiStateView multiplestatusView;
    TextView tv_regist, tv_fogetpwd, tv_xieyi;
    Comm_SubmitBtnView tv_login;

    Comm_EditView et_account, et_pwd;

    //初始化控件
    private void initView() {
        setTitle(Util.getString(R.string.login_title));
        setLeftImgClickListener();

        multiplestatusView = findViewById(R.id.multiplestatusView);
//        multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);    //页面初始化，默认为加载中状态
        multiplestatusView.setOnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
            }
        });

        tv_regist = findViewById(R.id.tv_regist);
        tv_login = findViewById(R.id.tv_login);
        tv_fogetpwd = findViewById(R.id.tv_fogetpwd);
        et_account = findViewById(R.id.et_account);
        et_pwd = findViewById(R.id.et_pwd);
        tv_xieyi = findViewById(R.id.tv_xieyi);

        tv_regist.setOnClickListener(this);
        tv_login.setOnClickListener(this);
        tv_fogetpwd.setOnClickListener(this);
        tv_xieyi.setOnClickListener(this);

        et_account.setInputCallBack(inputCallback);
        et_pwd.setInputCallBack(inputCallback);
        checkInput();

        String account = SPUtil.getStringValue(mContext, SPConstants.Phone, "");
        if(!TextUtils.isEmpty(account))
        {
            et_account.setText(account);
        }

    }

    CommCallBack inputCallback = new CommCallBack() {
        @Override
        public void onResult(Object obj) {
            checkInput();
        }
    };

    private void checkInput() {
        tv_login.setEnabled(false);
        if (TextUtils.isEmpty(et_account.getText())) {
            return;
        }
        if (TextUtils.isEmpty(et_pwd.getText())) {
            return;
        }
        tv_login.setEnabled(true);
    }

    private void login() {
        final String account = et_account.getText().toString();
        final String pwd = et_pwd.getText().toString();

        if (TextUtils.isEmpty(account)) {
            CommToast.showToast(mContext, "请输入账号");
            et_account.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(pwd)) {
            CommToast.showToast(mContext, "请输入密码");
            et_pwd.requestFocus();
            return;
        }

        CommLoading.showLoading(mContext);
        API_LoginManager.login(mContext, account, pwd, new OkHttpCallBack() {
            @Override
            public void onSuccess(BaseResponce baseResponce) {
                CommLoading.dismissLoading();
                LoginResponce responce = (LoginResponce) baseResponce;
                if (BaseResponce.Status_Success.equals(responce.data.code)) {
                    UserManager.saveToken(mContext, responce.data.uid);
                    SPUtil.putValue(mContext, SPConstants.Phone, account);
                    CommToast.showToast(mContext, "登录成功");
                    finish();
                } else {
                    CommToast.showToast(mContext, responce.data.msg);
                }
            }

            @Override
            public void onFailure(BaseResponce baseResponce) {
                CommLoading.dismissLoading();
                CommToast.showToast(mContext,"网络连接失败，请重试");
            }
        });
    }

    //点击事件
    @Override
    public void onClick(View view) {
        super.onClick(view);
        Intent intent;
        switch (view.getId()) {
            case R.id.tv_regist:
                intent = new Intent(mContext, Regist_SubmitActivity.class);
                intent.putExtra("typeName", 0);
                startActivity(intent);
                break;
            case R.id.tv_login:   //登录
                login();
                break;
            case R.id.tv_fogetpwd:  //忘记密码
                intent = new Intent(mContext, Regist_SubmitActivity.class);
                intent.putExtra("typeName", 1);
                startActivity(intent);
                break;
        }
    }
}
