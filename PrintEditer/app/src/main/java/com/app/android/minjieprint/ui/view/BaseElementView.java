package com.app.android.minjieprint.ui.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.LayoutDirection;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.event.Event_ActionChange;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.util.Util;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Robert on 2017/6/20.
 */

public class BaseElementView extends RelativeLayout {

    public CommCallBack callBack;
    public ElementView_Base elementView_base;

    public void setCallBack(CommCallBack callBack) {
        this.callBack = callBack;
    }

    boolean isSelected = false;

    //选中当前view
    public void setSelect(boolean isSelected) {
        this.isSelected = isSelected;
        RelativeLayout change_bg = findViewById(R.id.change_bg);
        ImageView iv_move_right = findViewById(R.id.iv_move_right);
        ImageView iv_move_bottom = findViewById(R.id.iv_move_bottom);

        if (change_bg != null) {
            if (isSelected) {
                change_bg.setBackgroundResource(R.drawable.shape_baseview_bg);
                iv_move_right.setVisibility(VISIBLE);
                iv_move_bottom.setVisibility(VISIBLE);
            } else {
                change_bg.setBackgroundColor(getResources().getColor(R.color.transparent));
                iv_move_right.setVisibility(GONE);
                iv_move_bottom.setVisibility(GONE);
            }
        }
    }

    public void refresh() {
        oriLeft = elementView_base.elementsInfo.xpos;
        oriTop = elementView_base.elementsInfo.ypos;
        oriRight = elementView_base.elementsInfo.xpos + elementView_base.elementsInfo.width - Util.dip2px(getContext(), 10);
        oriBottom = elementView_base.elementsInfo.ypos + elementView_base.elementsInfo.height - Util.dip2px(getContext(), 10);

        elementView_base.refreshUI();
        move();
    }

    //旋转
    public void rotate() {
        RelativeLayout change_bg = findViewById(R.id.change_bg);
        RelativeLayout add_your_view_here = findViewById(R.id.add_your_view_here);
        if (elementView_base.elementsInfo.orientation == 0) {
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(getHeight() - 30, getWidth() - 30);
            add_your_view_here.setLayoutParams(lp);
            change_bg.setGravity(Gravity.LEFT | Gravity.TOP);
        } else if (elementView_base.elementsInfo.orientation == 1) {
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(getWidth() - 30, getHeight() - 30);
            add_your_view_here.setLayoutParams(lp);
            change_bg.setGravity(Gravity.TOP | Gravity.RIGHT);
        } else if (elementView_base.elementsInfo.orientation == 2) {
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(getHeight() - 30, getWidth() - 30);
            add_your_view_here.setLayoutParams(lp);
            change_bg.setGravity(Gravity.BOTTOM | Gravity.RIGHT);
        } else if (elementView_base.elementsInfo.orientation == 3) {
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(getWidth() - 30, getHeight() - 30);
            add_your_view_here.setLayoutParams(lp);
            change_bg.setGravity(Gravity.BOTTOM | Gravity.LEFT);
        }


        add_your_view_here.setRotation(elementView_base.elementsInfo.orientation * 90);
        refresh();

    }

    //重写这个方法，并且在方法里面请求所有的父控件都不要拦截他的事件
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        getParent().requestDisallowInterceptTouchEvent(true);
        return super.dispatchTouchEvent(ev);
    }


    private int dragDirection = 0;
    private static final int TOP = 1;
    private static final int LEFT = 2;
    private static final int BOTTOM = 3;
    private static final int RIGHT = 4;
    private static final int LEFT_TOP = 5;
    private static final int RIGHT_TOP = 6;
    private static final int LEFT_BOTTOM = 7;
    private static final int RIGHT_BOTTOM = 8;
    private static final int CENTER = 9;


    private int lastX;
    private int lastY;

    private int screenWidth;
    private int screenHeight;

    public int oriLeft;
    public int oriRight;
    public int oriTop;
    public int oriBottom;

    /**
     * 标示此类的每个实例的id
     */
    private int identity = 0;


    /**
     * 触控区域设定
     */
    private int touchAreaLength = 60;

    private int minHeight = 90;
    private int minWidth = 90;

    public BaseElementView(Context context) {
        super(context);
        init();
    }

    public BaseElementView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BaseElementView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();

    }

    private void init() {
        screenHeight = 500;//getResources().getDisplayMetrics().heightPixels - 40;
        screenWidth = 500;// getResources().getDisplayMetrics().widthPixels;

    }

    public void setViewWidthHeight(int width, int height) {
        screenWidth = width;
        screenHeight = height;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                if (callBack != null) {
                    callBack.onResult(this);
                }

                oriLeft = getLeft();
                oriRight = getRight();
                oriTop = getTop();
                oriBottom = getBottom();

                lastY = (int) event.getRawY();
                lastX = (int) event.getRawX();
                dragDirection = getDirection((int) event.getX(), (int) event.getY());
                break;
            case MotionEvent.ACTION_UP:
                //      Log.d(TAG, "onTouchEvent: up");
//                spotL = false;
//                spotT = false;
//                spotR = false;
//                spotB = false;
//                requestLayout();
//                mDeleteView.setVisibility(View.INVISIBLE);
                // invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                // Log.d(TAG, "onTouchEvent: move");
                int tempRawX = (int) event.getRawX();
                int tempRawY = (int) event.getRawY();

                int dx = tempRawX - lastX;
                int dy = tempRawY - lastY;
                lastX = tempRawX;
                lastY = tempRawY;

                switch (dragDirection) {
                    case LEFT:
                        left(dx);
                        break;
                    case RIGHT:
                        right(dx);
                        break;
                    case BOTTOM:
                        bottom(dy);
                        break;
                    case TOP:
                        top(dy);
                        break;
                    case CENTER:
                        center(dx, dy);
                        break;
//                    case LEFT_BOTTOM:
//                        left( dx);
//                        bottom( dy);
//                        break;
//                    case LEFT_TOP:
//                        left( dx);
//                        top(dy);
//                        break;
//                    case RIGHT_BOTTOM:
//                        right( dx);
//                        bottom( dy);
//                        break;
//                    case RIGHT_TOP:
//                        right( dx);
//                        top( dy);
//                        break;
                }

                move();
                break;
        }
        return super.onTouchEvent(event);
    }

    public void move() {
        //new pos l t r b is set into oriLeft, oriTop, oriRight, oriBottom
        int width = oriRight - oriLeft;
        int height = oriBottom - oriTop;
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(width, height);
//        LayoutParams lp = (LayoutParams) getLayoutParams();
        lp.setMargins(oriLeft, oriTop, 0, 0);
        setLayoutParams(lp);

        elementView_base.elementsInfo.xpos = oriLeft;
        elementView_base.elementsInfo.ypos = oriTop;
        elementView_base.elementsInfo.width = width + Util.dip2px(getContext(), 10);
        elementView_base.elementsInfo.height = height + Util.dip2px(getContext(), 10);

        EventBus.getDefault().post(new Event_ActionChange());

//        rotate();
    }


    /**
     * 触摸点为中心->>移动
     */
    public void center(int dx, int dy) {
        int left = getLeft() + dx;
        int top = getTop() + dy;
        int right = getRight() + dx;
        int bottom = getBottom() + dy;

//        if (left < 0) {
//            left = 0;
//            right = left + getWidth();
//        }
//        if (right > screenWidth) {
//            right = screenWidth;
//            left = right - getWidth();
//        }
//        if (top < 4) {
//            top = 4;
//            bottom = top + getHeight();
//        }
//        if (bottom > screenHeight) {
//            bottom = screenHeight;
//            top = bottom - getHeight();
//        }

        oriLeft = left;
        oriTop = top;
        oriRight = right;
        oriBottom = bottom;

    }

    /**
     * 触摸点为上边缘
     */
    public void top(int dy) {
        oriTop += dy;
//        if (oriTop < 0) {
//            oriTop = 0;
//        }
        if (oriBottom - oriTop < minHeight) {
            oriTop = oriBottom - minHeight;
        }
    }

    /**
     * 触摸点为下边缘
     */
    public void bottom(int dy) {

        oriBottom += dy;
//        if (oriBottom > screenHeight) {
//            oriBottom = screenHeight;
//        }
        if (oriBottom - oriTop < minHeight) {
            oriBottom = minHeight + oriTop;
        }
        Log.i("test", "oriBottom:" + oriBottom);
    }

    /**
     * 触摸点为右边缘
     */
    public void right(int dx) {
        oriRight += dx;
//        if (oriRight > screenWidth) {
//            oriRight = screenWidth;
//        }
        if (oriRight - oriLeft < minWidth) {
            oriRight = oriLeft + minWidth;
        }
    }

    /**
     * 触摸点为左边缘
     */
    public void left(int dx) {
        oriLeft += dx;
//        if (oriLeft < 0) {
//            oriLeft = 0;
//        }
        if (oriRight - oriLeft < minWidth) {
            oriLeft = oriRight - minWidth;
        }
    }

    public int getDirection(int x, int y) {
        int left = getLeft();
        int right = getRight();
        int bottom = getBottom();
        int top = getTop();

//        if (xpos < touchAreaLength && ypos < touchAreaLength) {
//            return LEFT_TOP;
//        }
//        if (ypos < touchAreaLength && right - left - xpos < touchAreaLength) {
//            return RIGHT_TOP;
//        }
//        if (xpos < touchAreaLength && bottom - top - ypos < touchAreaLength) {
//            return LEFT_BOTTOM;
//        }
//        if (right - left - xpos < touchAreaLength && bottom - top - ypos < touchAreaLength) {
//            return RIGHT_BOTTOM;
//        }

//        if (x < touchAreaLength) {
//            spotL = true;
//            requestLayout();
//            return LEFT;
//        }
//        if (y < touchAreaLength) {
//            spotT = true;
//            requestLayout();
//            return TOP;
//        }
        if (right - left - x < touchAreaLength) {
            spotR = true;
            requestLayout();
            return RIGHT;
        }
        if (bottom - top - y < touchAreaLength) {
            spotB = true;
            requestLayout();
            return BOTTOM;
        }
        return CENTER;
    }

    private boolean spotL = false;
    private boolean spotT = false;
    private boolean spotR = false;
    private boolean spotB = false;

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);


        RelativeLayout rlt = (RelativeLayout) getChildAt(0);
        int count = rlt.getChildCount();

//        for (int a = 0; a < count; a++) {
//            if (a == 1) {        //l
//                if (spotL)
//                    rlt.getChildAt(a).setVisibility(View.VISIBLE);
//                else
//                    rlt.getChildAt(a).setVisibility(View.INVISIBLE);
//            } else if (a == 2) { //t
//                if (spotT)
//                    rlt.getChildAt(a).setVisibility(View.VISIBLE);
//                else
//                    rlt.getChildAt(a).setVisibility(View.INVISIBLE);
//            } else if (a == 3) { //r
//                if (spotR)
//                    rlt.getChildAt(a).setVisibility(View.VISIBLE);
//                else
//                    rlt.getChildAt(a).setVisibility(View.INVISIBLE);
//            } else if (a == 4) { //b
//                if (spotB)
//                    rlt.getChildAt(a).setVisibility(View.VISIBLE);
//                else
//                    rlt.getChildAt(a).setVisibility(View.INVISIBLE);
//            }
//            // Log.d(TAG, "onLayout: "+rlt.getChildAt(a).getClass().toString());
//        }

    }


    public int getIdentity() {
        return identity;
    }

    public void setIdentity(int identity) {
        this.identity = identity;
    }

    //set the main delete area object (to change visibility)
    private View mDeleteView = null;

    public void setDeleteView(View v) {
        mDeleteView = v;
    }

    //delete listener
    private DeleteMoveLayout mListener = null;

    public interface DeleteMoveLayout {
        void onDeleteMoveLayout(int identity);
    }

    public void setOnDeleteMoveLayout(DeleteMoveLayout l) {
        mListener = l;
    }

}
