package com.app.android.minjieprint.constant;

/**
 * Created by ZT on 2018/8/13.
 * SPUtil key 常量
 */

public class SPConstants {
    //使用相关
    public static final String UserInfo = "UserInfo";   //用户信息
    public static final String Token = "Token";         //用户Token
    public static final String Token_Refresh = "Token_Refresh";         //Token_Refresh
    public static final String Phone = "Phone";         //用户最近一次登陆成功的手机号
    public static final String Cookie = "Cookie";         //Cookie

    public static final String Languge = "Languge";         //Languge

    public static final String FileName_Num = "FileName_Num";   //文件名数字

    public static final String HasAccept = "HasAccept";         //是否同意协议
}
