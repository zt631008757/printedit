package com.app.android.minjieprint.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.bean.ElementsInfo;
import com.app.android.minjieprint.tool.Log;

public class ElementView_Line extends ElementView_Base {

    public ElementView_Line(Context context, ElementsInfo info) {
        super(context, info);
        init();
    }

    @Override
    public void refreshUI() {
        super.refreshUI();
        try {
            paint.setColor(Color.parseColor(elementsInfo.color));
        } catch (Exception e) {
            paint.setColor(Color.parseColor("#333333"));
        }
        paint.setStyle(Paint.Style.STROKE);
        paint.setAntiAlias(true);

        requestLayout();
    }

    Paint paint;

    private void init() {
        paint = new Paint(); //设置一个笔刷大小是3的黄色的画笔
        refreshUI();
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        //线条类型. 0 - 实线, 1 - 虚线
        if (elementsInfo.lineStyle == 0) {
            paint.setPathEffect(new DashPathEffect(new float[]{0, 0}, 0));

        } else if (elementsInfo.lineStyle == 1) {
            paint.setPathEffect(new DashPathEffect(new float[]{elementsInfo.padding * 2, elementsInfo.padding}, 0));
        }

        Path mPath = new Path();

        if (elementsInfo.orientation % 180 == 0) {
            paint.setStrokeWidth(getHeight() - 60);
            mPath.moveTo(0, getHeight() / 2);
            mPath.lineTo(getWidth(), getHeight() / 2);
        } else {
            paint.setStrokeWidth(getWidth() - 60);
            mPath.moveTo(getWidth() / 2,0 );
            mPath.lineTo(getWidth() / 2,getHeight());
        }
        canvas.drawPath(mPath, paint);
    }

//    @Override
//    protected void onDraw(Canvas canvas) {
//        canvas.translate(getMeasuredWidth() * 0.5f, getMeasuredHeight() * 0.5f);
//        //首先偏移在旋转，是因为，如果先旋转，本身xy坐标系也会跟着旋转，之后在偏移会不方便我们的控制，也不直观
//        canvas.rotate(elementsInfo.orientation);
//        super.onDraw(canvas);
//    }
}
