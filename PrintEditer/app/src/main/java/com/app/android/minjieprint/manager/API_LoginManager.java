package com.app.android.minjieprint.manager;

import android.content.Context;

import com.app.android.minjieprint.constant.ApiConstants;
import com.app.android.minjieprint.interface_.OkHttpCallBack;
import com.app.android.minjieprint.responce.CommResponce;
import com.app.android.minjieprint.responce.GetFileResponce;
import com.app.android.minjieprint.responce.LoginResponce;
import com.lzy.okgo.model.HttpMethod;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2018/8/29.
 */

public class API_LoginManager {

    //验证手机号
    public static void checkmobile(Context context, String phone, OkHttpCallBack callBack) {
        String url = Config.SERVER_HOST + ApiConstants.checkmobile;
        Map<String, String> bodyParames = new HashMap<>();
        bodyParames.put("phone", phone);
        bodyParames.put("zone", "86");
        OkHttpManager.okHttpRequest(context, HttpMethod.POST, url, bodyParames, CommResponce.class, callBack);
    }

    //注册
    public static void smsregister(Context context, String phone, String name, String password, String code, OkHttpCallBack callBack) {
        String url = Config.SERVER_HOST + ApiConstants.smsregister;
        Map<String, String> bodyParames = new HashMap<>();
        bodyParames.put("phone", phone);
        bodyParames.put("filename", name);
        bodyParames.put("password", password);
        bodyParames.put("code", code);
        OkHttpManager.okHttpRequest(context, HttpMethod.POST, url, bodyParames, LoginResponce.class, callBack);
    }

    //登录
    public static void login(Context context, String phone, String password, OkHttpCallBack callBack) {
        String url = Config.SERVER_HOST + ApiConstants.login;
        Map<String, String> bodyParames = new HashMap<>();
        bodyParames.put("phone", phone);
        bodyParames.put("password", password);
        OkHttpManager.okHttpRequest(context, HttpMethod.POST, url, bodyParames, LoginResponce.class, callBack);
    }

    //重置密码
    public static void reset(Context context, String phone, String password, String code, OkHttpCallBack callBack) {
        String url = Config.SERVER_HOST + ApiConstants.reset;
        Map<String, String> bodyParames = new HashMap<>();
        bodyParames.put("phone", phone);
        bodyParames.put("password", password);
        bodyParames.put("code", code);
        OkHttpManager.okHttpRequest(context, HttpMethod.POST, url, bodyParames, LoginResponce.class, callBack);
    }

    //获取文档
    public static void docs(Context context, OkHttpCallBack callBack) {
        String url = Config.SERVER_HOST + ApiConstants.docs +"?uid=" + UserManager.getToken(context);
        Map<String, String> bodyParames = new HashMap<>();
        OkHttpManager.okHttpRequest(context, HttpMethod.GET, url, bodyParames, GetFileResponce.class, callBack);
    }



}
