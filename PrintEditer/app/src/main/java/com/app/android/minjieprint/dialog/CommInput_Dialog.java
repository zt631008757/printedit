package com.app.android.minjieprint.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.tool.CommToast;
import com.app.android.minjieprint.util.AnimUtil;
import com.app.android.minjieprint.util.StatusBarUtil_Dialog;

/**
 * Created by Administrator on 2018/8/29.
 */

public class CommInput_Dialog extends Dialog implements View.OnClickListener {
    public CommInput_Dialog(@NonNull Context context) {
        super(context, R.style.myDialog);
    }

    public CommInput_Dialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
    }

    protected CommInput_Dialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    LinearLayout ll_content;
    View view_bg;
    EditText et_input;
    TextView tv_cancel, tv_submit, tv_title;

    CommCallBack callBack;
    String title;
    String hint;
    String defaultValue;
    int length = 10;
    int inputType = 0;

    //设置输入回调
    public void setIntputCallBack(CommCallBack callBack) {
        this.callBack = callBack;
    }

    public void setData(String title, String hint, int length, String defaultValue) {
        this.title = title;
        this.hint = hint;
        this.length = length;
        this.defaultValue = defaultValue;
    }

    public void setInputType(int inputType) {
        this.inputType = inputType;
    }

    //默认文案
    public void setDefault(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_comminput);
        initView();
        StatusBarUtil_Dialog.setImmersiveStatusBar(this, false);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    private void initView() {
        ll_content = (LinearLayout) findViewById(R.id.ll_content);
        view_bg = findViewById(R.id.view_bg);

        AnimUtil.fadeIn(view_bg);
        AnimUtil.fadeIn(ll_content);

        view_bg.setOnClickListener(this);
        ll_content.setOnClickListener(this);

        et_input = findViewById(R.id.et_pwd);
        tv_cancel = findViewById(R.id.tv_cancel);
        tv_submit = findViewById(R.id.tv_submit);
        tv_title = findViewById(R.id.tv_title);

        tv_cancel.setOnClickListener(this);
        tv_submit.setOnClickListener(this);
        et_input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(length)});
        et_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        if (!TextUtils.isEmpty(title)) {
            tv_title.setText(title);
        }
        if (!TextUtils.isEmpty(hint)) {
            et_input.setHint(hint);
        }
        if (!TextUtils.isEmpty(defaultValue)) {
            et_input.setText(defaultValue);
            et_input.setSelection(defaultValue.length());
        }

        if (inputType == 1) {
            et_input.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.view_bg:
                dismissWithAnim();
                break;
            case R.id.ll_content:
                //不处理，只消耗点击事件
                break;
            case R.id.tv_cancel:
                dismissWithAnim();
                break;
            case R.id.tv_submit:
                String input = et_input.getText().toString();
                if (TextUtils.isEmpty(input)) {
                    CommToast.showToast(getContext(), hint);
                    return;
                }
                if (callBack != null) {
                    callBack.onResult(input);
                }
                dismissWithAnim();
                break;
        }
    }

    public void dismissWithAnim() {
        if (view_bg.getAnimation() != null) return;
        AnimUtil.fadeOut(ll_content, null);
        AnimUtil.fadeOut(view_bg, new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        dismissWithAnim();
    }
}
