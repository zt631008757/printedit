package com.app.android.minjieprint.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import com.app.android.minjieprint.R;
import com.app.android.minjieprint.ui.view.MultiStateView;

/**
 * Created by Administrator on 2018/6/20.
 */

public class A_ModelActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_new);
        initView();
        getData();
    }

    MultiStateView multiplestatusView;

    //初始化控件
    private void initView() {
        setTitle("模板");    //设置标题
        setLeftImgClickListener();  //设置左键返回

        multiplestatusView =  findViewById(R.id.multiplestatusView);
//        multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);    //页面初始化，默认为加载中状态
        multiplestatusView.setOnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
                getData();
            }
        });
    }

    //网路请求 获取数据
    private void getData() {
//        //获取资产（例子）
//        API_HomeManager.getTotalBalance(mContext, new OkHttpCallBack() {
//            @Override
//            public void onSuccess(BaseResponce baseResponce) {
//                multiplestatusView.setViewState(MultiStateView.ViewState.CONTENT);    //页面状态为：显示内容
//                if (BaseResponce.Status_Success.equals(baseResponce.code)) {
//                    //返回成功
//                    GetBalanceResponce responce = (GetBalanceResponce) baseResponce;
//                    //解析数据，do something...
//                    bindUI();
//                } else {
//                    //返回错误信息
////                    CommToast.showToast(mContext, baseResponce.msg);
//                }
//            }
//
//            @Override
//            public void onFailure(BaseResponce baseResponce) {
//                //网络请求失败
//                multiplestatusView.setViewState(MultiStateView.ViewState.ERROR);    //页面状态为：显示内容
//            }
//        });
    }

    //绑定数据
    private void bindUI()
    {

    }

    //点击事件
    @Override
    public void onClick(View view) {
        super.onClick(view);
        Intent intent;
        switch (view.getId()) {
//            case R.id.tv_tohaibao:
//                intent = new Intent(mContext, YaoQing_HaiBaoActivity.class);
//                startActivity(intent);
//                break;

        }
    }
}
