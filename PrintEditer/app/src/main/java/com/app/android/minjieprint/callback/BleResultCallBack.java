package com.app.android.minjieprint.callback;

public interface BleResultCallBack {
    void onReturnResult(byte[] data);

    void onDiscoverServicesSuccess();
}
