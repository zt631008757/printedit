package com.app.android.minjieprint.bean;

import com.app.android.minjieprint.constant.SPConstants;

import org.litepal.crud.DataSupport;

import java.io.Serializable;
import java.util.List;

public class LocalFileInfo extends DataSupport implements Serializable {
    public String filename;  //文件名
    public String timestamp;    //时间
    public List<ElementsInfo> views;   //控件
    public String snapshot;     //截图

    public String viewStr;    //控件json字符串

    public int serialNumber;   //序列号
}
