package com.app.android.minjieprint.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.ui.view.citypiker.adapter.NumericWheelAdapter;
import com.app.android.minjieprint.ui.view.citypiker.widget.OnWheelChangedListener;
import com.app.android.minjieprint.ui.view.citypiker.widget.WheelView;
import com.app.android.minjieprint.util.AnimUtil;
import java.util.Calendar;

/**
 * Created by Administrator on 2018/7/29.
 */

public class Home_TimeSelectDialog extends Dialog implements OnWheelChangedListener, View.OnClickListener {

    Context context;

    public Home_TimeSelectDialog(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    public Home_TimeSelectDialog(@NonNull Context context, @StyleRes int themeResId, CommCallBack callback) {
        super(context, themeResId);
        this.context = context;
        this.callback = callback;
    }

    protected Home_TimeSelectDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_timeselect_wheel);
        initView();
    }

    public void setCurrentTime(int curYear, int curMonth, int curDate) {
        this.curYear = curYear;
        this.curMonth = curMonth;
        this.curDate = curDate;
    }

    int curYear = 0;
    int curMonth = 0;
    int curDate = 0;
    CommCallBack callback;

    public static final int MIN_YEAR = 1900;
    public static final int VISIBLE_ITEMS = 7;

    Calendar myCalendar;
    WheelView wv_year, wv_month, wv_day;
    TextView tv_cancel, tv_ok;
    LinearLayout ll_content;
    View view_bg;

    private void initView() {
        Log.i("test","curYear:"+curYear+ "curMonth:"+curMonth+ "curDate:"+curDate);
        myCalendar = Calendar.getInstance();//获取现在时间
        if (curYear == 0 || curMonth == 0 || curDate == 0) {
            curYear = myCalendar.get(Calendar.YEAR);
            curMonth = myCalendar.get(Calendar.MONTH) + 1;//通过Calendar算出的月数要+1
            curDate = myCalendar.get(Calendar.DATE);
        }
        wv_year = (WheelView) findViewById(R.id.year);
        initYear(curYear);
        wv_month = (WheelView) findViewById(R.id.month);
        initMonth();
        wv_day = (WheelView) findViewById(R.id.day);
        initDay(curYear, curMonth);

        wv_year.setCurrentItem(curYear - MIN_YEAR);
        wv_month.setCurrentItem(curMonth - 1);
        wv_day.setCurrentItem(curDate - 1);

        wv_year.setCyclic(false);
        wv_month.setCyclic(false);
        wv_day.setCyclic(false);
        wv_year.setVisibleItems(VISIBLE_ITEMS);
        wv_month.setVisibleItems(VISIBLE_ITEMS);
        wv_day.setVisibleItems(VISIBLE_ITEMS);

        wv_year.addChangingListener(this);
        wv_month.addChangingListener(this);
        wv_day.addChangingListener(this);

        ll_content = (LinearLayout) findViewById(R.id.ll_content);
        view_bg = findViewById(R.id.view_bg);
        tv_cancel = findViewById(R.id.tv_cancel);
        tv_ok = findViewById(R.id.tv_ok);
        tv_cancel.setOnClickListener(this);
        tv_ok.setOnClickListener(this);
        view_bg.setOnClickListener(this);
        ll_content.setOnClickListener(this);
        AnimUtil.fadeIn(view_bg);
        AnimUtil.fadeIn(ll_content);
    }


    /**
     * 初始化年
     */
    private void initYear(int curYear) {
        NumericWheelAdapter numericWheelAdapter = new NumericWheelAdapter(context, MIN_YEAR, curYear);
        numericWheelAdapter.setLabel("年");
        numericWheelAdapter.setTextSize(16);  //设置字体大小
        wv_year.setViewAdapter(numericWheelAdapter);
    }

    /**
     * 初始化月
     */
    private void initMonth() {
        NumericWheelAdapter numericWheelAdapter = new NumericWheelAdapter(context, 1, 12, "%02d");
        numericWheelAdapter.setLabel("月");
        numericWheelAdapter.setTextSize(16);  //设置字体大小
        wv_month.setViewAdapter(numericWheelAdapter);
    }

    /**
     * 初始化天
     */
    private void initDay(int arg1, int arg2) {
        NumericWheelAdapter numericWheelAdapter = new NumericWheelAdapter(context, 1, getDay(arg1, arg2), "%02d");
        numericWheelAdapter.setLabel("日");
        numericWheelAdapter.setTextSize(16);  //设置字体大小
        wv_day.setViewAdapter(numericWheelAdapter);
    }

    /**
     * @param year
     * @param month
     * @return
     */
    private int getDay(int year, int month) {
        int day = 30;
        boolean flag = false;
        switch (year % 4) {
            case 0:
                flag = true;
                break;
            default:
                flag = false;
                break;
        }
        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                day = 31;
                break;
            case 2:
                day = flag ? 29 : 28;
                break;
            default:
                day = 30;
                break;
        }
        return day;
    }

    @Override
    public void onChanged(WheelView wheel, int oldValue, int newValue) {
        if (wheel == wv_year) {
            initDay(wv_year.getCurrentItem() + MIN_YEAR, wv_month.getCurrentItem() + 1);
        } else if (wheel == wv_month) {
            initDay(wv_year.getCurrentItem() + MIN_YEAR, wv_month.getCurrentItem() + 1);
        } else if (wheel == wv_day) {

        }

        curYear = wv_year.getCurrentItem() + MIN_YEAR;
        curMonth = wv_month.getCurrentItem() + 1;
        curDate = wv_day.getCurrentItem() + 1;

        if(getDay(curYear, curMonth) < curDate)
        {
            wv_day.setCurrentItem(curDate - 1);
            curDate = getDay(curYear, curMonth);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_ok:
                if (callback != null) {
                    callback.onResult(curYear + "|" + curMonth + "|" + curDate);
                }
                dismissWithAnim();
                break;
            case R.id.tv_cancel:
                dismissWithAnim();
                break;
            case R.id.view_bg:
                dismissWithAnim();
                break;
            case R.id.ll_content:
                //不处理，只消耗点击事件
                break;
        }
    }

    public void dismissWithAnim() {
        if (view_bg.getAnimation() != null) return;
        AnimUtil.fadeOut(ll_content, null);
        AnimUtil.fadeOut(view_bg, new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                dismiss();
            }
        });
    }
}
