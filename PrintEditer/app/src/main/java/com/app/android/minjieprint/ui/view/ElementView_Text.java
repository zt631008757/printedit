package com.app.android.minjieprint.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Build;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.bean.ElementsInfo;
import com.app.android.minjieprint.bean.TypeFaceInfo;
import com.app.android.minjieprint.dialog.TextInput_Dialog;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.manager.TypeFaceManager;
import com.app.android.minjieprint.tool.Log;
import com.app.android.minjieprint.util.Util;

import static com.lzy.okgo.utils.HttpUtils.runOnUiThread;

public class ElementView_Text extends ElementView_Base {

    public ElementView_Text(Context context, ElementsInfo info) {
        super(context, info);
        init();
    }

    private static long lastClickTime = 0;

    TextView tv_text;
    LinearLayout ll_root;
    String fontName = "";
    Typeface iconfont;

    private void init() {
        View.inflate(mContext, R.layout.view_ele_text, this);

        tv_text = findViewById(R.id.tv_text);
        ll_root = findViewById(R.id.ll_root);

        tv_text.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Log.i("tv_text点击");
                        long currentTimeMillis = System.currentTimeMillis();
                        if (currentTimeMillis - lastClickTime < DOUBLE_TIME) {
                            onDoubleClick();
                        }
                        lastClickTime = currentTimeMillis;
                        break;
                }
                return false;
            }
        });

        refreshUI();
    }

    //双击
    public void onDoubleClick() {
        final TextInput_Dialog dialog = new TextInput_Dialog(mContext);
        dialog.setData(tv_text.getText().toString());
        dialog.setIntputCallBack(new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                String text = (String) obj;
                elementsInfo.text = text;
                refreshUI();
                dialog.dismissWithAnim();
            }
        });
        dialog.show();
    }

    //刷新控件
    @Override
    public void refreshUI() {
        super.refreshUI();
        try {
            tv_text.setTextColor(Color.parseColor(elementsInfo.color));
        } catch (Exception e) {
            tv_text.setTextColor(Color.parseColor("#333333"));
        }

        if (!fontName.equals(elementsInfo.fontname)) {
            fontName = elementsInfo.fontname;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    TypeFaceInfo info = TypeFaceManager.getTypeFaceInfo(elementsInfo.fontname);
                    iconfont = Typeface.createFromAsset(mContext.getAssets(), info.filePath);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            bindFont();
                        }
                    });
                }
            }).start();
        } else {
            bindFont();
        }
        tv_text.setTextSize(TypedValue.COMPLEX_UNIT_PX, elementsInfo.fontsize);     //字体大小
        //字间距
        float space = (float) elementsInfo.kerning / (float) elementsInfo.fontsize;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tv_text.setLetterSpacing(space);
        }

        //对齐
        if (elementsInfo.alignment == 0) {
            tv_text.setGravity(Gravity.LEFT);
        } else if (elementsInfo.alignment == 1) {
            tv_text.setGravity(Gravity.CENTER_HORIZONTAL);
        } else if (elementsInfo.alignment == 2) {
            tv_text.setGravity(Gravity.RIGHT);
        }

        tv_text.getPaint().setAntiAlias(true);//抗锯齿
//        //下划线
        int flag = 0;
        if (elementsInfo.underscore && elementsInfo.deleteline) {
            flag = Paint.UNDERLINE_TEXT_FLAG | Paint.STRIKE_THRU_TEXT_FLAG;
        } else if (elementsInfo.underscore) {
            flag = Paint.UNDERLINE_TEXT_FLAG;
        } else if (elementsInfo.deleteline) {
            flag = Paint.STRIKE_THRU_TEXT_FLAG;
        } else {
            flag = 0;
        }
        tv_text.getPaint().setFlags(flag);


        //计算字体大小
        Rect rect = new Rect();
        Paint paint = new Paint();
//        paint.setTextScaleX();


//        setotation(elementsInfo.orientation);
//        tv_text.setBackgroundColor(Color.parseColor("#ffff00"));
//        ll_root.setBackgroundColor(Color.parseColor("#086DFF"));

//        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) tv_text.getLayoutParams();
////
////        Log.i("test", "params.width:" + params.width + "  params.height:" + params.height);
////
//        RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams) ll_root.getLayoutParams();
//        if (elementsInfo.orientation % 180 == 0)   //横
//        {
//            params.width = elementsInfo.width;
//            params.height = elementsInfo.height ;
//
//            params1.width = elementsInfo.width;
//            params1.height = elementsInfo.height ;
//
//
//
//        } else {
//            params.width = elementsInfo.height;
//            params.height = elementsInfo.width;
//
//            params1.width = elementsInfo.height;
//            params1.height = elementsInfo.width ;
//        }
//        tv_text.setLayoutParams(params);
//        ll_root.setLayoutParams(params1);


//
//        tv_text.setRotation(elementsInfo.orientation);
//        refitTextSize(elementsInfo.text);
//        settextSize();
    }

    private void settextSize() {
        float textSize = tv_text.getTextSize();
        Paint textPaint = tv_text.getPaint();
        int viewWidth = elementsInfo.width - 30;
        String t = elementsInfo.text;
        while (textPaint.measureText(t) > viewWidth) {
            textSize--;
            textPaint.setTextSize(textSize);
        }

        tv_text.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);

    }


    private void refitTextSize(String t) {
        int viewWidth = getWidth();
        float textSize = tv_text.getTextSize();
        int maxSize;

        if (t == null || viewWidth <= 0)
            return;

        /**
         * 最大行数
         */
        int line = 1;
        Log.e("AutoAdjustTextView", "文字行数" + line);

        /**
         * 文字的宽度
         */
        Paint textPaint = tv_text.getPaint();

        float textWidth = textPaint.measureText(t);
        Log.e("AutoAdjustTextView", "文字长度" + textWidth);

        while (textPaint.measureText(t) / line > viewWidth) {
            textSize--;
            tv_text.setTextSize(textSize);
            textPaint.setTextSize(textSize);
        }
        while (textPaint.measureText(t) / line < viewWidth) {
            if (textSize >= 1000) {
                break;
            }
            textSize++;
            tv_text.setTextSize(textSize);
            textPaint.setTextSize(textSize);
        }
        Log.e("AutoAdjustTextView", "当前文字大小" + tv_text.getTextSize());

        tv_text.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
    }

    private void bindFont() {
        //斜体
        tv_text.setTypeface(iconfont, Typeface.NORMAL);
        if (elementsInfo.blod && elementsInfo.italic) {
            tv_text.setTypeface(iconfont, Typeface.BOLD_ITALIC);
        } else if (elementsInfo.italic) {
            tv_text.setTypeface(iconfont, Typeface.ITALIC);
        } else if (elementsInfo.blod) {
            tv_text.setTypeface(iconfont, Typeface.BOLD);
        } else {
            tv_text.setTypeface(iconfont, Typeface.NORMAL);
        }
        tv_text.setText(elementsInfo.text);
    }
}