package com.app.android.minjieprint.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.tool.CommToast;
import com.app.android.minjieprint.tool.Log;
import com.app.android.minjieprint.util.AnimUtil;
import com.app.android.minjieprint.util.BitmapUtil;
import com.app.android.minjieprint.util.StatusBarUtil_Dialog;
import com.app.android.minjieprint.util.Util;

/**
 * Created by Administrator on 2018/8/29.
 */

public class PrintLoading_Dialog extends Dialog implements View.OnClickListener {
    public PrintLoading_Dialog(@NonNull Context context) {
        super(context, R.style.myDialog);
    }

    public PrintLoading_Dialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
    }

    protected PrintLoading_Dialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    LinearLayout ll_content;
    View view_bg;

    CommCallBack callBack;
    Bitmap bitmap;
    TextView tv_print;
    ImageView iv_img;
    ProgressBar progressbar, progressbar1;

    public void setData(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    //设置输入回调
    public void setIntputCallBack(CommCallBack callBack) {
        this.callBack = callBack;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_printloading);
        initView();
        StatusBarUtil_Dialog.setImmersiveStatusBar(this, false);
    }

    private void initView() {
        ll_content = (LinearLayout) findViewById(R.id.ll_content);
        view_bg = findViewById(R.id.view_bg);
        view_bg.setOnClickListener(this);
        ll_content.setOnClickListener(this);
        AnimUtil.fadeIn(view_bg);
        AnimUtil.fadeIn(ll_content);

        iv_img = findViewById(R.id.iv_img);
        tv_print = findViewById(R.id.tv_print);
        progressbar = findViewById(R.id.progressbar);
        progressbar1 = findViewById(R.id.progressbar1);

//        bitmap = BitmapUtil.rotateBitmap(bitmap, 90);
        iv_img.setImageBitmap(bitmap);

//        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(bitmap.getWidth(),bitmap.getHeight());
//        iv_img.setLayoutParams(params);

    }

    public void setProgress(int progress) {
        Log.i("progress:" + progress);
        progressbar.setProgress(progress);
        tv_print.setText(Util.getString(R.string.printdialog_sending) + progress + "%");
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.view_bg:
                dismissWithAnim();
                break;
            case R.id.ll_content:
                //不处理，只消耗点击事件
                break;
        }
    }

    public void dismissWithAnim() {
        if (view_bg.getAnimation() != null) return;
        AnimUtil.fadeOut(ll_content, null);
        AnimUtil.fadeOut(view_bg, new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        dismissWithAnim();
    }
}
