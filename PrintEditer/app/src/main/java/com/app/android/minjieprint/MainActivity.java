package com.app.android.minjieprint;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.view.KeyEvent;

import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.tool.CommToast;
import com.app.android.minjieprint.ui.activity.BaseActivity;
import com.app.android.minjieprint.ui.fragment.Home_Fragment_Main;
import com.app.android.minjieprint.ui.fragment.Home_Fragment_Mine;
import com.app.android.minjieprint.ui.view.MyBottomTabView;

public class MainActivity extends BaseActivity {

    public final static int HOME_INDEX_0 = 0;
    public final static int HOME_INDEX_1 = 1;
    public final static int HOME_INDEX_2 = 2;
    public final static int HOME_INDEX_3 = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
    }

    MyBottomTabView mybottomtabview;

    private void initView() {
        mybottomtabview = (MyBottomTabView) findViewById(R.id.mybottomtabview);
        mybottomtabview.setOnTabSelectCallBack(onBottomTabSelect);
    }


    private void chooseFragment(int index) {
        Fragment currentFragment = getSupportFragmentManager().findFragmentByTag(String.valueOf(index));
        hideAllFrag();
        if (currentFragment == null) {
            switch (index) {
                case HOME_INDEX_0:     // 首页
                    currentFragment = Home_Fragment_Main.newInstance();
                    break;
                case HOME_INDEX_1:     //
                    currentFragment = Home_Fragment_Mine.newInstance();
                    break;
//                case HOME_INDEX_2:      //
//                    currentFragment = Home_Fragment_IM.newInstance();
//                    break;
//                case HOME_INDEX_3:      //我的
//                    currentFragment = Home_Fragment_Mine.newInstance();
//                    break;
            }
            addFragment(currentFragment, index);
        } else {
            showFragment(currentFragment);
        }
        //切换刷新
        currentFragment.onResume();
    }

    CommCallBack onBottomTabSelect = new CommCallBack() {
        @Override
        public void onResult(Object obj) {
            int index = (int) obj;
            chooseFragment(index);
        }
    };

    private void addFragment(Fragment currentFragment, int index) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.framelayout, currentFragment, index + "");
        ft.commitAllowingStateLoss();
    }

    /**
     * 隐藏所有的fragment
     */
    private void hideAllFrag() {
        for (int i = 0; i < 5; i++) {
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(String.valueOf(i));
            if (fragment != null) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.hide(fragment);
                ft.commitAllowingStateLoss();
            }
        }
    }

    /**
     * 展示fragment
     */
    public void showFragment(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (fragment.isDetached()) {
            ft.attach(fragment).show(fragment);
            ft.commitAllowingStateLoss();
        } else {
            ft.show(fragment);
            ft.commitAllowingStateLoss();
        }
    }


    private long exitTime = 0;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            if ((System.currentTimeMillis() - exitTime) > 2000) {
                CommToast.showToast(mContext, "再按一次退出程序");
                exitTime = System.currentTimeMillis();
                return true;
            } else {
                return super.onKeyDown(keyCode, event);
            }
        }
        return super.onKeyDown(keyCode, event);
    }
}
