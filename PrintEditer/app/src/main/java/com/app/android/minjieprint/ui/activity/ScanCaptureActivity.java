package com.app.android.minjieprint.ui.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.util.StatusBarUtil;
import com.app.android.minjieprint.util.Util;
import com.google.zxing.Result;
import com.google.zxing.client.android.AutoScannerView;
import com.google.zxing.client.android.BaseCaptureActivity;

/**
 * 扫描界面
 */
public class ScanCaptureActivity extends BaseCaptureActivity {

    private static final String TAG = ScanCaptureActivity.class.getSimpleName();

    private SurfaceView surfaceView;
    private AutoScannerView autoScannerView;
    ImageView public_title_left_img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wechat_capture);
        surfaceView = (SurfaceView) findViewById(R.id.preview_view);
        autoScannerView = (AutoScannerView) findViewById(R.id.autoscanner_view);

//        Message msg=new Message();
//        ((CaptureActivityHandler)getHandler()).decodeThread.getHandler().handleMessage();

        //白色状态栏 黑色文字
//        setStatuBarPadding(true, Color.parseColor("#1d1f22")); //状态栏颜色
        StatusBarUtil.setImmersiveStatusBar(this, false);
        ((ViewGroup) getWindow().getDecorView()).getChildAt(0).setBackgroundColor(Color.parseColor("#f5f5f5"));

        public_title_left_img = findViewById(R.id.public_title_left_img);
        public_title_left_img.setVisibility(View.VISIBLE);
        public_title_left_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void setStatuBarPadding(boolean hasPadding, int color) {
        if (hasPadding) {
            getWindow().getDecorView().setPadding(0, Util.getStatusBarHeight(this), 0, 0);
            getWindow().getDecorView().setBackgroundColor(color);
        } else {
            getWindow().getDecorView().setPadding(0, 0, 0, 0);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        autoScannerView.setCameraManager(cameraManager);
    }

    @Override
    public SurfaceView getSurfaceView() {
        return (surfaceView == null) ? (SurfaceView) findViewById(R.id.preview_view) : surfaceView;
    }

    @Override
    public void dealDecode(Result rawResult, Bitmap barcode, float scaleFactor) {
        Log.i(TAG, "dealDecode ~~~~~ " + rawResult.getText() + " " + barcode + " " + scaleFactor);
        playBeepSoundAndVibrate(true, true);
//        对此次扫描结果不满意可以调用
//        reScan();
        Intent intent = new Intent();
        intent.putExtra("text", rawResult.getText());
        setResult(0, intent);
        finish();

    }
}
