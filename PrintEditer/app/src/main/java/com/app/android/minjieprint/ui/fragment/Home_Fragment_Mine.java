package com.app.android.minjieprint.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.android.minjieprint.R;

/**
 * Created by Administrator on 2018/8/20.
 */

public class Home_Fragment_Mine extends BaseFragment {

    private static Home_Fragment_Mine fragment = null;

    public static Home_Fragment_Mine newInstance() {
        if (fragment == null) {
            fragment = new Home_Fragment_Mine();
        }
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContext = getActivity();
        rootView = inflater.inflate(R.layout.fragment_home_mine, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    Context mContext;
    View rootView;

    private void initView() {


    }

}
