package com.app.android.minjieprint.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.bean.ElementsInfo;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.util.GlideUtil;
import com.app.android.minjieprint.util.Util;

import java.util.List;

/**
 * Created by Administrator on 2018/8/22.
 */

public class FileEdit_AddElementsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    List<ElementsInfo> list;
    CommCallBack callBack;
    boolean isLandspace = false;  //是否横屏

    public FileEdit_AddElementsAdapter(Context mContext, CommCallBack callBack) {
        this.mContext = mContext;
        this.callBack = callBack;
    }

    public void setData(List<ElementsInfo> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void setOrientation(boolean isLandspace) {
        this.isLandspace = isLandspace;
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        ViewGroup viewGroup = (ViewGroup) mInflater.inflate(R.layout.item_add_elment, null, false);
        ContentViewHolder holder = new ContentViewHolder(viewGroup);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ContentViewHolder viewHolder = (ContentViewHolder) holder;
        final ElementsInfo info = list.get(position);
        viewHolder.tv_text.setText(info.name);
        GlideUtil.displayImage(mContext, info.resouce, viewHolder.iv_img, 0);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callBack != null) {
                    callBack.onResult(info);
                }
            }
        });

        if (isLandspace) {
            //横屏
            viewHolder.itemView.setLayoutParams(new RecyclerView.LayoutParams(Util.dip2px(mContext, 80), Util.dip2px(mContext, 80)));
        } else {
            //竖屏
            viewHolder.itemView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Util.dip2px(mContext, 80)));
        }
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }


    class ContentViewHolder extends RecyclerView.ViewHolder {
        TextView tv_text;
        ImageView iv_img;

        public ContentViewHolder(View itemView) {
            super(itemView);
            tv_text = itemView.findViewById(R.id.tv_text);
            iv_img = itemView.findViewById(R.id.iv_img);
        }
    }
}
