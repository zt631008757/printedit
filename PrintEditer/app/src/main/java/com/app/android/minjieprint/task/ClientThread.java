package com.app.android.minjieprint.task;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.app.android.minjieprint.callback.ClientCallBack;
import com.app.android.minjieprint.event.Event_SendData_Success;
import com.app.android.minjieprint.tool.Log;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Method;

public class ClientThread extends Thread {

    private static final String TAG = "mcy";
    private ClientCallBack callBack;
    private BluetoothSocket bluetoothSocket = null;
    private Handler handler;
    private OutputStream outputStream;
    BluetoothDevice mmDevice;

    public ClientThread(BluetoothSocket bluetoothSocket, BluetoothDevice mmDevice, ClientCallBack callBack) {
        this.callBack = callBack;
        this.bluetoothSocket = bluetoothSocket;
        this.mmDevice = mmDevice;
    }

    /**
     * 写数据
     *
     * @param data
     */

    public void write(byte[] data) {
        Message message = new Message();
        message.obj = data;
        handler.sendMessage(message);
    }

    public boolean connet() {
        try {
            // Connect the device through the socket. This will block
            // until it succeeds or throws an exception
            bluetoothSocket.connect();
            return true;
        } catch (IOException connectException) {
            // Unable to connect; close the socket and get out
            try {
                Method m = mmDevice.getClass().getMethod("createRfcommSocket", new Class[]{int.class});
                bluetoothSocket = (BluetoothSocket) m.invoke(mmDevice, 1);
                bluetoothSocket.connect();
                return true;
            } catch (Exception e) {
                Log.e("BLUE", e.toString());
                try {
                    bluetoothSocket.close();
                } catch (IOException ie) {
                }
                return false;
            }
        }
    }

    /**
     * 关闭各种连接连接
     */
    public void closeSocket() {
        try {
            outputStream.close();
            bluetoothSocket.close();
            callBack.onConnectClose();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            connet();
            outputStream = bluetoothSocket.getOutputStream();//读取需要发送的的数据
            Looper.prepare();
            handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    try {
                        Log.i("写入数据开始");
//                        callBack.onSend(0);
                        byte[] data = (byte[]) msg.obj;
                        outputStream.write(data);
                        outputStream.flush();

                        Log.i("写入数据结束");
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                callBack.onSend(100);
                            }
                        }, 500);
                    } catch (IOException e) {
                        e.printStackTrace();
//                        callBack.onSend(-1);
                    }
                }
            };
            callBack.onConnectSuccess();
            Looper.loop();
        } catch (IOException e) {
            e.printStackTrace();
            callBack.onConnectFail(e.getMessage());
        }
    }
}
