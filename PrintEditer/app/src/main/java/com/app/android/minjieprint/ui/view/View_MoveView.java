package com.app.android.minjieprint.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.util.AnimUtil;

public class View_MoveView extends LinearLayout implements View.OnClickListener {
    public View_MoveView(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public View_MoveView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    public View_MoveView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    Context mContext;
    ImageView iv_close, iv_move_top, iv_move_left, iv_move_right, iv_move_bottom;
    TextView tv_horizontal, tv_vertical;
    LinearLayout ll_root;


    private void init() {
        View.inflate(mContext, R.layout.view_action_move, this);
        iv_close = findViewById(R.id.iv_close);
        iv_move_top = findViewById(R.id.iv_move_top);
        iv_move_left = findViewById(R.id.iv_move_left);
        iv_move_right = findViewById(R.id.iv_move_right);
        iv_move_bottom = findViewById(R.id.iv_move_bottom);
        tv_horizontal = findViewById(R.id.tv_horizontal);
        tv_vertical = findViewById(R.id.tv_vertical);
        ll_root = findViewById(R.id.ll_root);

        iv_close.setOnClickListener(this);
        iv_move_top.setOnClickListener(this);
        iv_move_left.setOnClickListener(this);
        iv_move_right.setOnClickListener(this);
        iv_move_bottom.setOnClickListener(this);
        tv_horizontal.setOnClickListener(this);
        tv_vertical.setOnClickListener(this);
        ll_root.setOnClickListener(this);
    }

    CommCallBack callBack;

    public void setCallBack(CommCallBack callBack) {
        this.callBack = callBack;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_close:
                close();
                break;
            case R.id.iv_move_top:
                move(1);
                break;
            case R.id.iv_move_left:
                move(2);
                break;
            case R.id.iv_move_right:
                move(3);
                break;
            case R.id.iv_move_bottom:
                move(4);
                break;
            case R.id.tv_horizontal:
                move(5);
                break;
            case R.id.tv_vertical:
                move(6);
                break;
            case R.id.ll_root:
                break;
        }
    }

    //显示
    public void show() {
        setVisibility(VISIBLE);
        AnimUtil.enterFromBottom(this);
    }

    //关闭
    public void close() {
        AnimUtil.outToBottom(this, new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                setVisibility(GONE);
            }
        });
    }


    //移动
    private void move(int action) {
        if (callBack != null) {
            callBack.onResult(action);
        }
    }
}
