package com.app.android.minjieprint.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.android.minjieprint.MyApplication;
import com.app.android.minjieprint.R;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.manager.Config;
import com.app.android.minjieprint.tool.Log;
import com.app.android.minjieprint.util.AnimUtil;
import com.app.android.minjieprint.util.BitmapUtil;
import com.app.android.minjieprint.util.ImageDispose;
import com.app.android.minjieprint.util.StatusBarUtil_Dialog;
import com.app.android.minjieprint.util.Util;
import com.dd.CircularProgressButton;
import com.feasycom.bean.BluetoothDeviceWrapper;
import com.feasycom.controler.FscBeaconCallbacks;
import com.feasycom.controler.FscBleCentralApi;
import com.feasycom.controler.FscBleCentralApiImp;
import com.feasycom.controler.FscSppApi;
import com.feasycom.controler.FscSppApiImp;
import com.feasycom.controler.FscSppCallbacks;
import com.feasycom.controler.FscSppCallbacksImp;
import com.lzy.okgo.utils.HttpUtils;

import java.util.ArrayList;

/**
 * Created by Administrator on 2018/8/29.
 */

public class Print_Dialog1 extends Dialog implements View.OnClickListener {
    public Print_Dialog1(@NonNull Context context) {
        super(context, R.style.myDialog);
        mContext = context;
    }

    Context mContext;
    CommCallBack callBack;
    Bitmap bitmap;
    BluetoothDeviceWrapper deviceWrapper;
    BluetoothDevice device;
    FscSppApiImp fscSppApi;
    Handler handler = new Handler();

    CircularProgressButton circularprogressbutton;

    public void setData(Bitmap bitmap, BluetoothDevice device) {
        this.bitmap = bitmap;
        this.device = device;
    }

    public void setData(Bitmap bitmap, BluetoothDeviceWrapper deviceWrapper) {
        this.bitmap = bitmap;
        this.deviceWrapper = deviceWrapper;
    }

    //设置输入回调
    public void setIntputCallBack(CommCallBack callBack) {
        this.callBack = callBack;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_print1);
        initView();
        StatusBarUtil_Dialog.setImmersiveStatusBar(this, false);
        connectionBlueTooth();
    }

    LinearLayout ll_content;
    View view_bg;
    TextView tv_print;
    ImageView iv_img;
    ProgressBar progressbar, progressbar1;

    private void initView() {
        ll_content = (LinearLayout) findViewById(R.id.ll_content);
        view_bg = findViewById(R.id.view_bg);
        view_bg.setOnClickListener(this);
        ll_content.setOnClickListener(this);
        AnimUtil.fadeIn(view_bg);
        AnimUtil.fadeIn(ll_content);

        iv_img = findViewById(R.id.iv_img);
        tv_print = findViewById(R.id.tv_print);
        progressbar = findViewById(R.id.progressbar);
        progressbar1 = findViewById(R.id.progressbar1);
        circularprogressbutton = findViewById(R.id.circularprogressbutton);


        tv_print.setOnClickListener(this);

        bitmap = BitmapUtil.rotateBitmap(bitmap, 90);
//        byte[] data = ImageDispose.Bitmap2Bytes(bitmap);
//        bitmap = ImageDispose.getPicFromBytes(data, null);

        bitmap = BitmapUtil.compressQuality(bitmap, 30);
//        bitmap = bitmap.copy(Bitmap.Config.RGB_565, true);
        iv_img.setImageBitmap(bitmap);

        fscSppApi = FscSppApiImp.getInstance(MyApplication.context);
        fscSppApi.setCallbacks(new FscSppCallbacksImp() {
            @Override
            public void sppConnected(BluetoothDevice device) {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        HttpUtils.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tv_print.setText(Util.getString(R.string.printdialog_conneted));
                                progressbar1.setVisibility(View.GONE);
                                sendData();
                            }
                        });
                    }
                }, 500);
            }

            @Override
            public void sppDisconnected(BluetoothDevice device) {

            }

            @Override
            public void packetReceived(byte[] dataByte, String dataString, String dataHexString) {

            }

            @Override
            public void sendPacketProgress(BluetoothDevice device, final int percentage, byte[] tempByte) {
                try {
                    HttpUtils.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (percentage <= 100) {
                                progressbar.setProgress(percentage);
                                tv_print.setText(Util.getString(R.string.printdialog_sending) + percentage + "%");
                            }
                            if (percentage == 100) {
                                //发送成功
                                tv_print.setText(Util.getString(R.string.printdialog_send_success));
                            }
                        }
                    });
                    if (percentage != 101) {
                        Log.d("sendPacketProgress:" + percentage);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        connectionBlueTooth();
    }

    /**
     * 连接蓝牙
     */
    public void connectionBlueTooth() {
        if (deviceWrapper != null) {
            fscSppApi.connect(deviceWrapper.getAddress());
        } else if (device != null) {
            fscSppApi.connect(device.getAddress());
        }
    }

    //断开连接
    public void disConnectionBlueTooth() {
        try {
            if (fscSppApi.isConnected()) {
                fscSppApi.disconnect();
            }
        } catch (Exception e) {

        }
    }

    /**
     * 写数据
     */
    public void sendData() {
        try {
            byte[] data = Util.buildPacket(ImageDispose.Bitmap2Bytes(bitmap), bitmap.getHeight());


            Log.i("bitmap.getWidth:" + bitmap.getWidth() + "  " + bitmap.getHeight());
            Log.i("data.length:" + data.length);
            fscSppApi.send(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.view_bg:
                dismissWithAnim();
                break;
            case R.id.ll_content:
                //不处理，只消耗点击事件
                break;
            case R.id.tv_print:
//                connectionBlueTooth();
                break;
        }
    }

    @Override
    public void dismiss() {
        disConnectionBlueTooth();
        super.dismiss();
    }

    public void dismissWithAnim() {
        if (view_bg.getAnimation() != null) return;
        AnimUtil.fadeOut(ll_content, null);
        AnimUtil.fadeOut(view_bg, new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        dismissWithAnim();
    }
}
