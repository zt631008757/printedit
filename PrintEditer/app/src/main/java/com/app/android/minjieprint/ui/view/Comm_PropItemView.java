package com.app.android.minjieprint.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.dialog.CommInput_Dialog;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.tool.CommToast;
import com.app.android.minjieprint.util.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/9/20.
 */

public class Comm_PropItemView extends LinearLayout implements View.OnClickListener {
    public Comm_PropItemView(Context context) {
        super(context);
        this.context = context;
        initView(null);
    }

    public Comm_PropItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initView(attrs);
    }

    public Comm_PropItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initView(attrs);
    }

    Context context;
    int type;
    String unit;
    String selectValue;
    CommCallBack callBack;
    Handler handler = new Handler();
    int number_minValue = 0;

    public TextView tv_title, tv_content, tv_unit;
    public ImageView iv_arr;
    public LongClickButton_Image iv_jian, iv_jia;
    View view_line;
    LinearLayout ll_num, ll_check, ll_content;
    SwitchButton switchbutton;

    public void setCallBack(CommCallBack callBack) {
        this.callBack = callBack;
    }

    private void initView(AttributeSet attrs) {
        View.inflate(context, R.layout.view_comm_propview, this);
        tv_title = findViewById(R.id.tv_title);
        tv_content = findViewById(R.id.tv_content);
        iv_arr = findViewById(R.id.iv_arr);
        view_line = findViewById(R.id.view_line);
        ll_num = findViewById(R.id.ll_num);
        switchbutton = findViewById(R.id.switchbutton);
        iv_jian = findViewById(R.id.iv_jian);
        iv_jia = findViewById(R.id.iv_jia);
        tv_unit = findViewById(R.id.tv_unit);
        ll_check = findViewById(R.id.ll_check);
        ll_content = findViewById(R.id.ll_content);

        iv_jia.setOnClickListener(this);
        iv_jian.setOnClickListener(this);

        iv_jia.setLongClickRepeatListener(new LongClickButton_Image.LongClickRepeatListener() {
            @Override
            public void repeatAction() {
                iv_jia.performClick();
            }
        });
        iv_jian.setLongClickRepeatListener(new LongClickButton_Image.LongClickRepeatListener() {
            @Override
            public void repeatAction() {
                iv_jian.performClick();
            }
        });


        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.Comm_PropItemView);
            //标题
            String text = typedArray.getString(R.styleable.Comm_PropItemView_piv_titleName);
            tv_title.setText(text);
            //内容
            String content = typedArray.getString(R.styleable.Comm_PropItemView_piv_content);
            tv_content.setText(content);
            //线条
            boolean showLine = typedArray.getBoolean(R.styleable.Comm_PropItemView_piv_showLine, false);
            view_line.setVisibility(showLine ? VISIBLE : GONE);
            //类型
            type = typedArray.getInt(R.styleable.Comm_PropItemView_piv_type, 0);
            //单位
            unit = typedArray.getString(R.styleable.Comm_PropItemView_piv_numberUnit);
            if (!TextUtils.isEmpty(unit)) {
                tv_unit.setVisibility(VISIBLE);
                tv_unit.setText(" " + unit);
            } else {
                tv_unit.setVisibility(GONE);
            }
            //选择项
            selectValue = typedArray.getString(R.styleable.Comm_PropItemView_piv_selectValue);


            initType();
            typedArray.recycle();
        }
    }

    private void initType() {
        ll_num.setVisibility(GONE);
        iv_arr.setVisibility(GONE);
        switchbutton.setVisibility(GONE);
        ll_check.setVisibility(GONE);
        switch (type) {
            case 0:   //选择 或 输入
                iv_arr.setVisibility(VISIBLE);
                break;
            case 1:   //数字 加减
                ll_num.setVisibility(VISIBLE);
                ll_content.setOnClickListener(this);
                break;
            case 2:   //开关
                switchbutton.setVisibility(VISIBLE);
                break;
            case 3:   //切换
                ll_check.setVisibility(VISIBLE);
                createSelectValue();
                break;
        }
    }

    List<TextView> selecTextSize = new ArrayList<>();

    //创建选择项
    public void createSelectValue() {
        try {
            String[] arr = selectValue.split(",");
            selecTextSize.clear();
            for (int i = 0; i < arr.length; i++) {
                if (i > 0) {
                    View view = new View(getContext());
                    view.setBackgroundColor(getResources().getColor(R.color.mainColor_blue));
                    view.setLayoutParams(new LinearLayout.LayoutParams(Util.dip2px(getContext(), 1), ViewGroup.LayoutParams.MATCH_PARENT));
                    ll_check.addView(view);
                }

                TextView textView = new TextView(getContext());
                textView.setText(arr[i]);
                textView.setGravity(Gravity.CENTER);
                textView.setTextSize(13);
                textView.setMinWidth(Util.dip2px(getContext(), 50));
                textView.setPadding(Util.dip2px(getContext(), 15), 0, Util.dip2px(getContext(), 15), 0);
                LinearLayout.LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
                textView.setLayoutParams(params);
                final int finalI = i;
                textView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        setSelectValue(finalI);
                        if (callBack != null) {
                            callBack.onResult(finalI);
                        }
                    }
                });

                ll_check.addView(textView);
                selecTextSize.add(textView);
            }
            setSelectValue(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //选中
    public void setSelectValue(int index) {
        for (int i = 0; i < selecTextSize.size(); i++) {
            TextView textView = selecTextSize.get(i);
            if (index == i) {
                if (index == 0) {
                    textView.setBackgroundResource(R.drawable.shape_round4dp_maincolor_blue_left);
                } else if (index == selecTextSize.size() - 1) {
                    textView.setBackgroundResource(R.drawable.shape_round4dp_maincolor_blue_right);
                } else {
                    textView.setBackgroundColor(getResources().getColor(R.color.mainColor_blue));
                }
                textView.setTextColor(Color.parseColor("#ffffff"));
            } else {
                textView.setBackgroundColor(getResources().getColor(R.color.transparent));
                textView.setTextColor(getResources().getColor(R.color.mainColor_blue));
            }
        }
    }

    //设置标题
    public void setTitle(String text) {
        tv_title.setText(text);
    }

    //设置内容
    public void setContext(String text) {
        tv_content.setText(text);
    }

    //设置开关选中
    public void setSwitchChecked(boolean switchChecked) {
        switchbutton.setOnCheckedChangeListener(null);
        switchbutton.setChecked(switchChecked);
        switchbutton.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                if (callBack != null) {
                    callBack.onResult(isChecked);
                }
            }
        });
    }

    public void setNumber_minValue(int number_minValue)
    {
        this.number_minValue = number_minValue;
    }

    @Override
    public void onClick(View view) {
        int num = Integer.parseInt(tv_content.getText().toString());
        switch (view.getId()) {
            case R.id.iv_jia:
                num++;
                if (callBack != null) {
                    callBack.onResult(num);
                }
                break;
            case R.id.iv_jian:
                if (num == number_minValue) return;
                num--;
                if (callBack != null) {
                    callBack.onResult(num);
                }
                break;
            case R.id.ll_content:
                CommInput_Dialog dialog = new CommInput_Dialog(getContext());
                dialog.setData(tv_title.getText().toString(), "请输入", 10, num + "");
                dialog.setInputType(1);
                dialog.setIntputCallBack(new CommCallBack() {
                    @Override
                    public void onResult(Object obj) {
                        try {
                            int num = Integer.parseInt((String) obj);
                            callBack.onResult(num);
                        } catch (Exception e) {
                            CommToast.showToast(getContext(), "请输入整数数字");
                        }
                    }
                });
                dialog.show();
                break;
        }
    }
}
