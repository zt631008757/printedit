package com.app.android.minjieprint.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.adapter.A_ModelAdapter;
import com.app.android.minjieprint.adapter.LogoList_LogoAdapter;
import com.app.android.minjieprint.adapter.LogoList_NameAdapter;
import com.app.android.minjieprint.bean.LogoInfo;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.responce.GetLogListResponce;
import com.app.android.minjieprint.ui.view.MultiStateView;
import com.google.gson.Gson;

/**
 * Created by Administrator on 2018/6/20.
 */

public class LogoListActivity extends BaseActivity {

    String result = "{\"data\":{\"groupList\":[{\"groupName\":\"通用\",\"childList\":[{\"groupName\":\"通用标识\",\"childList\":null,\"logoList\":[{\"icon\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1564588085965&di=fa879c956373c783d467b918c0a65de6&imgtype=0&src=http%3A%2F%2Fku.90sjimg.com%2Felement_origin_min_pic%2F17%2F05%2F24%2Fbd35ed9fcd5caabfd72e682a810614e9.jpg\"},{\"icon\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1564588085965&di=fa879c956373c783d467b918c0a65de6&imgtype=0&src=http%3A%2F%2Fku.90sjimg.com%2Felement_origin_min_pic%2F17%2F05%2F24%2Fbd35ed9fcd5caabfd72e682a810614e9.jpg\"},{\"icon\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1564588085965&di=fa879c956373c783d467b918c0a65de6&imgtype=0&src=http%3A%2F%2Fku.90sjimg.com%2Felement_origin_min_pic%2F17%2F05%2F24%2Fbd35ed9fcd5caabfd72e682a810614e9.jpg\"},{\"icon\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1564588085965&di=fa879c956373c783d467b918c0a65de6&imgtype=0&src=http%3A%2F%2Fku.90sjimg.com%2Felement_origin_min_pic%2F17%2F05%2F24%2Fbd35ed9fcd5caabfd72e682a810614e9.jpg\"}]},{\"groupName\":\"通用箭头\",\"childList\":null,\"logoList\":[{\"icon\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1564588085965&di=fa879c956373c783d467b918c0a65de6&imgtype=0&src=http%3A%2F%2Fku.90sjimg.com%2Felement_origin_min_pic%2F17%2F05%2F24%2Fbd35ed9fcd5caabfd72e682a810614e9.jpg\"},{\"icon\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1564588085965&di=fa879c956373c783d467b918c0a65de6&imgtype=0&src=http%3A%2F%2Fku.90sjimg.com%2Felement_origin_min_pic%2F17%2F05%2F24%2Fbd35ed9fcd5caabfd72e682a810614e9.jpg\"},{\"icon\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1564588085965&di=fa879c956373c783d467b918c0a65de6&imgtype=0&src=http%3A%2F%2Fku.90sjimg.com%2Felement_origin_min_pic%2F17%2F05%2F24%2Fbd35ed9fcd5caabfd72e682a810614e9.jpg\"}]}],\"logoList\":null},{\"groupName\":\"卡通\",\"childList\":[{\"groupName\":\"背景\",\"childList\":null,\"logoList\":[{\"icon\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1564588085965&di=fa879c956373c783d467b918c0a65de6&imgtype=0&src=http%3A%2F%2Fku.90sjimg.com%2Felement_origin_min_pic%2F17%2F05%2F24%2Fbd35ed9fcd5caabfd72e682a810614e9.jpg\"},{\"icon\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1564588085965&di=fa879c956373c783d467b918c0a65de6&imgtype=0&src=http%3A%2F%2Fku.90sjimg.com%2Felement_origin_min_pic%2F17%2F05%2F24%2Fbd35ed9fcd5caabfd72e682a810614e9.jpg\"},{\"icon\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1564588085965&di=fa879c956373c783d467b918c0a65de6&imgtype=0&src=http%3A%2F%2Fku.90sjimg.com%2Felement_origin_min_pic%2F17%2F05%2F24%2Fbd35ed9fcd5caabfd72e682a810614e9.jpg\"},{\"icon\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1564588085965&di=fa879c956373c783d467b918c0a65de6&imgtype=0&src=http%3A%2F%2Fku.90sjimg.com%2Felement_origin_min_pic%2F17%2F05%2F24%2Fbd35ed9fcd5caabfd72e682a810614e9.jpg\"}]},{\"groupName\":\"生肖\",\"childList\":null,\"logoList\":[{\"icon\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1564588085965&di=fa879c956373c783d467b918c0a65de6&imgtype=0&src=http%3A%2F%2Fku.90sjimg.com%2Felement_origin_min_pic%2F17%2F05%2F24%2Fbd35ed9fcd5caabfd72e682a810614e9.jpg\"},{\"icon\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1564588085965&di=fa879c956373c783d467b918c0a65de6&imgtype=0&src=http%3A%2F%2Fku.90sjimg.com%2Felement_origin_min_pic%2F17%2F05%2F24%2Fbd35ed9fcd5caabfd72e682a810614e9.jpg\"},{\"icon\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1564588085965&di=fa879c956373c783d467b918c0a65de6&imgtype=0&src=http%3A%2F%2Fku.90sjimg.com%2Felement_origin_min_pic%2F17%2F05%2F24%2Fbd35ed9fcd5caabfd72e682a810614e9.jpg\"}]}],\"logoList\":null}]}}";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logolist);
        initView();
        getData();
    }

    MultiStateView multiplestatusView;

    RecyclerView recyclerview_name, recyclerview_logo;
    LogoList_NameAdapter nameAdapter;
    LogoList_LogoAdapter logoList_logoAdapter;

    GetLogListResponce.LogoGroup currentGroupInfo;

    //初始化控件
    private void initView() {
        setTitle("Logo");    //设置标题
        setLeftImgClickListener();  //设置左键返回

        multiplestatusView = findViewById(R.id.multiplestatusView);
//        multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);    //页面初始化，默认为加载中状态
        multiplestatusView.setOnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
                getData();
            }
        });

        recyclerview_name = findViewById(R.id.recyclerview_name);
        recyclerview_name.setLayoutManager(new LinearLayoutManager(mContext));
        nameAdapter = new LogoList_NameAdapter(mContext, callBack);
        recyclerview_name.setAdapter(nameAdapter);

        recyclerview_logo = findViewById(R.id.recyclerview_logo);
        recyclerview_logo.setLayoutManager(new LinearLayoutManager(mContext));
        logoList_logoAdapter = new LogoList_LogoAdapter(mContext, onLogoClick);
        recyclerview_logo.setAdapter(logoList_logoAdapter);


    }

    CommCallBack callBack = new CommCallBack() {
        @Override
        public void onResult(Object obj) {
            currentGroupInfo = (GetLogListResponce.LogoGroup) obj;
            nameAdapter.setSelectGroup(currentGroupInfo);
            logoList_logoAdapter.setData(currentGroupInfo.logoList);
        }
    };

    CommCallBack onLogoClick = new CommCallBack() {
        @Override
        public void onResult(Object obj) {
            LogoInfo info = (LogoInfo) obj;
            Intent intent = new Intent();
            intent.putExtra("logo", info);
            setResult(100, intent);
            finish();
        }
    };

    //网路请求 获取数据
    private void getData() {
        GetLogListResponce responce = new Gson().fromJson(result, GetLogListResponce.class);
        if (responce != null) {
            nameAdapter.setData(responce.data.groupList);
        }

//        //获取资产（例子）
//        API_HomeManager.getTotalBalance(mContext, new OkHttpCallBack() {
//            @Override
//            public void onSuccess(BaseResponce baseResponce) {
//                multiplestatusView.setViewState(MultiStateView.ViewState.CONTENT);    //页面状态为：显示内容
//                if (BaseResponce.Status_Success.equals(baseResponce.code)) {
//                    //返回成功
//                    GetBalanceResponce responce = (GetBalanceResponce) baseResponce;
//                    //解析数据，do something...
//                    bindUI();
//                } else {
//                    //返回错误信息
////                    CommToast.showToast(mContext, baseResponce.msg);
//                }
//            }
//
//            @Override
//            public void onFailure(BaseResponce baseResponce) {
//                //网络请求失败
//                multiplestatusView.setViewState(MultiStateView.ViewState.ERROR);    //页面状态为：显示内容
//            }
//        });
    }

    //绑定数据
    private void bindUI() {

    }

    //点击事件
    @Override
    public void onClick(View view) {
        super.onClick(view);
        Intent intent;
        switch (view.getId()) {
//            case R.id.tv_tohaibao:
//                intent = new Intent(mContext, YaoQing_HaiBaoActivity.class);
//                startActivity(intent);
//                break;

        }
    }
}
