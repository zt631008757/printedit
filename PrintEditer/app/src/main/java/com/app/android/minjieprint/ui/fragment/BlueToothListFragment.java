package com.app.android.minjieprint.ui.fragment;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.adapter.SearchDeviceListAdapter;
import com.app.android.minjieprint.dialog.PrintLoading_Dialog;
import com.app.android.minjieprint.event.Event_BeginPrint;
import com.app.android.minjieprint.event.Event_PrintProgress;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.manager.BuildPackgeManager;
import com.app.android.minjieprint.tool.CommLoading;
import com.app.android.minjieprint.tool.CommToast;
import com.app.android.minjieprint.tool.Log;
import com.app.android.minjieprint.ui.view.Comm_SubmitBtnView;
import com.app.android.minjieprint.ui.view.RoundProgressBar;
import com.app.android.minjieprint.util.BitmapUtil;
import com.app.android.minjieprint.util.ImageDispose;
import com.app.android.minjieprint.util.Util;
import com.feasycom.bean.BluetoothDeviceWrapper;
import com.feasycom.controler.FscSppApi;
import com.feasycom.controler.FscSppApiImp;
import com.feasycom.controler.FscSppCallbacksImp;
import com.lzy.okgo.utils.HttpUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Administrator on 2018/8/20.
 */

public class BlueToothListFragment extends BaseFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_bluetoothlist, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    Context mContext;
    BluetoothDeviceWrapper currentDeviceWrapper;


    View rootView;
    ImageView iv_scan;
    RecyclerView recyclerview;
    SearchDeviceListAdapter adapter;
    RoundProgressBar roundprogressbar;
    LinearLayout ll_divice_info, ll_unconnect, ll_disconnect;
    RelativeLayout rl_connected;
    Comm_SubmitBtnView csb_print;
    PrintLoading_Dialog printLoadingDialog;
    ProgressBar pb_blue, pb_red, pb_yellow;
    TextView tv_blue, tv_red, tv_yellow;

    private void initView() {
        iv_scan = rootView.findViewById(R.id.iv_scan);

        recyclerview = rootView.findViewById(R.id.recyclerview);
        recyclerview.setLayoutManager(new LinearLayoutManager(mContext));
        adapter = new SearchDeviceListAdapter(mContext, callBack);
        recyclerview.setAdapter(adapter);

        roundprogressbar = rootView.findViewById(R.id.roundprogressbar);
        ll_divice_info = rootView.findViewById(R.id.ll_divice_info);
        rl_connected = rootView.findViewById(R.id.rl_connected);
        ll_unconnect = rootView.findViewById(R.id.ll_unconnect);
        ll_disconnect = rootView.findViewById(R.id.ll_disconnect);
        csb_print = rootView.findViewById(R.id.csb_print);
        pb_blue = rootView.findViewById(R.id.pb_blue);
        pb_red = rootView.findViewById(R.id.pb_red);
        pb_yellow = rootView.findViewById(R.id.pb_yellow);
        tv_blue = rootView.findViewById(R.id.tv_blue);
        tv_red = rootView.findViewById(R.id.tv_red);
        tv_yellow = rootView.findViewById(R.id.tv_yellow);

//        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) ll_divice_info.getLayoutParams();
//        layoutParams.width = LinearLayout.LayoutParams.MATCH_PARENT;
//        layoutParams.height = (getResources().getDisplayMetrics().widthPixels - Util.dip2px(mContext, 7 + 7 + 7)) / 2;
//        ll_divice_info.setLayoutParams(layoutParams);

        iv_scan.setOnClickListener(this);
        ll_disconnect.setOnClickListener(this);
        csb_print.setOnClickListener(this);
    }

    boolean hasFirstSuccess;
    FscSppApi fscSppApi;
    Queue<BluetoothDeviceWrapper> deviceQueue = new LinkedList<BluetoothDeviceWrapper>();
    private Timer timerUI;
    private TimerTask taskUI;
    private static String[] PERMISSIONS_LOCATION = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_LOCATION_EXTRA_COMMANDS,
            Manifest.permission.BLUETOOTH_PRIVILEGED
    };
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_LOCATION_EXTRA_COMMANDS,
            Manifest.permission.BLUETOOTH_PRIVILEGED
    };
    private final int REQUEST_LOCATION = 3;
    private final int REQUEST_FILE = 4;


    @Override
    public void onResume() {
        super.onResume();
        fscSppApi = FscSppApiImp.getInstance(getActivity());
        fscSppApi.initialize();
        fscSppApi.setCallbacks(new FscSppCallbacksImp() {
            @Override
            public void sppDeviceFound(BluetoothDeviceWrapper device, int rssi) {
//                String uuid = device.getIncompleteServiceUUIDs_128bit().replace(" ", "").toLowerCase();
                if (!TextUtils.isEmpty(device.getName()) && device.getName().startsWith("PKK")) {
                    if (device.getRssi() > -100 && device.getRssi() < 0) {
                        deviceQueue.offer(device);
                    }
                }
            }

            @Override
            public void startScan() {
                startRefrsh();
            }

            @Override
            public void stopScan() {
                HttpUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        startScanBlueTooth();  //无限重复扫描
                    }
                });
//                stopRefrsh();
            }

            @Override
            public void sppConnected(BluetoothDevice device) {
                HttpUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        refreshState();
                        CommLoading.dismissLoading();
                    }
                });
            }

            @Override
            public void sppDisconnected(BluetoothDevice device) {
                currentDeviceWrapper = null;
                HttpUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        refreshState();
                    }
                });
            }

            //发送进度回调
            @Override
            public void sendPacketProgress(BluetoothDevice device, final int percentage, byte[] tempByte) {
                try {
                    HttpUtils.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (percentage != 101) {
//                                Log.i("percentage:" + percentage);
                            }
                            if (printLoadingDialog != null) {
                                if (percentage < 100) {
                                    printLoadingDialog.setProgress(percentage);
                                } else if (percentage == 100) {
//                                    new Handler().postDelayed(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            hidePrintLoadingDialog();
//                                        }
//                                    }, 1000);
                                }
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            //收到数据包
            @Override
            public void packetReceived(byte[] dataByte, final String dataString, final String dataHexString) {
                HttpUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.i("dataString: " + dataString + "  dataHexString: " + dataHexString);
                        parsePacketReceive(dataHexString);
//                        CommToast.showToast(mContext, "打印机回调：" + dataHexString, 3000);
                    }
                });
            }
        });
        int permissionLocation = ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION);
        int permissionFile = ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), PERMISSIONS_LOCATION, REQUEST_LOCATION);
        } else if (permissionFile != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), PERMISSIONS_STORAGE, REQUEST_FILE);
        }
        startScanBlueTooth();
        timerUI = new Timer();
        taskUI = new UITimerTask(new WeakReference<BlueToothListFragment>(BlueToothListFragment.this));
        timerUI.schedule(taskUI, 100, 1000);
        refreshState();
    }

    //解析蓝牙传过来的数据包
    private void parsePacketReceive(String dataString) {
        String[] arr = dataString.split(" ");
        if (arr.length == 6) {
            if ("82".equals(arr[1]))   //发数据 回调
            {
                if ("00".equals(arr[3]))  //发送成功
                {
                    if (!hasFirstSuccess)  //第一次发送成功
                    {
                        Log.i("发第1次 成功");
                        hasFirstSuccess = true;
                        sendData();
                    } else   //第二次发送成功
                    {
                        Log.i("发第2次 成功");
                        hidePrintLoadingDialog();
                        CommToast.showToast(getContext(), getString(R.string.bluetooth_print_sucess), 2000);
                    }
                } else            //发送失败
                {
                    sendFirstData();
                }
            } else if ("81".equals(arr[1]))  //电量  index 第3位数 表示电量
            {
                int battery = (int) Long.parseLong(arr[3], 16);
                setBattery(battery);
            }
        } else if (arr.length == 8) {
            if ("80".equals(arr[1]))    //墨量  index 第3 4 5位  表示CNY
            {
                int blue = (int) Long.parseLong(arr[3], 16);
                int red = (int) Long.parseLong(arr[4], 16);
                int yellow = (int) Long.parseLong(arr[5], 16);
                setRemainingInk(blue, red, yellow);
            }
        }
    }

    //设置电池电量
    private void setBattery(int battery) {
        roundprogressbar.setProgress(battery);
    }

    //设置墨盒剩余容量
    private void setRemainingInk(int blue, int red, int yellow) {
        pb_blue.setProgress(blue);
        pb_red.setProgress(red);
        pb_yellow.setProgress(yellow);

        tv_blue.setText(blue + "%");
        tv_red.setText(red + "%");
        tv_yellow.setText(yellow + "%");
    }

    //刷新连接连接状态
    private void refreshState() {
        if (currentDeviceWrapper != null) {
            rl_connected.setVisibility(View.VISIBLE);
            ll_unconnect.setVisibility(View.GONE);
        } else {
            rl_connected.setVisibility(View.GONE);
            ll_unconnect.setVisibility(View.VISIBLE);

            setBattery(0);
            setRemainingInk(0, 0, 0);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (null != timerUI) {
            timerUI.cancel();
            timerUI = null;
        }
        if (null != taskUI) {
            taskUI.cancel();
            taskUI = null;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        fscSppApi.stopScan();
        if (null != timerUI) {
            timerUI.cancel();
            timerUI = null;
        }
        if (null != taskUI) {
            taskUI.cancel();
            taskUI = null;
        }
    }

    CommCallBack callBack = new CommCallBack() {
        @Override
        public void onResult(Object obj) {
            currentDeviceWrapper = (BluetoothDeviceWrapper) obj;
            try {
                connectionBlueTooth();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private final int ENABLE_BT_REQUEST_ID = 2;

    /**
     * 开始扫描
     */
    public void startScanBlueTooth() {
        if (fscSppApi.isBtEnabled() == false) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, ENABLE_BT_REQUEST_ID);
        }
        fscSppApi.startScan(5000);
    }

    /**
     * 连接蓝牙
     */
    public void connectionBlueTooth() {
        if (currentDeviceWrapper != null) {
            fscSppApi.connect(currentDeviceWrapper.getAddress());
            CommLoading.showLoading(mContext, getString(R.string.bluetooth_connectting));
        }
    }

    /**
     * 断开连接
     */
    public void disConnectionBlueTooth() {
        try {
            if (fscSppApi.isConnected()) {
                fscSppApi.disconnect();
            }
        } catch (Exception e) {

        }
    }

    /**
     * 写数据
     */
    Bitmap bitmap;
    int reteyCount = 0;
    BuildPackgeManager manager;
    Handler handler = new Handler();

    public void printBitmap(final Bitmap bitmapTemp) {
        if (fscSppApi.isConnected()) {
            showPrintLoadingDialog(bitmapTemp);
            reteyCount = 0;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    //压缩图片
                    bitmap = BitmapUtil.compressMatrix(bitmapTemp, 0.85f, 1.0f);
                    //旋转图片
                    bitmap = BitmapUtil.rotateBitmap(bitmap, 90);
                    manager = new BuildPackgeManager(bitmap);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            sendFirstData();
                        }
                    });
                }
            }).start();
        } else {
            CommToast.showToast(mContext, getString(R.string.bluetooth_connect_tips));
        }
    }

    //第一次发送数据
    public void sendFirstData() {
        if (fscSppApi.isConnected()) {
            Log.i("发第1次");
            try {
                hasFirstSuccess = false;
                reteyCount++;
                byte[] data = manager.buildPacket_First();
                Log.i("data:" + data.toString());
                fscSppApi.send(data);
            } catch (Exception e) {
                e.printStackTrace();
            }
            //倒计时1秒 不成功 就关掉
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.i("发送失败");
                    if(!hasFirstSuccess) {
                        hidePrintLoadingDialog();
                        CommToast.showToast(getContext(), getString(R.string.bluetooth_print_fail), 2000);
                    }
                }
            }, 3000);
        } else {
            CommToast.showToast(mContext, getString(R.string.bluetooth_connect_tips));
        }
    }

    //第二次发送数据
    public void sendData() {
        if (fscSppApi.isConnected()) {
            Log.i("发第2次");
            try {
                byte[] data = manager.buildPacket_sencond();
                fscSppApi.send(data);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            CommToast.showToast(mContext, "请先连接设备");
        }
    }

    private void showPrintLoadingDialog(Bitmap bitmapTemp) {
        if (printLoadingDialog == null) {
            printLoadingDialog = new PrintLoading_Dialog(mContext);
            printLoadingDialog.setData(bitmapTemp);
            printLoadingDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    bitmap = null;
                    printLoadingDialog = null;
                }
            });
        }
        printLoadingDialog.show();
    }

    private void hidePrintLoadingDialog() {
        if (printLoadingDialog != null) {
            printLoadingDialog.dismiss();
        }
    }

    //点击事件
    @Override
    public void onClick(View view) {
        super.onClick(view);
        Intent intent;
        switch (view.getId()) {
            case R.id.iv_scan:              //开始扫描
                adapter.clearList();
                startScanBlueTooth();
                break;
            case R.id.ll_disconnect:       //断开连接
                disConnectionBlueTooth();
                break;
            case R.id.csb_print:        //开始打印
                EventBus.getDefault().post(new Event_BeginPrint());
                break;
        }
    }

    //开始旋转动画
    private void startRefrsh() {
        RotateAnimation ta = new RotateAnimation(0, 360, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        ta.setDuration(500);
        ta.setInterpolator(new LinearInterpolator());
        ta.setRepeatCount(-1);
        iv_scan.startAnimation(ta);
    }

    //结束旋转动画
    private void stopRefrsh() {
        iv_scan.clearAnimation();
    }


    class UITimerTask extends TimerTask {
        private WeakReference<BlueToothListFragment> activityWeakReference;

        public UITimerTask(WeakReference<BlueToothListFragment> activityWeakReference) {
            this.activityWeakReference = activityWeakReference;
        }

        @Override
        public void run() {
            BluetoothDeviceWrapper wrapper = activityWeakReference.get().getDeviceQueue().poll();
            if (wrapper != null) {
                activityWeakReference.get().getAdapter().addDevice(wrapper);
                HttpUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        activityWeakReference.get().getAdapter().notifyDataSetChanged();
                    }
                });
            }
        }
    }

    public SearchDeviceListAdapter getAdapter() {
        return adapter;
    }

    public Queue<BluetoothDeviceWrapper> getDeviceQueue() {
        return deviceQueue;
    }
}
