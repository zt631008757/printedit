package com.app.android.minjieprint.ui.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.bean.UploadFileInfo;
import com.app.android.minjieprint.dialog.CommList_Dialog;
import com.app.android.minjieprint.event.Event_ChangeLanguge;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.manager.LangugeManager;
import com.app.android.minjieprint.tool.CommToast;
import com.app.android.minjieprint.tool.Log;
import com.app.android.minjieprint.util.BitmapUtil;
import com.app.android.minjieprint.util.Util;
import com.app.android.minjieprint.util.VideoUtil;
import com.hloong.clipheadicon.ClipImageActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/6/12.
 */

public class BaseActivity extends AppCompatActivity implements View.OnClickListener {

    public void changeAppLanguage() {
        // 本地语言设置
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = LangugeManager.getCurrentLanguge();
        res.updateConfiguration(conf, dm);
    }

    //切换语言
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(Event_ChangeLanguge event) {
        Log.i("收到刷新语言");
        changeAppLanguage();
        recreate();//刷新界面
    }

    public Context mContext;
    public String title = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changeAppLanguage();
        mContext = this;
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);   //禁用横屏

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            builder.detectFileUriExposure();
        }
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        MobclickAgent.onResume(this);
//        MobclickAgent.onPageStart(TextUtils.isEmpty(title) ? getClass().getSimpleName() : title);
    }


    @Override
    protected void onPause() {
        super.onPause();
//        MobclickAgent.onPause(this);
//        MobclickAgent.onPageEnd(TextUtils.isEmpty(title) ? getClass().getSimpleName() : title);
    }

    //设置标题
    public void setTitle(String title) {
        this.title = title;
        TextView tv_title = (TextView) findViewById(R.id.tv_title);
        if (tv_title != null) {
            tv_title.setText(title);
        }
    }

    //设置左侧返回按钮
    public void setLeftImgClickListener() {
        ImageView public_title_left_img = (ImageView) findViewById(R.id.public_title_left_img);
        public_title_left_img.setVisibility(View.VISIBLE);
        public_title_left_img.setOnClickListener(this);
    }

    //设置左侧按钮图片
    public void setLeftImgResouce(int resouce) {
        ImageView public_title_left_img = (ImageView) findViewById(R.id.public_title_left_img);
        public_title_left_img.setImageResource(resouce);
    }

    //设置右侧图片按钮
    public void setRightImgClickListener(int rightImgResouceID) {
        ImageView public_title_right_img = (ImageView) findViewById(R.id.public_title_right_img);
        public_title_right_img.setImageResource(rightImgResouceID);
        public_title_right_img.setVisibility(View.VISIBLE);
        public_title_right_img.setOnClickListener(this);
    }
    //设置右侧图片按钮
    public void setRightImg1ClickListener(int rightImgResouceID) {
        ImageView public_title_right_img1 = (ImageView) findViewById(R.id.public_title_right_img1);
        public_title_right_img1.setImageResource(rightImgResouceID);
        public_title_right_img1.setVisibility(View.VISIBLE);
        public_title_right_img1.setOnClickListener(this);
    }

    //设置右侧文字
    public void setRightTextClickListener(String text) {
        TextView tv_right_text = (TextView) findViewById(R.id.tv_right_text);
        tv_right_text.setText(text);
        tv_right_text.setVisibility(View.VISIBLE);
        tv_right_text.setOnClickListener(this);
    }

    public void setRightTextEnable(boolean isEnable) {
        TextView tv_right_text = (TextView) findViewById(R.id.tv_right_text);
        tv_right_text.setEnabled(isEnable);
        if (isEnable) {
            tv_right_text.setTextColor(getResources().getColor(R.color.mainColor));
        } else {
            tv_right_text.setTextColor(Color.parseColor("#cccccc"));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.public_title_left_img:
               onBackPressed();
                break;
        }
    }

    protected void setFullscreen() {
        //计算头部高度  (5.0.1系统以上)
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            //设置透明状态栏,这样才能让 ContentView 向上
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            //需要设置这个 flag 才能调用 setStatusBarColor 来设置状态栏颜色
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            //设置状态栏颜色
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
            ViewGroup mContentView = (ViewGroup) findViewById(Window.ID_ANDROID_CONTENT);
            View mChildView = mContentView.getChildAt(0);
            if (mChildView != null) {
                //注意不是设置 ContentView 的 FitsSystemWindows, 而是设置 ContentView 的第一个子 View . 使其不为系统 View 预留空间.
                ViewCompat.setFitsSystemWindows(mChildView, false);
            }

//            setMiuiStatusBarDarkMode(this,true);
//            setMeizuStatusBarDarkIcon(this,true);
        }
    }

    public boolean checkPermission(@NonNull String permission) {
        return ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED;
    }

    public static final int REQUEST_PERMISSION_STORAGE = 101;
    public static final int REQUEST_PERMISSION_CAMERA = 102;

    //---------------------------上传照片逻辑----------------------------------------


    CommCallBack upLoadCallBack;
    int width = 1;          //默认尺寸比例
    int height = 1;
    boolean isCrop = false;  //是否需要裁剪
    boolean isVideo = false;  //传视频还是传照片

    private final int REQUEST_LOCAL = 1;
    private final int REQUEST_CAMERA = 2;
    private final int REQUEST_CUT = 3;
    private final int REQUEST_LOCAL_Video = 4;
    private final int REQUEST_CAMERA_Video = 5;
    public static final String IMAGE_FILE_NAME = "clip_temp.jpg";
    public static final String Video_FILE_NAME = "temp.mp4";

    public static final String PASS_PATH = "pass_path";

    public void upLoadPhoto(boolean isCrop, int width, int height, final CommCallBack callBack) {
        this.isCrop = isCrop;
        this.width = width;
        this.height = height;
        this.isVideo = false;
        upLoadPhoto(isCrop, callBack);
    }

    public void upLoadPhoto(boolean isCrop, final CommCallBack callBack) {
        this.isCrop = isCrop;
        this.upLoadCallBack = callBack;
        this.isVideo = false;
        List<String> list = new ArrayList<>();
        list.add(Util.getString(R.string.comm_camare));
        list.add(Util.getString(R.string.comm_gallery));
        CommList_Dialog dialog = new CommList_Dialog(mContext);
        dialog.setData(list, null, false, true);
        dialog.setCallBack(new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                int selectStr = (int) obj;
                if (selectStr == 0) {
                    checkCameraPermission();
                } else if (selectStr == 1) {
                    checkFilePermission();
                }
            }
        });
        dialog.show();
    }

    public void upLoadVideo(final CommCallBack callBack) {
        this.upLoadCallBack = callBack;
        this.isVideo = true;
        List<String> list = new ArrayList<>();
        list.add("拍照");
        list.add("相册");
        CommList_Dialog dialog = new CommList_Dialog(mContext);
        dialog.setData(list, null, true, true);
        dialog.setCallBack(new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                int selectStr = (int) obj;
                if (selectStr == 0) {
                    startVideoCamera();
                } else if (selectStr == 1) {
                    startVideoGallery();
                }
            }
        });
        dialog.show();
    }

    private void doUploadImg(File file) {
        if (upLoadCallBack != null) {
            String filename = "img_" + System.currentTimeMillis() + ".jpg";
            File tempFile = Util.compressImageToFile(file, filename);
            Bitmap bitmap = BitmapUtil.getSmallBitmap(tempFile.getAbsolutePath());

            UploadFileInfo fileInfo = new UploadFileInfo();
            fileInfo.width = bitmap.getWidth();
            fileInfo.height = bitmap.getHeight();
            fileInfo.file = tempFile;
            fileInfo.bitmap = bitmap;
            upLoadCallBack.onResult(fileInfo);
        }
//        showLoadingDialog("正在上传");
//        try {
//            File tempFile = Util.compressImageToFile(file);
//            final Bitmap bitmap = BitmapFactory.decodeFile(tempFile.getAbsolutePath());
//            API_LoginManager.upload(mContext, Arrays.asList(tempFile), "11.jpg", new OkHttpCallBack() {
//                @Override
//                public void onSuccess(BaseResponce baseResponce) {
//                    StringResponce responce = (StringResponce) baseResponce;
//                    dismissLoadingDialog();
//                    if ("200".equals(baseResponce.code)) {
//                        if (upLoadCallBack != null) {
//                            Log.i("上传成功 url:" + responce.data);
//                            UploadFileInfo fileInfo = new UploadFileInfo();
//                            fileInfo.url = responce.data;
//                            fileInfo.width = bitmap.getWidth();
//                            fileInfo.height = bitmap.getHeight();
//                            upLoadCallBack.onResult(fileInfo);
//                        }
//                    } else {
//                        CommToast.showToast(mContext, baseResponce.msg);
//                    }
//                }
//
//                @Override
//                public void onFailure(BaseResponce baseResponce) {
//                    dismissLoadingDialog();
//                    CommToast.showToast(mContext, "请求失败，请重试");
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    private void doUploadVideo(final VideoUtil.VideoInfo videoInfo) {
        try {
            final File tempFile = new File(videoInfo.path);
//            API_LoginManager.upload(mContext, Arrays.asList(tempFile), "11.mp4", new OkHttpCallBack() {
//                @Override
//                public void onSuccess(BaseResponce baseResponce) {
//                    StringResponce responce = (StringResponce) baseResponce;
//                    dismissLoadingDialog();
//                    if ("200".equals(baseResponce.code)) {
//                        if (upLoadCallBack != null) {
//                            Log.i("上传成功 url:" + responce.data);
//                            UploadFileInfo fileInfo = new UploadFileInfo();
//                            fileInfo.url = responce.data;
//                            fileInfo.width = videoInfo.width;
//                            fileInfo.height = videoInfo.height;
//                            fileInfo.duration = videoInfo.duration;
//                            fileInfo.isVideo = true;
//                            fileInfo.thumb = videoInfo.thumbPath;
//                            upLoadCallBack.onResult(fileInfo);
//                        }
//                    } else {
//                        CommToast.showToast(mContext, baseResponce.msg);
//                    }
//                }
//
//                @Override
//                public void onFailure(BaseResponce baseResponce) {
//                    dismissLoadingDialog();
//                    CommToast.showToast(mContext, "请求失败，请重试");
//                }
//            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //检查相册文件读取权限
    private void checkFilePermission() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_STORAGE);
                return;
            }
        }
        startGallery();
    }

    //打开相册
    private void startGallery() {
        Intent intentFromGallery;
        if (Build.VERSION.SDK_INT >= 19) { // 判断是不是4.4
            intentFromGallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        } else {
            intentFromGallery = new Intent(Intent.ACTION_GET_CONTENT);
        }
        intentFromGallery.setType("image/*"); // 设置文件类型
        startActivityForResult(intentFromGallery, REQUEST_LOCAL);
    }

    //检查拍照权限
    private void checkCameraPermission() {
        if (!checkPermission(Manifest.permission.CAMERA) || !checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_CAMERA);
        } else {
            startCamera();
        }
    }

    //打开相机
    private void startCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(getFile()));
        startActivityForResult(cameraIntent, REQUEST_CAMERA);
    }

    //打开相机录视频
    private void startVideoCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(getFile()));
//        cameraIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
//        // 限制时长 ，参数61代表61秒，可以根据需求自己调，最高应该是2个小时。
//        //当在这里设置时长之后，录制到达时间，系统会自动保存视频，停止录制
//        cameraIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 5000);
//        // 限制大小 限制视频的大小，这里是100兆。当大小到达的时候，系统会自动停止录制
////        cameraIntent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, 1024 * 1024 * 2L);
        startActivityForResult(cameraIntent, REQUEST_CAMERA);
    }

    //打开视频相册
    private void startVideoGallery() {
        /**
         * 从相册中选择视频
         */
        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT < 19) {
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.setType("video/*");
        } else {
            intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("video/*");
        }
        startActivityForResult(Intent.createChooser(intent, "选择要上传的视频"), REQUEST_LOCAL_Video);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startGallery();
            } else {
                CommToast.showToast(getApplicationContext(), "权限被禁止，无法选择本地图片");
            }
        } else if (requestCode == REQUEST_PERMISSION_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startCamera();
            } else {
                CommToast.showToast(getApplicationContext(), "权限被禁止，无法打开相机");
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }

        switch (requestCode) {
            case REQUEST_CUT:     //裁剪返回
                //在此处来做图片的上传处理
                if (isCrop)   //裁剪完成
                {
                    File file = new File(data.getStringExtra(ClipImageActivity.RESULT_PATH));
                    doUploadImg(file);
                }
                break;
            case REQUEST_LOCAL:
                if (!isCrop)   //不用裁剪
                {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            File temp = Util.uri2File(mContext, data.getData());
                            if (temp != null) {
                                doUploadImg(temp);
                            } else {
                                CommToast.showToast(mContext, "图片不存在");
                            }
                        }
                    });
                } else {
                    startCropImageActivity(getFilePath(data.getData()));
                }
                break;
            case REQUEST_CAMERA:
                // 照相机程序返回的,再次调用图片剪辑程序去修剪图片
                if (!isCrop)   //不用裁剪
                {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            File temp = getFile();
                            if (temp != null) {
                                doUploadImg(temp);
                            } else {
                                CommToast.showToast(mContext, "图片不存在");
                            }
                        }
                    });
                } else {
                    startCropImageActivity(Environment.getExternalStorageDirectory() + "/" + IMAGE_FILE_NAME);
                }
                break;
            case REQUEST_LOCAL_Video:   //本地选取视频
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        VideoUtil.VideoInfo videoInfo = VideoUtil.getVideoDataFromUri(mContext, data.getData());

                        VideoUtil.getVideoInfo(videoInfo.path);

                        doUploadVideo(videoInfo);
                    }
                });
                break;
            case REQUEST_CAMERA_Video:   //本地录制视频
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        File temp = getFile();
                        if (temp != null) {
                            doUploadImg(temp);
                        } else {
                            CommToast.showToast(mContext, "图片不存在");
                        }
                    }
                });
                break;
        }
    }

    private void startCropImageActivity(String path) {
        Intent intent = new Intent(this, ClipImageActivity.class);
        intent.putExtra(PASS_PATH, path);
        intent.putExtra("widthScale", width);
        intent.putExtra("heightScale", height);
        startActivityForResult(intent, REQUEST_CUT);
    }

    /**
     * 获取file的时候如果没有路径就重新创建
     *
     * @return
     */
    private File getFile() {
        File file;
        if (isVideo) {
            file = new File(Environment.getExternalStorageDirectory(), Video_FILE_NAME);
        } else {
            file = new File(Environment.getExternalStorageDirectory(), IMAGE_FILE_NAME);
        }
        if (!file.exists()) {
            file.getParentFile().mkdirs();
        }
        return file;
    }

    /**
     * 通过uri获取文件路径
     *
     * @param mUri
     * @return
     */
    public String getFilePath(Uri mUri) {
        try {
            if (mUri.getScheme().equals("file")) {
                return mUri.getPath();
            } else {
                return getFilePathByUri(mUri);
            }
        } catch (FileNotFoundException ex) {
            return null;
        }
    }

    // 获取文件路径通过url
    private String getFilePathByUri(Uri mUri) throws FileNotFoundException {
        Cursor cursor = getContentResolver().query(mUri, null, null, null, null);
        cursor.moveToFirst();
        return cursor.getString(1);
    }


}
