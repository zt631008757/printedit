package com.app.android.minjieprint.manager;

import android.content.Context;
import android.view.View;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.bean.ElementsInfo;
import com.app.android.minjieprint.ui.view.ElementView_Base;
import com.app.android.minjieprint.ui.view.ElementView_ErWeiMa;
import com.app.android.minjieprint.ui.view.ElementView_Img;
import com.app.android.minjieprint.ui.view.ElementView_Line;
import com.app.android.minjieprint.ui.view.ElementView_Rect;
import com.app.android.minjieprint.ui.view.ElementView_Text;
import com.app.android.minjieprint.ui.view.ElementView_TiaoMa;
import com.app.android.minjieprint.ui.view.ElementView_Time;
import com.app.android.minjieprint.ui.view.ElementView_XuLieHao;

import java.util.ArrayList;
import java.util.List;

public class ElementManager {

    public static List<ElementsInfo> getElementList() {
        //1:文本  2：条码  3：二维码  4：图片  5：Logo   6：线条  7 形状   8:序列号   9：时间  10 扫码  11 语音识别  12 图像识别
        List<ElementsInfo> list = new ArrayList<>();
        list.add(new ElementsInfo(R.string.edit_ele_text, "文本", R.drawable.ico_element_wenben));
        list.add(new ElementsInfo(R.string.edit_ele_tiaoma, "条码", R.drawable.ico_ele_tiaoma));
        list.add(new ElementsInfo(R.string.edit_ele_erweima, "二维码", R.drawable.ico_ele_erweima));
        list.add(new ElementsInfo(R.string.edit_ele_img, "图片", R.drawable.ico_ele_img));
//        list.add(new ElementsInfo(R.string.edit_ele_logo, "Logo", R.drawable.ico_ele_logo1));
        list.add(new ElementsInfo(R.string.edit_ele_line, "线条", R.drawable.ico_ele_xiantiao));
        list.add(new ElementsInfo(R.string.edit_ele_rect, "形状", R.drawable.ico_ele_juxing));
//        list.add(new ElementsInfo(R.string.edit_ele_xueliehao, "序列号", R.drawable.ico_ele_xuliehao));
        list.add(new ElementsInfo(R.string.edit_ele_time, "时间", R.drawable.ico_ele_time));
        list.add(new ElementsInfo(R.string.edit_ele_saoma, "扫码", R.drawable.ico_ele_saoma));
        list.add(new ElementsInfo(R.string.edit_ele_voice, "语音识别", R.drawable.ico_ele_yuyinshibie));
        list.add(new ElementsInfo(R.string.edit_ele_img_shibie, "图像识别", R.drawable.ico_ele_tuxiangshibie));
        return list;
    }

    public static ElementsInfo getElementInfo(String name) {
        //1:文本  2：条码  3：二维码  4：图片  5：Logo   6：线条  7 形状   8:序列号   9：时间  10 扫码  11 语音识别  12 图像识别
        ElementsInfo elementsInfo = new ElementsInfo(name);
        switch (name) {
            case "文本":
                elementsInfo.width = 1200;
                elementsInfo.height = 320;
                elementsInfo.text = "";
//                elementsInfo.fontsize = 60;
                break;
            case "条码":
                elementsInfo.text = "123456789012";
                elementsInfo.width = 400;
                elementsInfo.height = 300;
                break;
            case "二维码":
                elementsInfo.text = "123456789012";
                elementsInfo.width = 300;
                elementsInfo.height = 300;
                break;
            case "图片":
                elementsInfo.width = 300;
                elementsInfo.height = 300;
                break;
            case "Logo":
                elementsInfo.width = 300;
                elementsInfo.height = 300;
                break;
            case "时间":
                elementsInfo.width = 2000;
                elementsInfo.height = 320;
                break;
            case "序列号":
                elementsInfo.width = 500;
                elementsInfo.height = 320;
                elementsInfo.text = "001";
                break;
            case "线条":
                elementsInfo.width = 600;
                elementsInfo.height = 121;
                break;
            default:
                elementsInfo.width = 300;
                elementsInfo.height = 300;
                break;
        }
        return elementsInfo;
    }

    public static ElementView_Base getElementView(Context mContext, ElementsInfo info) {
        ElementView_Base view = null;
        //1:文本  2：条码  3：二维码  4：图片  5：Logo   6：线条  7 形状   8:序列号   9：时间  10 扫码  11 语音识别  12 图像识别
        switch (info.viewtype) {
            case "文本":
                view = new ElementView_Text(mContext, info);
                break;
            case "条码":
                view = new ElementView_TiaoMa(mContext, info);
                break;
            case "二维码":
                view = new ElementView_ErWeiMa(mContext, info);
                break;
            case "图片":
                view = new ElementView_Img(mContext, info);
                break;
            case "线条":
                view = new ElementView_Line(mContext, info);
                break;
            case "形状":
                view = new ElementView_Rect(mContext, info);
                break;
            case "时间":
                view = new ElementView_Time(mContext, info);
                break;
            case "序列号":   //序列号
                view = new ElementView_XuLieHao(mContext, info);
                break;
            default:
                view = new ElementView_Rect(mContext, info);
                break;
        }
        return view;
    }
}
