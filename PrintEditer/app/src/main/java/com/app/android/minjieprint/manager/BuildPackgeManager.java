package com.app.android.minjieprint.manager;

import android.graphics.Bitmap;
import android.graphics.Color;

import com.app.android.minjieprint.tool.Log;
import com.app.android.minjieprint.util.BitmapUtil;
import com.app.android.minjieprint.util.PackBits;

import java.io.IOException;
import java.util.Arrays;

public class BuildPackgeManager {

    byte[] imageData_first;
    byte[] imageData_copy;

    Bitmap bitmap_first;
    Bitmap bitmap_copy;   //镜像后的图片

    public BuildPackgeManager(Bitmap baseBitmap) {
        this.bitmap_first = baseBitmap;
        this.bitmap_copy = BitmapUtil.convertBitmap(baseBitmap);

        imageData_first = new DataFamet(bitmap_first).getImageData();
        imageData_copy = new DataFamet(bitmap_copy).getImageData();

    }

    //生成第一次握手包
    public byte[] buildPacket_First() {
        byte[] bytes = new byte[20 + 24];   // 发送数据总长度为header 12字节 + png数据长度
        //头部数据
        byte[] head0 = {(byte) 0x80, (byte) 0x80, (byte) 0x80, (byte) 0x80};    //固定值
        byte[] head1 = intToByte(imageData_first.length);                             //第1张图数据数组长度
        byte[] head4 = intToByte(imageData_copy.length);                        //第2张图数据数组长度
        byte[] head2 = intToByte(0x88);                                     //固定值
        byte[] head3 = intToByte(bitmap_first.getHeight());                          //图片列数
        byte[] head_cmy = new byte[24];
        for (int i = 0; i < 24; i++) {
            head_cmy[i] = 0;
        }

        //组包  头部
        System.arraycopy(head0, 0, bytes, 0, 4);
        System.arraycopy(head1, 0, bytes, 4, 4);
        System.arraycopy(head4, 0, bytes, 8, 4);
        System.arraycopy(head2, 0, bytes, 12, 4);
        System.arraycopy(head3, 0, bytes, 16, 4);

        System.arraycopy(head_cmy, 0, bytes, 20, 24);

        return bytes;    // 返回的就是组好包的 byte [], 通过FscBleCentralApi的send接口发送出去
    }


    //生成第二次数据包
    public byte[] buildPacket_sencond() {
        byte[] bytes = new byte[imageData_first.length + imageData_copy.length];
        System.arraycopy(imageData_first, 0, bytes, 0, imageData_first.length);
        System.arraycopy(imageData_copy, 0, bytes, imageData_first.length, imageData_copy.length);

        return bytes;    // 返回的就是组好包的 byte [], 通过FscBleCentralApi的send接口发送出去
    }

    private byte[] intToByte(int i) {
        byte[] result = new byte[4];
        // 由高位到低位
        result[0] = (byte) ((i >> 24) & 0xFF);
        result[1] = (byte) ((i >> 16) & 0xFF);
        result[2] = (byte) ((i >> 8) & 0xFF);
        result[3] = (byte) (i & 0xFF);
        return result;
    }

    private class DataFamet {
        long[] cByte;
        long[] mByte;
        long[] yByte;
        byte[] bytesArr_sum;
        byte[] imageData;

        Bitmap bitmap;
        int imgWidth = 0;
        int imgHeight = 0;
        int arrLength = 0;


        public DataFamet(Bitmap baseBitmap) {
            this.bitmap = baseBitmap;

            imgWidth = bitmap.getWidth();
            imgHeight = bitmap.getHeight();

            arrLength = imgWidth * imgHeight * 2;

            cByte = new long[arrLength];
            mByte = new long[arrLength];
            yByte = new long[arrLength];
        }

        public byte[] getImageData() {
            extractPixelData();
            mergeCMYData();
            packbitsEncode();
            return imageData;
        }

        // [4] 提取像素数据, RGB转CMY并分别填充到cBits, mBits, yBits
        private void extractPixelData() {
            int cnt = 0;
            int nozzleNum = 0;
            for (int y = 0; y < imgHeight; y++) {
                for (int x = 0; x < imgWidth; x++) {
                    int pixel = bitmap.getPixel(x, y);
                    int cyan = 0xff - Color.red(pixel);
                    int meg = 0xff - Color.green(pixel);
                    int yel = 0xff - Color.blue(pixel);

                    if ((cnt % 2) == 1) {
                        if ((cyan & 0x80) == 0x80) {
                            nozzleNum = cnt * 2 + 1;
                            cByte[nozzleNum] = 1;
                        }
                        if ((cyan & 0x40) == 0x40) {
                            nozzleNum = cnt * 2;
                            cByte[nozzleNum] = 1;
                        }
                        if ((meg & 0x80) == 0x80) {
                            nozzleNum = cnt * 2 + 1;
                            mByte[nozzleNum] = 1;
                        }
                        if ((meg & 0x40) == 0x40) {
                            nozzleNum = cnt * 2;
                            mByte[nozzleNum] = 1;
                        }
                        if ((yel & 0x80) == 0x80) {
                            nozzleNum = cnt * 2 + 1;
                            yByte[nozzleNum] = 1;
                        }
                        if ((yel & 0x40) == 0x40) {
                            nozzleNum = cnt * 2;
                            yByte[nozzleNum] = 1;
                        }
                    } else {
                        if ((cyan & 0x80) == 0x80) {
                            nozzleNum = cnt * 2;
                            cByte[nozzleNum] = 1;
                        }
                        if ((cyan & 0x40) == 0x40) {
                            nozzleNum = cnt * 2 + 1;
                            cByte[nozzleNum] = 1;
                        }
                        if ((meg & 0x80) == 0x80) {
                            nozzleNum = cnt * 2;
                            mByte[nozzleNum] = 1;
                        }
                        if ((meg & 0x40) == 0x40) {
                            nozzleNum = cnt * 2 + 1;
                            mByte[nozzleNum] = 1;
                        }
                        if ((yel & 0x80) == 0x80) {
                            nozzleNum = cnt * 2;
                            yByte[nozzleNum] = 1;
                        }
                        if ((yel & 0x40) == 0x40) {
                            nozzleNum = cnt * 2 + 1;
                            yByte[nozzleNum] = 1;
                        }
                    }
                    ++cnt;
                }
            }
//            Log.i("cByte.length:" + cByte.length);
//            Log.i("cByte:" + Arrays.toString(cByte));
        }

        // [5] 将CMY组合到字节流cmyBytes
        private void mergeCMYData() {
            int size = cByte.length / 8;
            byte[] bytesArr_c = new byte[size];
            byte[] bytesArr_m = new byte[size];
            byte[] bytesArr_y = new byte[size];

            for (int i = 0; i < size; i++) {
                for (int m = 0; m < 8; m++) {
                    bytesArr_c[i] += (cByte[i * 8 + m] << m);
                }
                for (int m = 0; m < 8; m++) {
                    bytesArr_m[i] += (mByte[i * 8 + m] << m);
                }
                for (int m = 0; m < 8; m++) {
                    bytesArr_y[i] += (yByte[i * 8 + m] << m);
                }
            }
            bytesArr_sum = new byte[size * 3];
            int size86 = size / 86;
            for (int i = 0; i < size86; i++) {
                for (int m = 0; m < 86; m++) {
                    bytesArr_sum[i * 86 * 3 + m] = bytesArr_y[i * 86 + m];
                }
                for (int m = 0; m < 86; m++) {
                    bytesArr_sum[i * 86 * 3 + 86 + m] = bytesArr_m[i * 86 + m];
                }
                for (int m = 0; m < 86; m++) {
                    bytesArr_sum[i * 86 * 3 + 86 + 86 + m] = bytesArr_c[i * 86 + m];
                }
            }
        }

        private void packbitsEncode() {
            try {
                imageData = PackBits.compress(bytesArr_sum);
//                Log.i("imageData_first:" + Arrays.toString(imageData_first));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
