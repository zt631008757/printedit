package com.app.android.minjieprint.adapter;

import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.graphics.Color;
import android.os.ParcelUuid;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.tool.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/8/22.
 */

public class MyBlueToothListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    List<BluetoothDevice> list = new ArrayList<>();
    CommCallBack callBack;

    public MyBlueToothListAdapter(Context mContext, CommCallBack callBack) {
        this.mContext = mContext;
        this.callBack = callBack;
    }

    public void setData(List<BluetoothDevice> list) {
        this.list = list;
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        ViewGroup viewGroup = (ViewGroup) mInflater.inflate(R.layout.item_bluetoothlist, null, false);
        ContentViewHolder holder = new ContentViewHolder(viewGroup);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ContentViewHolder viewHolder = (ContentViewHolder) holder;
        final BluetoothDevice info = list.get(position);

        ParcelUuid[] Uuids = info.getUuids();

        if(Uuids!=null) {
            for (int i = 0; i < Uuids.length; i++) {
                Log.i("Uuids:" + Uuids[i].getUuid());
            }
        }

        int deviceClass = info.getBluetoothClass().getMajorDeviceClass();
        Log.i("deviceClass:" + deviceClass);
//        String className = "";
//        switch (deviceClass) {
//            case BluetoothClass.Device.Major.IMAGING:
//                className = "打印机";
//                break;
//            case BluetoothClass.Device.Major.PHONE:
//                className = "手机";
//                break;
//            case BluetoothClass.Device.Major.COMPUTER:
//                className = "电脑";
//                break;
//            case BluetoothClass.Device.Major.UNCATEGORIZED:
//                className = "未分类";
//                break;
//            case BluetoothClass.Device.Major.AUDIO_VIDEO:
//                className = "耳机";
//                break;
//            default:
//                className = "未知";
//                break;
//        }
        viewHolder.tv_text.setText((TextUtils.isEmpty(info.getName()) ? "未知" : info.getName()) + "  (" + info.getAddress() + ")");

        viewHolder.tv_statu.setVisibility(View.GONE);
        viewHolder.ll_loading.setVisibility(View.GONE);
        String statu = "";
        if (info.getBondState() == BluetoothDevice.BOND_BONDED) {
            statu = "已配对";
            viewHolder.tv_statu.setTextColor(Color.parseColor("#666666"));
            viewHolder.tv_statu.setVisibility(View.VISIBLE);
            stopRefrsh(viewHolder.iv_loading);
        } else if (info.getBondState() == BluetoothDevice.BOND_BONDING) {    //配对中
            viewHolder.tv_statu.setTextColor(Color.parseColor("#999999"));
            viewHolder.ll_loading.setVisibility(View.VISIBLE);
            startRefrsh(viewHolder.iv_loading);
        } else if (info.getBondState() == BluetoothDevice.BOND_NONE) {
            statu = "未配对";
            viewHolder.tv_statu.setTextColor(Color.parseColor("#999999"));
            viewHolder.tv_statu.setVisibility(View.VISIBLE);
            stopRefrsh(viewHolder.iv_loading);
        }
        viewHolder.tv_statu.setText(statu);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callBack != null) {
                    callBack.onResult(info);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    //开始旋转动画
    private void startRefrsh(View view) {
        RotateAnimation ta = new RotateAnimation(0, 360, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        ta.setDuration(500);
        ta.setInterpolator(new LinearInterpolator());
        ta.setRepeatCount(-1);
        view.startAnimation(ta);
    }

    //结束旋转动画
    private void stopRefrsh(View view) {
        view.clearAnimation();
    }

    class ContentViewHolder extends RecyclerView.ViewHolder {
        TextView tv_text, tv_statu;
        LinearLayout ll_loading;
        ImageView iv_loading;

        public ContentViewHolder(View itemView) {
            super(itemView);
            tv_text = itemView.findViewById(R.id.tv_text);
            tv_statu = itemView.findViewById(R.id.tv_statu);

            ll_loading = itemView.findViewById(R.id.ll_loading);
            iv_loading = itemView.findViewById(R.id.iv_loading);


        }
    }
}
