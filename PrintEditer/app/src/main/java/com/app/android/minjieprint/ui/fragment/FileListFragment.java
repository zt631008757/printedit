package com.app.android.minjieprint.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.adapter.FileListAdapter;
import com.app.android.minjieprint.bean.LocalFileInfo;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.interface_.OkHttpCallBack;
import com.app.android.minjieprint.manager.API_LoginManager;
import com.app.android.minjieprint.manager.UserManager;
import com.app.android.minjieprint.responce.BaseResponce;
import com.app.android.minjieprint.responce.GetFileResponce;
import com.app.android.minjieprint.tool.Log;
import com.app.android.minjieprint.ui.activity.LoginActivity;
import com.app.android.minjieprint.ui.view.Comm_SubmitBtnView;
import com.app.android.minjieprint.ui.view.MultiStateView;
import com.google.gson.Gson;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/8/20.
 */

public class FileListFragment extends BaseFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_filelist, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (rootView != null) {
            checkLogin();
            getData();
        }
    }

    public int type = 0;  //0 本地， 1 云目录
    List<LocalFileInfo> localFileList = new ArrayList<>();

    Context mContext;
    View rootView;
    MultiStateView multiplestatusView;
    LinearLayout ll_login;
    RecyclerView recyclerview;
    Comm_SubmitBtnView tv_login;

    FileListAdapter adapter;

    private void initView() {
        multiplestatusView = rootView.findViewById(R.id.multiplestatusView);
        multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);    //页面初始化，默认为加载中状态
        multiplestatusView.setOnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
                getData();
            }
        });

        ll_login = rootView.findViewById(R.id.ll_login);
        recyclerview = rootView.findViewById(R.id.recyclerview);
        LinearLayoutManager gm = new LinearLayoutManager(mContext);
        recyclerview.setLayoutManager(gm);
        adapter = new FileListAdapter(mContext, callBack);
        recyclerview.setAdapter(adapter);


        tv_login = rootView.findViewById(R.id.tv_login);
        tv_login.setOnClickListener(this);
    }

    CommCallBack callBack = new CommCallBack() {
        @Override
        public void onResult(Object obj) {
            String result = (String) obj;
            if ("refresh".equals(result)) {
                getData();
            }

        }
    };


    private void checkLogin() {
        if (type == 1) {
            if (UserManager.isLogin(mContext)) {
                ll_login.setVisibility(View.GONE);
            } else {
                ll_login.setVisibility(View.VISIBLE);
            }
        } else {
            ll_login.setVisibility(View.GONE);
        }
    }

    //网路请求 获取数据
    private void getData() {
        if (type == 0) {
            localFileList = DataSupport.findAll(LocalFileInfo.class);
            Log.i("localFileList:" + localFileList.size());
            bindUI();
        } else {
            API_LoginManager.docs(mContext, new OkHttpCallBack() {
                @Override
                public void onSuccess(BaseResponce baseResponce) {
                    try {
                        GetFileResponce fileResponce = (GetFileResponce) baseResponce;
                        Log.i("fileResponce.data.size:" + fileResponce.data.size());
                        localFileList.clear();
                        for (int i = 0; i < fileResponce.data.size(); i++) {
                            LocalFileInfo fileInfo = new Gson().fromJson(fileResponce.data.get(i).content, LocalFileInfo.class);
                            localFileList.add(fileInfo);
                        }
                        bindUI();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(BaseResponce baseResponce) {
                    multiplestatusView.setViewState(MultiStateView.ViewState.ERROR);
                }
            });
        }
    }

    //绑定数据
    private void bindUI() {
        adapter.setData(localFileList);
        if (adapter.getItemCount() == 0) {
            multiplestatusView.setViewState(MultiStateView.ViewState.EMPTY);
        } else {
            multiplestatusView.setViewState(MultiStateView.ViewState.CONTENT);
        }
    }

    //点击事件
    @Override
    public void onClick(View view) {
        super.onClick(view);
        Intent intent;
        switch (view.getId()) {
            case R.id.tv_login:
                intent = new Intent(mContext, LoginActivity.class);
                startActivity(intent);

//                // 打开注册页面
//                RegisterPage registerPage = new RegisterPage();
//                // 使用自定义短信模板(不设置则使用默认模板)
////                registerPage.setTempCode(TEMP_CODE);
//                registerPage.setRegisterCallback(new EventHandler() {
//                    public void afterEvent(int event, int result, Object data) {
//                        // 解析注册结果
//                        if (result == SMSSDK.RESULT_COMPLETE) {
//                            @SuppressWarnings("unchecked")
//                            HashMap<String,Object> phoneMap = (HashMap<String, Object>) data;
//                            String country = (String) phoneMap.get("country");
//                            String phone = (String) phoneMap.get("phone");
//                            // 提交用户信息
////						registerUser(country, phone);
//                        }
//                    }
//                });
//                registerPage.show(mContext);

                break;

        }
    }
}
