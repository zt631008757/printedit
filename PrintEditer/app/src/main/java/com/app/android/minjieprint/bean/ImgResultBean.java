package com.app.android.minjieprint.bean;

import java.io.Serializable;
import java.util.List;

public class ImgResultBean implements Serializable {

    public List<Words_result> words_result;

    public class Words_result implements Serializable {
        public String words;
    }

}
