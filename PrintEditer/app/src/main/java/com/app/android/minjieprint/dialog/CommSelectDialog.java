package com.app.android.minjieprint.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.bean.CommObjectInfo;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.ui.view.citypiker.adapter.MyWheelViewAdapter;
import com.app.android.minjieprint.ui.view.citypiker.widget.OnWheelChangedListener;
import com.app.android.minjieprint.ui.view.citypiker.widget.WheelView;
import com.app.android.minjieprint.util.AnimUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/7/29.
 */

public class CommSelectDialog extends Dialog implements OnWheelChangedListener, View.OnClickListener {

    Context context;

    public CommSelectDialog(@NonNull Context context) {
        super(context, R.style.myDialog);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_comm_select_wheel);
        initView();
    }

    CommCallBack callback;
    int select;

    public void setData(List<CommObjectInfo> hourList, int select, CommCallBack callback) {
        this.hourList = hourList;
        this.select = select;
        this.callback = callback;
    }

    public static final int VISIBLE_ITEMS = 7;

    List<CommObjectInfo> hourList = new ArrayList<>();

    WheelView wv_month;
    TextView tv_cancel, tv_ok;
    LinearLayout ll_content;
    View view_bg;

    private void initView() {
        wv_month = (WheelView) findViewById(R.id.month);
        initMonth();

        wv_month.setCurrentItem(select);

        wv_month.setCyclic(false);
        wv_month.setVisibleItems(VISIBLE_ITEMS);
        wv_month.addChangingListener(this);

        ll_content = (LinearLayout) findViewById(R.id.ll_content);
        view_bg = findViewById(R.id.view_bg);
        tv_cancel = findViewById(R.id.tv_cancel);
        tv_ok = findViewById(R.id.tv_ok);
        tv_cancel.setOnClickListener(this);
        tv_ok.setOnClickListener(this);
        view_bg.setOnClickListener(this);
        ll_content.setOnClickListener(this);
        AnimUtil.fadeIn(view_bg);
        AnimUtil.enterFromBottom(ll_content);
    }

    /**
     * 初始化月
     */
    private void initMonth() {
        MyWheelViewAdapter adapterHour = new MyWheelViewAdapter(context);
        adapterHour.setTextSize(20);  //设置字体大小
        wv_month.setViewAdapter(adapterHour);
        adapterHour.setData(hourList);
        wv_month.setCurrentItem(0);
    }

    @Override
    public void onChanged(WheelView wheel, int oldValue, int newValue) {
        if (wheel == wv_month) {

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_ok:
                if(callback!=null)
                {
                    callback.onResult(hourList.get(wv_month.getCurrentItem()));
                }
                dismissWithAnim();
                break;
            case R.id.tv_cancel:
                dismissWithAnim();
                break;
            case R.id.view_bg:
                dismissWithAnim();
                break;
            case R.id.ll_content:
                //不处理，只消耗点击事件
                break;
        }
    }

    public void dismissWithAnim() {
        if (view_bg.getAnimation() != null) return;
        AnimUtil.outToBottom(ll_content, null);
        AnimUtil.fadeOut(view_bg, new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                dismiss();
            }
        });
    }
}
