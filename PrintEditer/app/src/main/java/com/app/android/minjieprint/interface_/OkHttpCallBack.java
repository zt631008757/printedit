package com.app.android.minjieprint.interface_;

import com.app.android.minjieprint.responce.BaseResponce;

/**
 * Created by zt on 2018/6/13.
 */

public interface OkHttpCallBack {
    void onSuccess(BaseResponce baseResponce);
    void onFailure(BaseResponce baseResponce);
}
