package com.app.android.minjieprint.manager;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.bean.ActionInfo;
import java.util.ArrayList;
import java.util.List;

public class ActionManager {

    public static List<ActionInfo> getAllAction() {
        List<ActionInfo> list = new ArrayList<>();
        list.add(new ActionInfo(R.string.edit_action_delete, R.drawable.ico_action_delete));
        list.add(new ActionInfo(R.string.edit_action_rotate, R.drawable.ico_action_xuanzhuan));
        list.add(new ActionInfo(R.string.edit_action_move, R.drawable.ico_action_yidong));
        list.add(new ActionInfo(R.string.edit_action_print, R.drawable.ico_action_lianjie));
        list.add(new ActionInfo(R.string.edit_action_property, R.drawable.ico_action_shuxing));
        list.add(new ActionInfo(R.string.edit_action_zoomout, R.drawable.ico_action_suoxiao));
        list.add(new ActionInfo(R.string.edit_action_zoomin, R.drawable.ico_action_fangda));
        list.add(new ActionInfo(R.string.edit_action_copy, R.drawable.ico_copy));
        return list;
    }

}
