package com.app.android.minjieprint.ui.view;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.bean.ElementsInfo;
import com.app.android.minjieprint.util.DateUtil;

import java.util.Calendar;
import java.util.Date;

public class ElementView_Time extends ElementView_Text {

    public ElementView_Time(Context context, ElementsInfo info) {
        super(context, info);
        init();
    }

    Handler handler = new Handler();
    Calendar calendar;

    private void init() {
        handler.post(runnable);
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            refreshUI();
            handler.postDelayed(runnable, 1000);
        }
    };

    //刷新控件
    @Override
    public void refreshUI() {
        calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, elementsInfo.date_migration);
        String time = DateUtil.getDateTimeByFormat(calendar.getTime(), elementsInfo.format_date, elementsInfo.format_time);
        elementsInfo.text = time;
        super.refreshUI();
    }

    @Override
    public void onDoubleClick() {
        //重写，不做操作
    }
}
