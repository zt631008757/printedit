package com.app.android.minjieprint.ui.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.bean.ElementsInfo;
import com.app.android.minjieprint.dialog.TextInput_Dialog;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.tool.CommToast;
import com.app.android.minjieprint.tool.Log;
import com.app.android.minjieprint.util.ZXingUtil;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;

public class ElementView_TiaoMa extends ElementView_Base {

    public ElementView_TiaoMa(Context context, ElementsInfo info) {
        super(context, info);
        init();
    }

    @Override
    public void refreshUI() {
        super.refreshUI();
        try {
            int color = Color.parseColor(elementsInfo.color);
            Bitmap bitmap = ZXingUtil.creatTiaoMa(elementsInfo.text, (int) elementsInfo.width, (int) elementsInfo.width / 2, color, BarcodeFormat.valueOf(elementsInfo.codetype));
            iv_img.setImageBitmap(bitmap);
            tv_text.setText(elementsInfo.text);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            tv_text.setTextColor(Color.parseColor(elementsInfo.color));
        } catch (Exception e) {
            tv_text.setTextColor(Color.parseColor("#333333"));
        }

        if (elementsInfo.hastext) {
            tv_text.setVisibility(VISIBLE);
        } else {
            tv_text.setVisibility(GONE);
        }
        setRotation(elementsInfo.orientation);
    }

    private static long lastClickTime = 0;

    TextView tv_text;
    ImageView iv_img;

    private void init() {
        View.inflate(mContext, R.layout.view_ele_tiaoma, this);

        tv_text = findViewById(R.id.tv_text);
        iv_img = findViewById(R.id.iv_img);

        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Log.i("tv_text点击");
                        long currentTimeMillis = System.currentTimeMillis();
                        if (currentTimeMillis - lastClickTime < DOUBLE_TIME) {
                            onDoubleClick();
                        }
                        lastClickTime = currentTimeMillis;
                        break;
                }
                return false;
            }
        });
        refreshUI();
    }

    //双击
    private void onDoubleClick() {
        final TextInput_Dialog dialog = new TextInput_Dialog(mContext);
        dialog.setData(tv_text.getText().toString());
        dialog.setIntputCallBack(new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                String text = (String) obj;
                try {
                    int color = Color.parseColor(elementsInfo.color);
                    ZXingUtil.creatTiaoMa(elementsInfo.text, (int) elementsInfo.width, (int) elementsInfo.width / 2, color, BarcodeFormat.valueOf(elementsInfo.codetype));

                    elementsInfo.text = text;
                    refreshUI();
                    dialog.dismissWithAnim();
                } catch (Exception e) {
                    e.printStackTrace();
                    CommToast.showToast(mContext, e.getMessage());
                }
            }
        });
        dialog.show();
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) iv_img.getLayoutParams();
        params.width = getWidth();
        params.height = params.width / 2;
        iv_img.setLayoutParams(params);

    }
}
