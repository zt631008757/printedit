package com.app.android.minjieprint.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.adapter.A_ModelAdapter;
import com.app.android.minjieprint.adapter.FontListAdapter;
import com.app.android.minjieprint.bean.TypeFaceInfo;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.manager.TypeFaceManager;
import com.app.android.minjieprint.tool.Log;
import com.app.android.minjieprint.ui.view.MultiStateView;

import java.util.List;

/**
 * Created by Administrator on 2018/6/20.
 */

public class FontManagerActivity extends BaseActivity {

    public static CommCallBack callBack;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fontmanage);
        initView();
        getData();
    }

    MultiStateView multiplestatusView;
    RecyclerView recyclerview;
    FontListAdapter adapter;

    //初始化控件
    private void initView() {
        setTitle(getString(R.string.title_fout_manage));    //设置标题
        setLeftImgClickListener();  //设置左键返回

        multiplestatusView = findViewById(R.id.multiplestatusView);
        multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);    //页面初始化，默认为加载中状态
        multiplestatusView.setOnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
                getData();
            }
        });

        recyclerview = findViewById(R.id.recyclerview);
        recyclerview.setLayoutManager(new LinearLayoutManager(mContext));
        adapter = new FontListAdapter(mContext, new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                Log.i("FontManagerActivity.onResult");
                if (callBack != null) {
                    callBack.onResult(obj);
                    finish();
                }
            }
        });
        recyclerview.setAdapter(adapter);
    }

    //网路请求 获取数据
    private void getData() {
        final List<TypeFaceInfo> list = TypeFaceManager.getTypeFaceInfo();
        adapter.setData(list);
        multiplestatusView.setViewState(MultiStateView.ViewState.CONTENT);
    }

    //点击事件
    @Override
    public void onClick(View view) {
        super.onClick(view);
        Intent intent;
        switch (view.getId()) {
//            case R.id.tv_tohaibao:
//                intent = new Intent(mContext, YaoQing_HaiBaoActivity.class);
//                startActivity(intent);
//                break;

        }
    }
}
