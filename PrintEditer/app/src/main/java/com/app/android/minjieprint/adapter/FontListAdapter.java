package com.app.android.minjieprint.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.bean.TypeFaceInfo;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.manager.TypeFaceManager;
import com.app.android.minjieprint.ui.view.MultiStateView;

import java.util.List;

import static com.lzy.okgo.utils.HttpUtils.runOnUiThread;

/**
 * Created by Administrator on 2018/8/22.
 */

public class FontListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    List<TypeFaceInfo> list = null;
    CommCallBack callBack;

    public FontListAdapter(Context mContext, CommCallBack callBack) {
        this.mContext = mContext;
        this.callBack = callBack;
    }

    public void setData(List<TypeFaceInfo> list) {
        this.list = list;
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        ViewGroup viewGroup = (ViewGroup) mInflater.inflate(R.layout.item_fontlist, null, false);
        ContentViewHolder holder = new ContentViewHolder(viewGroup);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ContentViewHolder viewHolder = (ContentViewHolder) holder;
        final TypeFaceInfo info = list.get(position);

        viewHolder.tv_name.setText(info.name);

        new Thread(new Runnable() {
            @Override
            public void run() {
                final Typeface iconfont = Typeface.createFromAsset(mContext.getAssets(), info.filePath);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        viewHolder.tv_text.setTypeface(iconfont);
                    }
                });
            }
        }).start();

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(callBack!=null)
                {
                    callBack.onResult(info);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class ContentViewHolder extends RecyclerView.ViewHolder {
        TextView tv_text, tv_name;

        public ContentViewHolder(View itemView) {
            super(itemView);
            tv_text = itemView.findViewById(R.id.tv_text);
            tv_name = itemView.findViewById(R.id.tv_name);
        }
    }
}
