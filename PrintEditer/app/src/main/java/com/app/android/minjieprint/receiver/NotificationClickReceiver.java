package com.app.android.minjieprint.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


import com.app.android.minjieprint.event.Event_PushClick;

import org.greenrobot.eventbus.EventBus;

public class NotificationClickReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        //收到通知栏点击事件
        String androidUrl = intent.getStringExtra("androidUrl");
        Event_PushClick eventPushClick = new Event_PushClick();
        eventPushClick.androidUrl = androidUrl;
        EventBus.getDefault().post(eventPushClick);
    }
}
