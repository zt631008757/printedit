package com.app.android.minjieprint.bean;

import java.util.List;

public class VoiceResultBean {

    public boolean ls;
    public List<Ws> ws;

    public class Ws {
        public List<Cw> cw;
    }

    public class Cw {
        public String w;
    }
}
