package com.app.android.minjieprint.manager;

import android.graphics.Typeface;

import com.app.android.minjieprint.MyApplication;
import com.app.android.minjieprint.bean.TypeFaceInfo;

import java.util.ArrayList;
import java.util.List;

public class TypeFaceManager {

    public static List<TypeFaceInfo> getTypeFaceInfo() {
        List<TypeFaceInfo> list = new ArrayList<>();
        list.add(new TypeFaceInfo("AbrilFatface", "AbrilFatface-Regular.ttf"));
        list.add(new TypeFaceInfo("Anton", "Anton-Regular.ttf"));
        list.add(new TypeFaceInfo("Bungee", "Bungee-Regular.ttf"));
        list.add(new TypeFaceInfo("Cookie", "Cookie-Regular.ttf"));
        list.add(new TypeFaceInfo("Orbitron", "Orbitron-Regular.ttf"));
        list.add(new TypeFaceInfo("微软雅黑", "msyhbd.ttf"));
        list.add(new TypeFaceInfo("黑体", "simhei.ttf"));
        list.add(new TypeFaceInfo("楷体", "simkai.ttf"));
        list.add(new TypeFaceInfo("LiSu", "SIMLI.TTF"));
        list.add(new TypeFaceInfo("华文彩云", "STCAIYUN.TTF"));
        return list;
    }


    public static TypeFaceInfo getTypeFaceInfo(String name) {
        TypeFaceInfo info = null;
        List<TypeFaceInfo> list = getTypeFaceInfo();
        for (int i = 0; i < list.size(); i++) {
            if (name.equals(list.get(i).name)) {
                info = list.get(i);
            }
        }
        return info;
    }
}
