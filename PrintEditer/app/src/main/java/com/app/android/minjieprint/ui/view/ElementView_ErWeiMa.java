package com.app.android.minjieprint.ui.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.bean.ElementsInfo;
import com.app.android.minjieprint.dialog.TextInput_Dialog;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.tool.Log;
import com.app.android.minjieprint.util.ZXingUtil;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

public class ElementView_ErWeiMa extends ElementView_Base {
    public ElementView_ErWeiMa(Context context, ElementsInfo info) {
        super(context, info);
        init();
    }

    @Override
    public void refreshUI() {
        super.refreshUI();
        String errorLv = "";
        if (elementsInfo.correction == 0) {
            errorLv = ErrorCorrectionLevel.L.toString();
        } else if (elementsInfo.correction == 1) {
            errorLv = ErrorCorrectionLevel.M.toString();
        } else if (elementsInfo.correction == 2) {
            errorLv = ErrorCorrectionLevel.H.toString();
        } else if (elementsInfo.correction == 3) {
            errorLv = ErrorCorrectionLevel.Q.toString();
        }
        int color = Color.parseColor(elementsInfo.color);
        int width = 0;
        if (elementsInfo.width > elementsInfo.height) {
            width = elementsInfo.height;
        } else {
            width = elementsInfo.width;
        }
        try {
            Bitmap bitmap = ZXingUtil.generateBitmap(elementsInfo.text, width, width, errorLv, color);
            iv_img.setImageBitmap(bitmap);
        }
        catch (Exception e)
        {

        }

        setRotation(elementsInfo.orientation);
    }

    ImageView iv_img;

    private void init() {
        View.inflate(mContext, R.layout.view_ele_erweima, this);
        iv_img = findViewById(R.id.iv_img);


        iv_img.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        long currentTimeMillis = System.currentTimeMillis();
                        if (currentTimeMillis - lastClickTime < DOUBLE_TIME) {
                            onDoubleClick();
                        }
                        lastClickTime = currentTimeMillis;
                        break;
                }
                return false;
            }
        });


        refreshUI();
    }

    private static long lastClickTime = 0;

    //双击
    private void onDoubleClick() {
        final TextInput_Dialog dialog = new TextInput_Dialog(mContext);
        dialog.setData(elementsInfo.text);
        dialog.setIntputCallBack(new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                String text = (String) obj;
                elementsInfo.text = text;
                refreshUI();
                dialog.dismissWithAnim();
            }
        });
        dialog.show();
    }

}
