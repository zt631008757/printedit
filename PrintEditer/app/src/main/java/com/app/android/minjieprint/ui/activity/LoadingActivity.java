package com.app.android.minjieprint.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.WindowManager;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.constant.SPConstants;
import com.app.android.minjieprint.dialog.XieYi_Dialog;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.tool.SPUtil;


/**
 * Created by Administrator on 2018/7/21.
 */

public class LoadingActivity extends Activity {

    Handler handler = new Handler();
    Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
        mContext = this;


        boolean hasOk = SPUtil.getBoolValue(mContext, SPConstants.HasAccept, false);
        if (hasOk) {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    jump();
                }
            }, 1500);
        } else {
            XieYi_Dialog dialog = new XieYi_Dialog(mContext);
            dialog.show();
            dialog.setIntputCallBack(new CommCallBack() {
                @Override
                public void onResult(Object obj) {
                    SPUtil.putValue(mContext, SPConstants.HasAccept, true);
                    jump();
                }
            });
        }
    }

    private void jump() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        Intent intent = new Intent(mContext, HomeActivity.class);
        mContext.startActivity(intent);
        overridePendingTransition(R.anim.main_activity_in, R.anim.splash_out);
        finish();
    }
}
