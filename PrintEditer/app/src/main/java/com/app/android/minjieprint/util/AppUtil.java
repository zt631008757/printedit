package com.app.android.minjieprint.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.FileProvider;


import java.io.File;

/**
 * Created by Administrator on 2019/1/6.
 */

public class AppUtil {
    public static void install(Context mContext, String filePath) {
        File apkFile = new File(filePath);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//必须为addFlags 否则高版本的手机安装完成后没有打开、完成按钮
        intent.addCategory(Intent.CATEGORY_DEFAULT);//必须要  高版本的手机需要添加 否则安装完成后没有打开、完成按钮
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);//必须改为addFlags 否则高版本的手机安装完成后没有打开、完成按钮
            Uri contentUri = FileProvider.getUriForFile(
                    mContext
                    , mContext.getApplicationContext().getPackageName() +
                            ".fileProvider"
                    , apkFile);
            intent.setDataAndType(contentUri, "application/vnd.android.package-archive");
        } else {
            intent.setDataAndType(Uri.fromFile(apkFile), "application/vnd.android.package-archive");
        }
        mContext.startActivity(intent);
    }

}
