package com.app.android.minjieprint.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.bean.LocalFileInfo;
import com.app.android.minjieprint.dialog.Comm_Dialog;
import com.app.android.minjieprint.event.Event_CurrentFile;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.manager.Config;
import com.app.android.minjieprint.tool.Log;
import com.app.android.minjieprint.ui.activity.BlueToothListActivity;
import com.app.android.minjieprint.ui.activity.BlueToothListActivity_New;
import com.app.android.minjieprint.ui.activity.FileEditActivity;
import com.app.android.minjieprint.util.BitmapUtil;
import com.app.android.minjieprint.util.DateUtil;
import com.app.android.minjieprint.util.ImageDispose;
import com.app.android.minjieprint.util.Util;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by Administrator on 2018/8/22.
 */

public class FileListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    List<LocalFileInfo> list = null;
    CommCallBack callBack;

    public FileListAdapter(Context mContext, CommCallBack callBack) {
        this.mContext = mContext;
        this.callBack = callBack;
    }

    public void setData(List<LocalFileInfo> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        ViewGroup viewGroup = (ViewGroup) mInflater.inflate(R.layout.item_filelist, null, false);
        ContentViewHolder holder = new ContentViewHolder(viewGroup);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ContentViewHolder viewHolder = (ContentViewHolder) holder;
        final LocalFileInfo info = list.get(position);
        Log.i("info:" + info.toString());

        viewHolder.tv_name.setText(info.filename);
        try {
            viewHolder.tv_time.setText(DateUtil.getDateFromTimeLine(Long.parseLong(info.timestamp)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Bitmap bitmap = BitmapUtil.getSmallBitmap(info.snapshot);
            viewHolder.iv_img.setImageBitmap(bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }

        viewHolder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Comm_Dialog.showCommDialog(mContext, "", Util.getString(R.string.file_list_deletedialo_title), Util.getString(R.string.comm_ok), Util.getString(R.string.comm_cancel), new CommCallBack() {
                    @Override
                    public void onResult(Object obj) {
                        info.delete();
                        if (callBack != null) {
                            callBack.onResult("refresh");
                        }
                    }
                }, null);
            }
        });

        viewHolder.iv_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Event_CurrentFile eventCurrentFile = new Event_CurrentFile();
                eventCurrentFile.fileName = info.filename;
                EventBus.getDefault().post(eventCurrentFile);
                ((Activity)mContext).finish();
            }
        });
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, FileEditActivity.class);
                intent.putExtra("fileName", info.filename);
                mContext.startActivity(intent);
                ((Activity)mContext).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class ContentViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name, tv_time;
        ImageView iv_delete, iv_print;
        ImageView iv_img;

        public ContentViewHolder(View itemView) {
            super(itemView);
            iv_delete = itemView.findViewById(R.id.iv_delete);
            tv_name = itemView.findViewById(R.id.tv_name);
            iv_img = itemView.findViewById(R.id.iv_img);
            tv_time = itemView.findViewById(R.id.tv_time);
            iv_print = itemView.findViewById(R.id.iv_print);
        }
    }
}
