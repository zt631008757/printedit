package com.app.android.minjieprint.bean;

public class ActionInfo {

    public int name;
    public int resource;

    public ActionInfo(int name, int resource) {
        this.name = name;
        this.resource = resource;
    }

}
