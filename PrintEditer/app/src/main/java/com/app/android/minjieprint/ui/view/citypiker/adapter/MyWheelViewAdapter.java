package com.app.android.minjieprint.ui.view.citypiker.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.android.minjieprint.bean.CommObjectInfo;

import java.util.List;

/**
 * Created by Administrator on 2018/9/29.
 */

public class MyWheelViewAdapter extends AbstractWheelTextAdapter {

    List<CommObjectInfo> list;

    public MyWheelViewAdapter(Context context) {
        super(context);
    }

    public MyWheelViewAdapter(Context context, int itemResource) {
        super(context, itemResource);
    }

    public MyWheelViewAdapter(Context context, int itemResource, int itemTextResource) {
        super(context, itemResource, itemTextResource);
    }

    public void setData(List<CommObjectInfo> list) {
        this.list = list;
        notifyDataChangedEvent();
    }


    @Override
    public int getItemsCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    protected CharSequence getItemText(int index) {
        return list.get(index).name;
    }

    @Override
    public View getItem(int index, View convertView, ViewGroup parent) {
        if (index >= 0 && index < getItemsCount()) {
            if (convertView == null) {
                convertView = getView(itemResourceId, parent);
            }
            TextView textView = getTextView(convertView, itemTextResourceId);
            if (textView != null) {
                CharSequence text = getItemText(index);
                if (text == null) {
                    text = "";
                }
                textView.setText(text);
                textView.setPadding(0,3,0,3);
                if (itemResourceId == TEXT_VIEW_ITEM_RESOURCE) {
                    configureTextView(textView);
                }
            }
            return convertView;
        }
        return null;
    }
}
