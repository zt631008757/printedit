package com.app.android.minjieprint.ui.activity;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.android.minjieprint.MyApplication;
import com.app.android.minjieprint.R;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.tool.Log;
import com.app.android.minjieprint.ui.view.MultiStateView;
import com.app.android.minjieprint.util.AnimUtil;
import com.app.android.minjieprint.util.BitmapUtil;
import com.app.android.minjieprint.util.ImageDispose;
import com.app.android.minjieprint.util.StatusBarUtil_Dialog;
import com.app.android.minjieprint.util.Util;
import com.dd.CircularProgressButton;
import com.feasycom.bean.BluetoothDeviceWrapper;
import com.feasycom.controler.FscSppApiImp;
import com.feasycom.controler.FscSppCallbacksImp;
import com.lzy.okgo.utils.HttpUtils;

/**
 * Created by Administrator on 2018/6/20.
 */

public class PrintActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        deviceWrapper = (BluetoothDeviceWrapper) getIntent().getSerializableExtra("deviceWrapper");
        String snapshot = getIntent().getStringExtra("snapshot");
        bitmap = BitmapUtil.getSmallBitmap(snapshot);

        setContentView(R.layout.activity_print);
        initView();
        connectionBlueTooth();
    }

    CommCallBack callBack;
    Bitmap bitmap;
    BluetoothDeviceWrapper deviceWrapper;
    FscSppApiImp fscSppApi;
    Handler handler = new Handler();

    CircularProgressButton circularprogressbutton;

    //设置输入回调
    public void setIntputCallBack(CommCallBack callBack) {
        this.callBack = callBack;
    }


    LinearLayout ll_content;
    View view_bg;
    TextView tv_print;
    ImageView iv_img;
    ProgressBar progressbar, progressbar1;

    private void initView() {
        setTitle("打印");    //设置标题
        setLeftImgClickListener();  //设置左键返回
        ll_content = findViewById(R.id.ll_content);
        view_bg = findViewById(R.id.view_bg);
        view_bg.setOnClickListener(this);
        ll_content.setOnClickListener(this);
        AnimUtil.fadeIn(view_bg);
        AnimUtil.fadeIn(ll_content);

        iv_img = findViewById(R.id.iv_img);
        tv_print = findViewById(R.id.tv_print);
        progressbar = findViewById(R.id.progressbar);
        progressbar1 = findViewById(R.id.progressbar1);
        circularprogressbutton = findViewById(R.id.circularprogressbutton);


        tv_print.setOnClickListener(this);

        bitmap = BitmapUtil.rotateBitmap(bitmap, 90);
//        byte[] data = ImageDispose.Bitmap2Bytes(bitmap);
//        bitmap = ImageDispose.getPicFromBytes(data, null);

        bitmap = BitmapUtil.compressQuality(bitmap, 30);
//        bitmap = bitmap.copy(Bitmap.Config.RGB_565, true);
        iv_img.setImageBitmap(bitmap);

        fscSppApi = FscSppApiImp.getInstance(MyApplication.context);
        fscSppApi.setCallbacks(new FscSppCallbacksImp() {
            @Override
            public void sppConnected(BluetoothDevice device) {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        HttpUtils.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tv_print.setText(Util.getString(R.string.printdialog_conneted));
                                progressbar1.setVisibility(View.GONE);
                                sendData();
                            }
                        });
                    }
                }, 500);
            }

            @Override
            public void sppDisconnected(BluetoothDevice device) {

            }

            @Override
            public void packetReceived(byte[] dataByte, String dataString, String dataHexString) {

            }

            @Override
            public void sendPacketProgress(BluetoothDevice device, final int percentage, byte[] tempByte) {
                try {
                    HttpUtils.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (percentage <= 100) {
                                progressbar.setProgress(percentage);
                                tv_print.setText(Util.getString(R.string.printdialog_sending) + percentage + "%");
                            }
                            if (percentage == 100) {
                                //发送成功
                                tv_print.setText(Util.getString(R.string.printdialog_send_success));
                            }
                        }
                    });
                    if (percentage != 101) {
                        Log.d("sendPacketProgress:" + percentage);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        connectionBlueTooth();
    }

    /**
     * 连接蓝牙
     */
    public void connectionBlueTooth() {
        if (deviceWrapper != null) {
            fscSppApi.connect(deviceWrapper.getAddress());
        }
    }

    //断开连接
    public void disConnectionBlueTooth() {
        try {
            if (fscSppApi.isConnected()) {
                fscSppApi.disconnect();
            }
        } catch (Exception e) {

        }
    }

    /**
     * 写数据
     */
    public void sendData() {
        try {
            byte[] data = Util.buildPacket(ImageDispose.Bitmap2Bytes(bitmap), bitmap.getHeight());


            Log.i("bitmap.getWidth:" + bitmap.getWidth() + "  " + bitmap.getHeight());
            Log.i("data.length:" + data.length);
            fscSppApi.send(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.view_bg:
                break;
            case R.id.ll_content:
                //不处理，只消耗点击事件
                break;
            case R.id.tv_print:
//                connectionBlueTooth();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disConnectionBlueTooth();
    }
}
