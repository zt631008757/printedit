package com.app.android.minjieprint.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.adapter.FontListAdapter;
import com.app.android.minjieprint.adapter.LangugeListAdapter;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.manager.LangugeManager;
import com.app.android.minjieprint.ui.view.MultiStateView;

/**
 * Created by Administrator on 2018/6/20.
 */

public class LangugeListActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_languge);
        initView();
        getData();
    }

    MultiStateView multiplestatusView;
    RecyclerView recyclerview;
    LangugeListAdapter adapter;

    //初始化控件
    private void initView() {
        setTitle(getResources().getString(R.string.title_languge));    //设置标题
        setLeftImgClickListener();  //设置左键返回

        multiplestatusView = findViewById(R.id.multiplestatusView);
//        multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);    //页面初始化，默认为加载中状态
        multiplestatusView.setOnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
                getData();
            }
        });

        recyclerview = findViewById(R.id.recyclerview);
        recyclerview.setLayoutManager(new LinearLayoutManager(mContext));
        adapter = new LangugeListAdapter(mContext, callBack);
        recyclerview.setAdapter(adapter);
    }

    CommCallBack callBack = new CommCallBack() {
        @Override
        public void onResult(Object obj) {
            finish();
        }
    };

    //网路请求 获取数据
    private void getData() {
        adapter.setData(LangugeManager.getAllLanguge());
    }

    //点击事件
    @Override
    public void onClick(View view) {
        super.onClick(view);
        Intent intent;
        switch (view.getId()) {
//            case R.id.tv_tohaibao:
//                intent = new Intent(mContext, YaoQing_HaiBaoActivity.class);
//                startActivity(intent);
//                break;

        }
    }
}
