package com.app.android.minjieprint.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.adapter.ImgResultListAdapter;
import com.app.android.minjieprint.bean.ImgResultBean;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.ui.view.MultiStateView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/6/20.
 */

public class ImgResultListActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imgResultBean = (ImgResultBean) getIntent().getSerializableExtra("imgResultBean");
        setContentView(R.layout.activity_imgresultlist);
        initView();
    }

    ImgResultBean imgResultBean;
    RecyclerView recyclerview;
    ImgResultListAdapter adapter;

    //初始化控件
    private void initView() {
        setTitle("识别结果");    //设置标题
        setLeftImgClickListener();  //设置左键返回

        recyclerview = findViewById(R.id.recyclerview);
        recyclerview.setLayoutManager(new LinearLayoutManager(mContext));
        adapter = new ImgResultListAdapter(mContext, callBack);
        recyclerview.setAdapter(adapter);

        if (imgResultBean != null) {
            List<String> list = new ArrayList<>();
            for (int i = 0; i < imgResultBean.words_result.size(); i++) {
                list.add(imgResultBean.words_result.get(i).words);
            }
            adapter.setData(list);
        }
    }

    CommCallBack callBack = new CommCallBack() {
        @Override
        public void onResult(Object obj) {
            String result =  (String) obj;
            Intent intent = new Intent();
            intent.putExtra("result", result);
            setResult(100,intent);
            finish();
        }
    };

    //点击事件
    @Override
    public void onClick(View view) {
        super.onClick(view);
        Intent intent;
        switch (view.getId()) {
//            case R.id.tv_tohaibao:
//                intent = new Intent(mContext, YaoQing_HaiBaoActivity.class);
//                startActivity(intent);
//                break;

        }
    }
}
