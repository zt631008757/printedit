package com.app.android.minjieprint.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.util.AnimUtil;
import com.app.android.minjieprint.util.StatusBarUtil_Dialog;
import com.app.android.minjieprint.util.Util;

import java.util.List;

/**
 * Created by Administrator on 2018/8/29.
 */

public class CommList_Dialog extends Dialog implements View.OnClickListener {
    public CommList_Dialog(@NonNull Context context) {
        super(context, R.style.myDialog);
    }

    public CommList_Dialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
    }

    protected CommList_Dialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    LinearLayout ll_content, linearLayout, ll_title;
    View view_bg;
    TextView tv_cancel, tv_text;
    ScrollView scrollview;

    CommCallBack callBack;
    CommCallBack cancelCallBack;
    List<String> strList;
    boolean isShowCancel = false;  //是否显示取消按钮
    boolean showTitle = false;
    String text;

    //设置回调
    public void setCallBack(CommCallBack callBack) {
        this.callBack = callBack;
    }

    //设置取消回调
    public void setCancelCallBack(CommCallBack callBack) {
        this.cancelCallBack = callBack;
    }

    public void setData(List<String> strList, String text, boolean showTitle, boolean isShowCancel) {
        this.strList = strList;
        this.text = text;
        this.showTitle = showTitle;
        this.isShowCancel = isShowCancel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_list_comm);
        initView();
        StatusBarUtil_Dialog.setImmersiveStatusBar(this, false);
    }

    private void initView() {
        ll_content = findViewById(R.id.ll_content);
        view_bg = findViewById(R.id.view_bg);
        linearLayout = findViewById(R.id.list_content);
        scrollview = findViewById(R.id.scrollview);
        tv_cancel = findViewById(R.id.tv_cancel);
        tv_text = findViewById(R.id.tv_text);
        ll_title = findViewById(R.id.ll_title);

        AnimUtil.fadeIn(view_bg);
        AnimUtil.enterFromBottom(ll_content);

        view_bg.setOnClickListener(this);
        ll_content.setOnClickListener(this);
        tv_cancel.setOnClickListener(this);
        StatusBarUtil_Dialog.setImmersiveStatusBar(this, true);

        tv_text.setText(text);

        if (strList != null) {
            for (int i = 0; i < strList.size(); i++) {
                if (!TextUtils.isEmpty(strList.get(i))) {
                    TextView textView = new TextView(getContext());
                    textView.setText(strList.get(i));
                    textView.setTextSize(15);
                    textView.setGravity(Gravity.CENTER);
                    textView.setTextColor(Color.parseColor("#222222"));
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, Util.dip2px(getContext(), 50));
                    params.gravity = Gravity.CENTER;
                    textView.setLayoutParams(params);

                    final int finalI = i;
                    textView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //将position回传
                            if (callBack != null)
                                callBack.onResult(finalI);
                            dismissWithAnim();
                        }
                    });
                    //加分割线
                    if (i != 0) {
                        View view = new View(getContext());
                        view.setBackgroundColor(Color.parseColor("#eeeeee"));
                        LinearLayout.LayoutParams paramsView = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, Util.dip2px(getContext(), 0.5f));
                        view.setLayoutParams(paramsView);
                        linearLayout.addView(view);
                    }
                    linearLayout.addView(textView);
                }
            }
        }

        if (strList.size() > 8) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Util.dip2px(getContext(), 400));
            scrollview.setLayoutParams(params);
        }

        if (isShowCancel) {
            tv_cancel.setVisibility(View.VISIBLE);
        } else {
            tv_cancel.setVisibility(View.GONE);
        }
        if (showTitle) {
            ll_title.setVisibility(View.VISIBLE);
        } else {
            ll_title.setVisibility(View.GONE);
        }
    }

    CommCallBack onSelect = new CommCallBack() {
        @Override
        public void onResult(Object obj) {
            callBack.onResult(obj);
            dismissWithAnim();
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.view_bg:
                dismissWithAnim();
                if (cancelCallBack != null) {
                    cancelCallBack.onResult(null);
                }
                break;
            case R.id.ll_content:
                //不处理，只消耗点击事件
                break;
            case R.id.tv_cancel:
                dismissWithAnim();
                if (cancelCallBack != null) {
                    cancelCallBack.onResult(null);
                }
                break;
        }
    }

    public void dismissWithAnim() {
        if (view_bg.getAnimation() != null) return;
        AnimUtil.outToBottom(ll_content, null);
        AnimUtil.fadeOut(view_bg, new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        dismissWithAnim();
    }
}
