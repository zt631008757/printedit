package com.app.android.minjieprint.ui.view;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.android.minjieprint.interface_.CommCallBack;

import java.util.ArrayList;
import java.util.List;
import com.app.android.minjieprint.R;
/**
 * Created by Administrator on 2018/8/20.
 */

public class MyBottomTabView extends LinearLayout {

    Context mContext;
    View rootView;

    public MyBottomTabView(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public MyBottomTabView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    public MyBottomTabView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    int[] ico_normal = {R.drawable.ico_home_main_normal,  R.drawable.ico_home_mine_normal};
    int[] ico_select = {R.drawable.ico_home_main_select, R.drawable.ico_home_mine_select};
    String[] TAB_NAME = {"首页", "我的"};
    int TEXT_COLOR_PRESSED = Color.parseColor("#fe2641");
    int TEXT_COLOR_NORMAL = Color.parseColor("#999999");

    List<RelativeLayout> list = new ArrayList<>();

    CommCallBack callBack;
    int currentSelect = 0;

    private void init() {
        setOrientation(HORIZONTAL);
        for (int i = 0; i < TAB_NAME.length; i++) {
            RelativeLayout relativeLayout = (RelativeLayout) View.inflate(mContext, R.layout.view_mybottomtabview_item, null);
            TextView tv_name = relativeLayout.findViewById(R.id.tv_name);
            ImageView iv_img = relativeLayout.findViewById(R.id.iv_img);
            tv_name.setText(TAB_NAME[i]);
//            if(currentSelect == i)
//            {
//                iv_img.setImageResource(ico_select[i]);
//            }
//            else {
//                iv_img.setImageResource(ico_normal[i]);
//            }
            LayoutParams params = new LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);
            params.weight = 1;
            relativeLayout.setLayoutParams(params);
            final int finalI = i;
            relativeLayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    currentSelect = finalI;
                    setSelect();
                    if(callBack!=null)
                    {
                        callBack.onResult(finalI);
                    }
                }
            });
            list.add(relativeLayout);
            addView(relativeLayout);
        }
        setSelect();
    }

    public void setOnTabSelectCallBack(CommCallBack callBack) {
        this.callBack = callBack;
    }

    private void setSelect()
    {
        for (int i = 0; i < list.size(); i++) {
            RelativeLayout relativeLayout = list.get(i);
            ImageView iv_img = relativeLayout.findViewById(R.id.iv_img);
            TextView tv_name = relativeLayout.findViewById(R.id.tv_name);
            if(currentSelect == i)
            {
                iv_img.setImageResource(ico_select[i]);
                tv_name.setTextColor(TEXT_COLOR_PRESSED);
            }
            else {
                iv_img.setImageResource(ico_normal[i]);
                tv_name.setTextColor(TEXT_COLOR_NORMAL);
            }
        }
    }

    public void setImHasNew(int num)
    {
        RelativeLayout relativeLayout = list.get(2);
        View view_new = relativeLayout.findViewById(R.id.view_new);
        if(num>0)
        {
            view_new.setVisibility(VISIBLE);
        }
        else
        {
            view_new.setVisibility(GONE);
        }
    }
}
