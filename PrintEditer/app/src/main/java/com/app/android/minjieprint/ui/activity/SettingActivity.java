package com.app.android.minjieprint.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.constant.SPConstants;
import com.app.android.minjieprint.dialog.Comm_Dialog;
import com.app.android.minjieprint.event.Event_Logout;
import com.app.android.minjieprint.interface_.CommCallBack;
import com.app.android.minjieprint.manager.Config;
import com.app.android.minjieprint.manager.UserManager;
import com.app.android.minjieprint.tool.CommToast;
import com.app.android.minjieprint.tool.SPUtil;
import com.app.android.minjieprint.ui.view.Comm_TitleView;
import com.app.android.minjieprint.ui.view.MultiStateView;
import com.app.android.minjieprint.util.Util;
import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by Administrator on 2018/6/20.
 */

public class SettingActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        initView();
        getData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    MultiStateView multiplestatusView;
    Comm_TitleView tv_print, tv_islogin, tv_version, tv_logo, tv_fontmanager, tv_languge;
    TextView tv_logout, tv_yingsi, tv_yingsi1;

    //初始化控件
    private void initView() {
        setTitle(getResources().getString(R.string.title_setting));    //设置标题
        setLeftImgClickListener();  //设置左键返回

        multiplestatusView = findViewById(R.id.multiplestatusView);
//        multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);    //页面初始化，默认为加载中状态
        multiplestatusView.setOnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
                getData();
            }
        });

        tv_print = findViewById(R.id.tv_print);
        tv_islogin = findViewById(R.id.tv_islogin);
        tv_logout = findViewById(R.id.tv_logout);
        tv_version = findViewById(R.id.tv_version);
        tv_logo = findViewById(R.id.tv_logo);
        tv_fontmanager = findViewById(R.id.tv_fontmanager);
        tv_languge = findViewById(R.id.tv_languge);
        tv_yingsi = findViewById(R.id.tv_yingsi);
        tv_yingsi1 = findViewById(R.id.tv_yingsi1);

        tv_print.setOnClickListener(this);
        tv_islogin.setOnClickListener(this);
        tv_logout.setOnClickListener(this);
        tv_logo.setOnClickListener(this);
        tv_fontmanager.setOnClickListener(this);
        tv_languge.setOnClickListener(this);
        tv_yingsi.setOnClickListener(this);
        tv_yingsi1.setOnClickListener(this);

        try {
            tv_version.tv_content.setText(Config.getConfigVerName() + "_" + Util.getVersionName(mContext));
        } catch (Exception e) {

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (UserManager.isLogin(mContext)) {
            tv_islogin.tv_content.setText(SPUtil.getStringValue(mContext, SPConstants.Phone, ""));
            tv_islogin.iv_arr.setVisibility(View.GONE);
            tv_logout.setVisibility(View.VISIBLE);
        } else {
            tv_islogin.tv_content.setText(getString(R.string.setting_unlogin));
            tv_logout.setVisibility(View.GONE);
            tv_islogin.iv_arr.setVisibility(View.VISIBLE);
        }
    }

    //网路请求 获取数据
    private void getData() {
    }

    //点击事件
    @Override
    public void onClick(View view) {
        super.onClick(view);
        Intent intent;
        switch (view.getId()) {
            case R.id.tv_print:
                intent = new Intent(mContext, BlueToothListActivity_New.class);
                startActivity(intent);
                break;
            case R.id.tv_logout:
                Comm_Dialog.showCommDialog(mContext, "", "确定退出登录吗", "确定", "取消", new CommCallBack() {
                    @Override
                    public void onResult(Object obj) {
                        UserManager.logout(mContext);
                        CommToast.showToast(mContext, "已退出");
                    }
                }, null);
                break;
            case R.id.tv_islogin:
                if (!UserManager.isLogin(mContext)) {
                    intent = new Intent(mContext, LoginActivity.class);
                    startActivity(intent);
                }
                break;
            case R.id.tv_logo:                  //Logo列表
                intent = new Intent(mContext, LogoListActivity.class);
                startActivity(intent);
                break;
            case R.id.tv_fontmanager:          //字体列表
                intent = new Intent(mContext, FontManagerActivity.class);
                startActivity(intent);
                break;
            case R.id.tv_languge:               //多语言
                intent = new Intent(mContext, LangugeListActivity.class);
                startActivity(intent);
                break;
            case R.id.tv_yingsi:            //隐私声明
                intent = new Intent(mContext, YingSiActivity.class);
                intent.putExtra("type", 0);
                startActivity(intent);
                break;
            case R.id.tv_yingsi1:            //用户协议
                intent = new Intent(mContext, YingSiActivity.class);
                intent.putExtra("type", 1);
                startActivity(intent);
                break;
        }
    }

    //退出登录，关闭当前页
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(Event_Logout event) {
        if (UserManager.isLogin(mContext)) {
            tv_islogin.tv_content.setText("已登录");
            tv_logout.setVisibility(View.VISIBLE);
        } else {
            tv_islogin.tv_content.setText("未登录");
            tv_logout.setVisibility(View.GONE);
        }
    }
}
