package com.app.android.minjieprint.ui.view;

import android.content.Context;
import android.os.Handler;

import com.app.android.minjieprint.bean.ElementsInfo;

import java.util.Calendar;
import java.util.Date;

public class ElementView_XuLieHao extends ElementView_Text {

    public ElementView_XuLieHao(Context context, ElementsInfo info) {
        super(context, info);
        init();
    }

    Handler handler = new Handler();
    Calendar calendar;

    private void init() {
        calendar = Calendar.getInstance();
//        handler.post(runnable);
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            elementsInfo.text = new Date().toLocaleString();
//            calendar.add(Calendar.SECOND, 1);
            refreshUI();
            handler.postDelayed(runnable, 1000);
        }
    };

}
