package com.app.android.minjieprint.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.android.minjieprint.R;

public class Comm_SubmitBtnView extends RelativeLayout {
    public Comm_SubmitBtnView(Context context) {
        super(context);
        this.context = context;
        initView(null);
    }

    public Comm_SubmitBtnView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initView(attrs);
    }

    public Comm_SubmitBtnView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initView(attrs);
    }

    Context context;
    public TextView tv_btn;

    public int bg = R.drawable.shape_circle_maincolor;

    private void initView(AttributeSet attrs) {
        View.inflate(context, R.layout.view_comm_submit_btn, this);
        tv_btn = findViewById(R.id.tv_btn);

        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.Comm_SubmitBtnView);

            String text = typedArray.getString(R.styleable.Comm_SubmitBtnView_csb_text);
            tv_btn.setText(text);

            typedArray.recycle();
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (enabled) {
            tv_btn.setBackgroundResource(bg);
        } else {
            tv_btn.setBackgroundResource(R.drawable.shape_circle_maincolor_light);
        }
    }

    public void setText(String text)
    {
        tv_btn.setText(text);
    }
}
