package com.app.android.minjieprint.util;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by zt on 2017/5/16.
 * 图片加载工具类
 */
public class GlideUtil {

    public static RequestOptions getMode(Context context, Object url, int loadingImg) {
        RequestOptions options = new RequestOptions();
        options.skipMemoryCache(false);
        options.diskCacheStrategy(DiskCacheStrategy.RESOURCE);
        options.priority(Priority.NORMAL);
        options.error(loadingImg);
        //设置占位符,默认
        options.placeholder(loadingImg);
        return options;
    }

    //传入尺寸
    public static void displayImgWithSize(Context context, Object url, ImageView imageView, int loadingImg, int width, int height) {
        try {
            RequestOptions options = getMode(context, url, loadingImg);
            options.override(width, height).diskCacheStrategy(DiskCacheStrategy.RESOURCE);

            //通过apply方法将设置的属性添加到glide
            Glide.with(context).load(url).apply(options).transition(withCrossFade()).into(imageView);
        } catch (Exception e) {

        }
    }

    /**
     * @param context
     * @param url        图片路径（网络图片/本地资源文件）
     * @param imageView  ImageView
     * @param loadingImg 加载中图片
     */
    public static void displayImage(Context context, Object url, ImageView imageView, int loadingImg) {
        try {
            RequestOptions options = getMode(context, url, loadingImg);
            //通过apply方法将设置的属性添加到glide
            Glide.with(context).load(url).apply(options).transition(withCrossFade()).into(imageView);
        } catch (Exception e) {

        }
    }

    /**
     * @param context
     * @param imageView  ImageView
     * @param url        图片路径（网络图片/本地资源文件）
     * @param loadingImg 加载中图片
     * @param coner      圆角大小
     */
    public static void displayImage(Context context, Object url, ImageView imageView, int loadingImg, int coner) {
        try {
            RequestOptions options = getMode(context, url, loadingImg);
            options.transforms(new RoundedCorners(Util.dip2px(context, coner)));
            //通过apply方法将设置的属性添加到glide
            Glide.with(context).load(url).apply(options).transition(withCrossFade()).into(imageView);
        } catch (Exception e) {

        }
    }
}
