package com.app.android.minjieprint.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.app.android.minjieprint.bean.ElementsInfo;
import com.app.android.minjieprint.tool.Log;

/**
 * 矩形
 */

public class ElementView_Rect extends ElementView_Base {

    public ElementView_Rect(Context context, ElementsInfo info) {
        super(context, info);
        init();
    }

    @Override
    public void refreshUI() {
        super.refreshUI();
        try {
            paint.setColor(Color.parseColor(elementsInfo.color));
        } catch (Exception e) {
            paint.setColor(Color.parseColor("#333333"));
        }

        paint.setStrokeWidth(elementsInfo.borderwidth);
        paint.setAntiAlias(true);

        if (elementsInfo.fill) {
            paint.setStyle(Paint.Style.FILL);//实心矩形框
        } else {
            paint.setStyle(Paint.Style.STROKE);//空心矩形框
        }

        requestLayout();
    }

    Paint paint;

    private void init() {
        paint = new Paint(); //设置一个笔刷大小是3的黄色的画笔

        refreshUI();
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);

        //形状类型   0 - 矩形; 1 - 圆角矩形; 2 - 椭圆; 3 - 圆形
        int border = 0;
        if (elementsInfo.fill) {
            border = 0;
        } else {
            border = elementsInfo.borderwidth / 2;
        }


        if (elementsInfo.rectStyle == 0) {
            canvas.drawRect(border, border, getWidth() - border, getHeight() - border, paint);
        } else if (elementsInfo.rectStyle == 1) {
            canvas.drawRoundRect(border, border, getWidth() - border, getHeight() - border, elementsInfo.radius, elementsInfo.radius, paint);
        } else if (elementsInfo.rectStyle == 2) {
            //椭圆
            canvas.drawOval(border, border, getWidth() - border, getHeight() - border, paint);
        } else if (elementsInfo.rectStyle == 3) {
            int radius = 0;
            if (getWidth() > getHeight()) {
                radius = getHeight() / 2 - border;
            } else {
                radius = getWidth() / 2 - border;
            }
            canvas.drawCircle(getWidth() / 2, getHeight() / 2, radius, paint);
        }

    }


}
