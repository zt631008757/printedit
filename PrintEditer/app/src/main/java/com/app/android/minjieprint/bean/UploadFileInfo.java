package com.app.android.minjieprint.bean;

import android.graphics.Bitmap;

import java.io.File;

/**
 * Created by Administrator on 2018/9/12.
 */

public class UploadFileInfo {
    public String url;
    public int width;
    public int height;
    public File file;

    public Bitmap bitmap;
}
