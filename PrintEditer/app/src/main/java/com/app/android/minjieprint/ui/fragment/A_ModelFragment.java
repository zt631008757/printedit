package com.app.android.minjieprint.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.android.minjieprint.R;
import com.app.android.minjieprint.ui.view.MultiStateView;

/**
 * Created by Administrator on 2018/8/20.
 */

public class A_ModelFragment extends BaseFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_model, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    Context mContext;
    View rootView;
    MultiStateView multiplestatusView;

    private void initView() {
        multiplestatusView =  rootView.findViewById(R.id.multiplestatusView);
        multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);    //页面初始化，默认为加载中状态
        multiplestatusView.setOnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
                getData();
            }
        });

    }

    //网路请求 获取数据
    private void getData() {

        //获取资产（例子）
//        API_HomeManager.getTotalBalance(mContext, new OkHttpCallBack() {
//            @Override
//            public void onSuccess(BaseResponce baseResponce) {
//                multiplestatusView.setViewState(MultiStateView.ViewState.CONTENT);    //页面状态为：显示内容
//                if (BaseResponce.Status_Success.equals(baseResponce.code)) {
//                    //返回成功
//                    GetBalanceResponce responce = (GetBalanceResponce) baseResponce;
//                    //解析数据，do something...
//                    bindUI();
//                } else {
//                    //返回错误信息
//                    CommToast.showToast(mContext, baseResponce.msg);
//                }
//            }
//
//            @Override
//            public void onFailure(BaseResponce baseResponce) {
//                //网络请求失败
//                multiplestatusView.setViewState(MultiStateView.ViewState.ERROR);    //页面状态为：显示内容
//            }
//        });

    }

    //绑定数据
    private void bindUI() {

    }

    //点击事件
    @Override
    public void onClick(View view) {
        super.onClick(view);
        Intent intent;
        switch (view.getId()) {
//            case R.id.tv_tohaibao:
//                intent = new Intent(mContext, YaoQing_HaiBaoActivity.class);
//                startActivity(intent);
//                break;

        }
    }
}
