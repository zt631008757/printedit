package com.app.android.minjieprint.bean;

import com.google.zxing.BarcodeFormat;

import org.litepal.crud.DataSupport;

public class ElementsInfo extends DataSupport {

    //公共属性
    public int width = 300;
    public int height = 300;
    public int xpos = 0;
    public int ypos = 5;
    public int orientation = 0; //旋转角度

    public float red = 0;     //红
    public float green = 0;   //绿
    public float blue = 0;    //蓝
    public String color = "#333333";

    //文字属性
    public int alignment = 0;       // 文字对齐方式. 1 - 居左(默认), 2 - 居中, 3 - 居右
    public boolean deleteline;      //删除线  0 - 无删除线; 1 - 有删除线
    public String fontname = "微软雅黑";     //字体名
    public int fontsize = 180;        //字体大小
    public boolean italic;          //斜体     0 - 非斜体; 1 - 斜体
    public boolean blod;          //斜体     0 - 非斜体; 1 - 斜体
    public int kerning = 1;         //字间距
    public String text = "";
    public boolean underscore;      //下划线 0 - 无下划线; 1 - 有下划线

    //时间格式
    public String format_date = "yyyy-MM-dd";       //日期格式
    public String format_time = "HH:mm:ss";       //时间格式
    public int date_migration;       //日期偏移

    //线条属性
    public int padding;         //虚线时的实虚间隔
    public int lineStyle;       //线条类型. 0 - 实线, 1 - 虚线
    public int lineWidth;       //线条宽度

    //图片属性
    public String base64;       //图片内容
    public String imgLocalPath;     //图片地址

    //二维码属性
    public int correction;     // 二维码纠错等级   1 - 低, 2 - 中, 3 - 高, 4 - 最强

    //条码属性
    public String codetype = BarcodeFormat.CODE_39.name();        //条形码类型  1- Code39(默认), 2-Code93, 3-Code128, 4-EAN8, 5-EAN13, 6-UPCA, 7-UPCE
    public boolean hastext = true;         //是否显示条码内容  0 - 不显示, 1 - 显示

    //矩形
    public int borderwidth = 6;     //边框宽度
    public boolean fill;            //是否填充区域 0 - 不填充, 1 - 填充
    public int radius = 30;          //圆角半径
    public int rectStyle;      //形状类型   0 - 矩形; 1 - 圆角矩形; 2 - 椭圆; 3 - 圆形


    //本地使用
    public int name;
    public int resouce;
    public String viewtype;    //类型  1：文本  2：条码  3：二维码  4：照片  5：logo   6：线条  7 形状   8:序列号   9：时间  10 扫码  11语音识别  12 图像识别


    public ElementsInfo() {

    }

    public ElementsInfo(String viewtype) {
        this.viewtype = viewtype;
    }

    public ElementsInfo(int name, String viewtype, int resouce) {
        this.name = name;
        this.resouce = resouce;
        this.viewtype = viewtype;
    }
}
